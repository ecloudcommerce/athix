# Mercadolibre module for Magento 1.x #


### How do I get set up? ###

#### Triggers Compatibility ####
* First run SELECT @@Global.log_bin, If the result is 0, skip second and third step
* Second run SELECT @@Global.log_bin_trust_function_creators, if the result is 1, skip third step
* third run SET GLOBAL log_bin_trust_function_creators = 1;


## Releases ##

### Version 1.2.18 - 2019-08-06 ###

* Fix state id in order when state is required by country

### Version 1.2.17 - 2019-06-24 ###

* Fix publish whe sku is a number

### Version 1.2.16 - 2019-06-13 ###

* Fix state id in order

### Version 1.2.15 - 2019-04-25 ###

* Fix duplicate listing

### Version 1.2.14 - 2019-04-04 ###

* Price & Stock int Bug Fix

### Version 1.2.13 - 2019-04-04 ###

* Export CSV Bug Fix

### Version 1.2.12 - 2019-03-13 ###

* General Bug Fixes 

### Version 1.2.9 - 2019-02-08 ###

* Bug Fix order grid

### Version 1.2.8 - 2019-01-31 ###

* Region name in order is fetch in Magento by iso2 code. 

### Version 1.2.7 - 2019-01-16 ###

* Massive sync
* Retrive orders from meli

### Version 1.2.6 - 2018-12-28 ###

* Fix in Meli catalog Product Filters

### Version 1.2.5 - 2018-12-26 ###

* Firstname and lastname now are visible in order create by Mercadolibre 
* Magento Increment Id is sent to middle tier

### Version 1.2.4 - 2018-12-09 ###

* Attribue update for fashion categories -- https://developers.mercadolibre.com.ar/es_ar/estamos-realizando-cambios-en-categorias-de-moda 
* New User Agent sent in all request

### Version 1.2.3 - 2018-12-04 ###

* Bug Fix permissions for Mercadolibre Module

### Version 1.2.2 - 2018-11-23 ###

* Added permissions for Mercadolibre Module
* Added Mercadolibre Order Grid

### Version 1.2.1 - 2018-10-05 ###

* Creates magento orders setting price from meli

### Version 1.1.10 - 2018-10-01 ###

* Creates magento orders using Meli Customer Group
* Fix Attributes render, now hidden attributes are ignored
* Fix Variant render, now only appear in non visible products

### Version 1.1.9 - 2018-09-19 ###

* Fix stock sync more than 1000 products

### Version 1.1.8 - 2018-09-12 ###

* Fix Meli Attribute list and boolean

### Version 1.1.7 ###

* Fix price by website
* Fix variant type text
* Fix required attributes

### Version 1.1.6 ###

* Shipping Address recieves company field

### Version 1.1.5 ###

* Bugfix meli category assign

### Version 1.1.4 ###

* Bugfix get items with special characters

### Version 1.1.3 ###

* Bugfix variations

### Version 1.1.2 ###

* Bugfix Mercadolibre API change in attribute tags

### Version 1.1.1 ###

* Add Stores to meli
* Bugfix incomplete variations can be sent to meli

### Version 1.0.18 ###

* Recieves notifications

### Version 1.0.17 ###

* Improve Send stock offset

### Version 1.0.16 ###

* Save product data before publish or sync

### Version 1.0.15 ###

* Shipping selection by product

### Version 1.0.14 ###

* Version info in module.
* Bugfix when product title has html reserved characters (HTML Entities)
* Bugfix publish data has no title or description

### Version 1.0.12 ###

* Order creation from Meli is enable only if stock integration is too.
* Bugfix when filtering associated products from configurable

### Version 1.0.11 ###

* Bugfix item edit in magento version 1.7
* Bugfix field meli_condition empty
