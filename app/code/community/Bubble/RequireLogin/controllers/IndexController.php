<?php
 
class Bubble_RequireLogin_IndexController extends Mage_Core_Controller_Front_Action
{
    public function indexAction()
    {
        //Get current layout state
        $this->loadLayout();   
 
        $block = $this->getLayout()->createBlock(
            'Mage_Core_Block_Template',
            'bubble.require_login',
            array(
                'template' => 'access-control/access-control.phtml'
            )
        );
 
        $this->getLayout()->getBlock('content')->append($block);
        //$this->getLayout()->getBlock('right')->insert($block, 'catalog.compare.sidebar', true);
 
        $this->_initLayoutMessages('core/session');
 
        $this->renderLayout();
    }
 
    public function checkaccessAction()
    {
        //Fetch submited params
        $params = $this->getRequest()->getParams();
 
        if($params['code'] != ''){
            if($params['code'] == Mage::getStoreConfig('bubble_requirelogin/startup/access_code', Mage::app()->getStore())){
                Mage::getSingleton('core/session')->setAccess($params['code']);
                $this->_redirect();
            }else{
                Mage::getSingleton('core/session')->setAccess('false');
                Mage::getSingleton('core/session')->addError('Código incorrecto.');
                $this->_redirect('access-control');
            }
        }else{
            Mage::getSingleton('core/session')->setAccess('false');
            Mage::getSingleton('core/session')->addError('Código incorrecto.');
            $this->_redirect('access-control');
        }
    }
}
 
?>