<?php
/**
 * @version   0.1.0
 * @author    Jorge Temoche <jtemoche.g@gmail.com>
 */
?><?php
class Ecloud_Pickit_Model_Config_EnvioTipo
{
    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        return array(
            array('value' => 'punto', 'label'=>Mage::helper('adminhtml')->__('Retirá el pedido en tu Punto Pickit más cercano -')),
            array('value' => 'domicilio', 'label'=>Mage::helper('adminhtml')->__('Envío a domicilio')),
            array('value' => 'ambos', 'label'=>Mage::helper('adminhtml')->__('Ambos')),
        );
    }
}
