<?php
// @var $setup Mage_Eav_Model_Entity_Setup
$setup = $this;

$setup->startSetup();

$setup->run("
    ALTER TABLE `{$setup->getTable('pickit_order')}` ADD COLUMN `envio_tipo` VARCHAR(20) NULL
");
$setup->endSetup();