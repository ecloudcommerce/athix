<?php
/**
 * GoogleTagManager plugin for Magento
 *
 * @package     Yireo_GoogleTagManager
 * @author      Yireo (https://www.yireo.com/)
 * @copyright   Copyright 2015 Yireo (https://www.yireo.com/)
 * @license     Open Source License (OSL v3)
 */

/**
 * Class Yireo_GoogleTagManager_Model_Backend_Source_Method
 */
class Yireo_GoogleTagManager_Model_Backend_Source_Method
{
    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        return array(
            array('value' => '0', 'label'=> Mage::helper('googletagmanager')->__('Observer')),
            array('value' => '1', 'label'=> Mage::helper('googletagmanager')->__('XML Layout')),
        );
    }

}
