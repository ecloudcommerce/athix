<?php

class Best4Mage_ConfigurableProductsSimplePrices_Helper_Data extends Mage_Core_Helper_Abstract
{
	private $_store = null;

	private function useProduct()
	{
		if($this->_store == null) $this->_store = Mage::app()->getStore();
		return $this->getConfig('product_level');
	}

	public function isEnable($product)
	{
		if($this->useProduct() == 1) {
			$product = Mage::getModel('catalog/product')->load($product->getId());
			return ($product->getCpspEnable() == 1);
		} else return ($this->getConfig('enable') == 1);
	}

	protected $_productResource = null;

	public function getTaxClassId($pid)
	{
		if($this->_productResource == null){
			$this->_productResource = Mage::getSingleton('catalog/product')->getResource();
		}

		return $this->_productResource->getAttributeRawValue($pid,'tax_class_id', $this->_store);
	}

	public function isShowPrices($product)
	{
		if($this->useProduct() == 1) return ($product->getCpspExpandPrices() == 1);
		else return ($this->getConfig('expand_prices') == 1);
	}

	public function isHideSameLowest($product)
	{
		if($this->getConfig('hide_same_lowest') == 1){
			$simpleProducts = $product->getTypeInstance(true)->getUsedProductIds($product);
			$simplePriceArray = array();
			if(count($simpleProducts) > 0){
				if(count($simpleProducts) == 1) return false;

				foreach ($simpleProducts as $simpleProduct) {
					$simpleProduct = Mage::getModel('catalog/product')->load($simpleProduct);
					$simplePriceArray[] = $simpleProduct->getFinalPrice();
				}
				if(count($simplePriceArray) == 1)
				{
					return false;
				}
				elseif(count(array_unique($simplePriceArray)) != count($simplePriceArray) && count(array_unique($simplePriceArray)) == 1)
				{
					return false;
				}
			}
		}
		return true;
	}

	public function isShowLowestPrice($product)
	{
		$lowestPrice = false;
		if($this->useProduct() == 1)
			$lowestPrice = ($product->getCpspShowLowest() == 1);
		else
			$lowestPrice = ($this->getConfig('show_lowest') == 1);

		if($lowestPrice){
			$lowestPrice = $this->isHideSameLowest($product);
		}
		return $lowestPrice;
	}

	public function isShowMaxRegularPrice($product)
	{
		if($this->useProduct() == 1) return ($product->getCpspShowMaxregular() == 1);
		else return ($this->getConfig('show_maxregular') == 1);
	}

	public function showCpspStock($product)
	{
		if($this->useProduct() == 1) return $product->getCpspShowStock();
		else return $this->getConfig('show_stock');
	}

	public function isTierConfigurable($product)
	{
		if($this->useProduct() == 1){
			if(is_object($product)) {
				$productId = $product->getId();
			} else {
				$productId = $product;
			}
			$product = Mage::getModel('catalog/product')->load($productId);
			return ($product->getCpspEnableTier() == 1);
		} else return $this->getConfig('enable_tier');
	}

	public function showLastPrice($product)
	{
		if($this->useProduct() == 1) return ($product->getCpspShowLastPrice() == 1);
		else return $this->getConfig('show_last_price');
	}

	public function getCpspUpdateFields(){
		$flds = explode(',', $this->getConfig('update_fields'));
		return array_filter($flds);
	}

	public function isTierBase()
	{
		return ($this->getConfig('tier_base','cptp') == 1);
	}

	public function getCpspPriceFormate()
	{
		return $this->generatedPriceFormatArray($this->getConfig('price_format'));
	}

	public function isRemoveDecimalPoint()
	{
		return ($this->getConfig('choose_formate') == 1);
	}

	static $websiteId = null;
	static $storeId = null;
	static $groupId = null;
	static $statusId = null;
	static $taxClassId = null;
	static $minPriceArrey = array();
	static $maxPriceArrey = array();

	public function setUpStaticData($_product)
	{
		$resource = Mage::getSingleton('core/resource');
		$readConnection = $resource->getConnection('core_read');

		if(self::$websiteId === null) self::$websiteId = Mage::app()->getWebsite()->getId();

		if(self::$storeId === null) self::$storeId = Mage::app()->getStore()->getStoreId();

		if(self::$groupId === null) self::$groupId = Mage::getSingleton('customer/session')->getCustomerGroupId();

		if(self::$statusId === null) {
			self::$statusId = $readConnection->fetchOne("SELECT `attribute_id` FROM ".$resource->getTableName('eav_attribute')." WHERE `attribute_code` LIKE 'status' AND `entity_type_id` = ".$_product->getEntityTypeId());
		}

		if(self::$taxClassId === null) {
			self::$taxClassId = $readConnection->fetchOne("SELECT `attribute_id` FROM ".$resource->getTableName('eav_attribute')." WHERE `attribute_code` LIKE 'tax_class_id' AND `entity_type_id` = ".$_product->getEntityTypeId());
		}
	}

	public function getMinimalProductPrice($_productId)
	{
		$this->getProductPrices($_productId);
		$minPriceArrey = self::$minPriceArrey[$_productId];
		return array(array_keys($minPriceArrey, min($minPriceArrey)),min($minPriceArrey));
	}

	public function getMaximumProductPrice($_productId)
	{
		$this->getProductPrices($_productId);
		$maxPriceArrey = self::$maxPriceArrey[$_productId];
		return array(array_keys($maxPriceArrey, max($maxPriceArrey)),max($maxPriceArrey));
	}

	protected function getProductPrices($productId)
	{
		if(!array_key_exists($productId, self::$minPriceArrey) || !array_key_exists($productId, self::$maxPriceArrey))
		{
			$resource = Mage::getSingleton('core/resource');
			$readConnection = $resource->getConnection('core_read');
			$subQuery = '';

			$_joinCheckStockStatus = "";
			if(isset($this->showCpspStock)) {
				if(1*$this->showCpspStock !== 0) {
					$_joinCheckStockStatus = "INNER JOIN ".$resource->getTableName('cataloginventory_stock_status')." AS `css` ON ((`css`.`product_id` = `cpip`.`entity_id`) AND (`css`.`stock_id` = 1) AND (`css`.`stock_status` = 1) AND `css`.`website_id` = `cpip`.`website_id`)";
				}
			}

			if($this->isTierConfigurable($productId)){
				/*$query = "SELECT `cpip`.`entity_id`, `cpip`.`min_price` AS `minimal_price`,`cpip`.`max_price` FROM ".$resource->getTableName('catalog_product_index_price')." `cpip` INNER JOIN ".$resource->getTableName('catalog_product_entity_int')." AS `at_status_default` ON ((`at_status_default`.`entity_id` = `cpip`.`entity_id`) AND (`at_status_default`.`attribute_id` = '".self::$statusId."') AND `at_status_default`.`store_id` = 0) LEFT JOIN ".$resource->getTableName('catalog_product_entity_int')." AS `at_status` ON ((`at_status`.`entity_id` = `cpip`.`entity_id`) AND (`at_status`.`attribute_id` = '".self::$statusId."') AND (`at_status`.`store_id` = ".self::$storeId.")) INNER JOIN ".$resource->getTableName('catalog_product_entity_int')." AS `at_tax_class_id_default` ON ((`at_tax_class_id_default`.`entity_id` = `cpip`.`entity_id`) AND (`at_tax_class_id_default`.`attribute_id` = '".self::$taxClassId."') AND `at_tax_class_id_default`.`store_id` = 0) LEFT JOIN ".$resource->getTableName('catalog_product_entity_int')." AS `at_tax_class_id` ON ((`at_tax_class_id`.`entity_id` = `cpip`.`entity_id`) AND (`at_tax_class_id`.`attribute_id` = '".self::$taxClassId."') AND (`at_tax_class_id`.`store_id` = ".self::$storeId.")) ".$_joinCheckStockStatus." WHERE `cpip`.`entity_id` IN ( SELECT `cpsl`.`product_id` FROM ".$resource->getTableName('catalog_product_super_link')." `cpsl` WHERE `cpsl`.`parent_id` = ".$productId.") AND (IF(`at_status`.`value_id` > 0, `at_status`.`value`, `at_status_default`.`value`) = '1') AND (IF(`at_tax_class_id`.`value_id` > 0, `at_tax_class_id`.`value`, `at_tax_class_id_default`.`value`) = `cpip`.`tax_class_id`) AND `cpip`.`website_id` = ".self::$websiteId." AND `cpip`.`customer_group_id` = ".self::$groupId;*/


				$query = "
SELECT d.entity_id,
  CASE WHEN tp.value IS NULL AND gp.value IS NOT NULL THEN Least( gp.value, MIN(d.value) )
       WHEN gp.value IS NULL AND tp.value IS NOT NULL THEN Least( tp.value, MIN(d.value) )
       WHEN tp.value IS NULL AND gp.value IS NULL THEN MIN(d.value)
       ELSE Least(tp.value, gp.value, MIN(d.value))
  END
  AS minimal_price,
  CASE WHEN tp.value IS NULL AND gp.value IS NOT NULL THEN GREATEST( gp.value, d.value )
       WHEN gp.value IS NULL AND tp.value IS NOT NULL THEN GREATEST( tp.value, d.value )
       WHEN tp.value IS NULL AND gp.value IS NULL THEN d.value
       ELSE GREATEST(tp.value, gp.value, d.value)
  END
   AS max_price
FROM ".$resource->getTableName('catalog_product_entity_decimal')." d
      LEFT JOIN ".$resource->getTableName('catalog_product_entity_tier_price')." tp
              ON ( tp.entity_id = d.entity_id )
      LEFT JOIN ".$resource->getTableName('catalog_product_entity_group_price')." gp
              ON ( gp.entity_id = d.entity_id )
      INNER JOIN ".$resource->getTableName('catalog_product_entity_int')." AS `at_status_default`
              ON ( ( `at_status_default`.`entity_id` = d.`entity_id` )
                    AND ( `at_status_default`.`attribute_id` = '".self::$statusId."' )
                    AND `at_status_default`.`store_id` = 0 )
      LEFT JOIN ".$resource->getTableName('catalog_product_entity_int')." AS `at_status`
              ON ( ( `at_status`.`entity_id` = d.`entity_id` )
                   AND ( `at_status`.`attribute_id` = '".self::$statusId."' )
                   AND ( `at_status`.`store_id` = ".self::$storeId." ) )
      INNER JOIN ".$resource->getTableName('catalog_product_entity_int')." AS `at_tax_class_id_default`
              ON ( ( `at_tax_class_id_default`.`entity_id` = d.`entity_id` )
                    AND ( `at_tax_class_id_default`.`attribute_id` = '".self::$taxClassId."' )
                    AND `at_tax_class_id_default`.`store_id` = 0 )
      LEFT JOIN ".$resource->getTableName('catalog_product_entity_int')." AS `at_tax_class_id`
              ON ( ( `at_tax_class_id`.`entity_id` = d.`entity_id` )
                   AND ( `at_tax_class_id`.`attribute_id` = '".self::$taxClassId."' )
                   AND ( `at_tax_class_id`.`store_id` = ".self::$storeId." ) )
WHERE d.attribute_id
    IN (

      SELECT attribute_id
      FROM ".$resource->getTableName('eav_attribute')."
      WHERE attribute_code IN ('price','special_price')
      AND entity_type_id = (
        SELECT entity_type_id
        FROM ".$resource->getTableName('eav_entity_type')."
        WHERE entity_type_code = 'catalog_product' )
    )
    AND `d`.`entity_id`
    IN (

      SELECT `cpsl`.`product_id`
      FROM ".$resource->getTableName('catalog_product_super_link')." `cpsl`
      WHERE `cpsl`.`parent_id` =".$productId."
    )
    AND ( IF(`at_status`.`value_id` > 0, `at_status`.`value`,`at_status_default`.`value`) = '1' )
GROUP BY d.entity_id";

				$priceCollection = $readConnection->fetchAll($query);
				$first = 0;
				$firstChildId = $productId;
				foreach($priceCollection as $price)
				{
					if($first++ == 0) $firstChildId = $price['entity_id'];
					self::$minPriceArrey[$productId][$price['entity_id']][] = $price['minimal_price'];
					self::$maxPriceArrey[$productId][$price['entity_id']] = $price['max_price'];
				}
				//echo $query; echo '<br><br><br>';
				$query = "SELECT `cpip`.`tier_price` AS `minimal_price` FROM ".$resource->getTableName('catalog_product_index_price')." `cpip` INNER JOIN ".$resource->getTableName('catalog_product_entity_int')." AS `at_status_default` ON ((`at_status_default`.`entity_id` = `cpip`.`entity_id`) AND (`at_status_default`.`attribute_id` = '".self::$statusId."') AND `at_status_default`.`store_id` = 0) LEFT JOIN ".$resource->getTableName('catalog_product_entity_int')." AS `at_status` ON ((`at_status`.`entity_id` = `cpip`.`entity_id`) AND (`at_status`.`attribute_id` = '".self::$statusId."') AND (`at_status`.`store_id` = ".self::$storeId.")) INNER JOIN ".$resource->getTableName('catalog_product_entity_int')." AS `at_tax_class_id_default` ON ((`at_tax_class_id_default`.`entity_id` = `cpip`.`entity_id`) AND (`at_tax_class_id_default`.`attribute_id` = '".self::$taxClassId."') AND `at_tax_class_id_default`.`store_id` = 0) LEFT JOIN ".$resource->getTableName('catalog_product_entity_int')." AS `at_tax_class_id` ON ((`at_tax_class_id`.`entity_id` = `cpip`.`entity_id`) AND (`at_tax_class_id`.`attribute_id` = '".self::$taxClassId."') AND (`at_tax_class_id`.`store_id` = ".self::$storeId.")) ".$_joinCheckStockStatus." WHERE `cpip`.`entity_id` = ".$productId." AND (IF(`at_status`.`value_id` > 0, `at_status`.`value`, `at_status_default`.`value`) = '1') AND (IF(`at_tax_class_id`.`value_id` > 0, `at_tax_class_id`.`value`, `at_tax_class_id_default`.`value`) = `cpip`.`tax_class_id`) AND `cpip`.`website_id` = ".self::$websiteId." AND `cpip`.`customer_group_id` = ".self::$groupId;
				//echo $query; die;
				$priceCollection = min($readConnection->fetchCol($query));
				self::$minPriceArrey[$productId][$firstChildId][] = $priceCollection;

			} else {
				/*$query = "SELECT `cpip`.`entity_id`,IF(`cpip`.`tier_price` IS NOT NULL, LEAST(`cpip`.`min_price`, `cpip`.`tier_price`), `cpip`.`min_price`) AS `minimal_price`,`cpip`.`max_price` FROM ".$resource->getTableName('catalog_product_index_price')." `cpip` INNER JOIN ".$resource->getTableName('catalog_product_entity_int')." AS `at_status_default` ON ((`at_status_default`.`entity_id` = `cpip`.`entity_id`) AND (`at_status_default`.`attribute_id` = '".self::$statusId."') AND `at_status_default`.`store_id` = 0) LEFT JOIN ".$resource->getTableName('catalog_product_entity_int')." AS `at_status` ON ((`at_status`.`entity_id` = `cpip`.`entity_id`) AND (`at_status`.`attribute_id` = '".self::$statusId."') AND (`at_status`.`store_id` = ".self::$storeId.")) INNER JOIN ".$resource->getTableName('catalog_product_entity_int')." AS `at_tax_class_id_default` ON ((`at_tax_class_id_default`.`entity_id` = `cpip`.`entity_id`) AND (`at_tax_class_id_default`.`attribute_id` = '".self::$taxClassId."') AND `at_tax_class_id_default`.`store_id` = 0) LEFT JOIN ".$resource->getTableName('catalog_product_entity_int')." AS `at_tax_class_id` ON ((`at_tax_class_id`.`entity_id` = `cpip`.`entity_id`) AND (`at_tax_class_id`.`attribute_id` = '".self::$taxClassId."') AND (`at_tax_class_id`.`store_id` = ".self::$storeId.")) ".$_joinCheckStockStatus." WHERE `cpip`.`entity_id` IN ( SELECT `cpsl`.`product_id` FROM ".$resource->getTableName('catalog_product_super_link')." `cpsl` WHERE `cpsl`.`parent_id` = ".$productId.") AND (IF(`at_status`.`value_id` > 0, `at_status`.`value`, `at_status_default`.`value`) = '1') AND (IF(`at_tax_class_id`.`value_id` > 0, `at_tax_class_id`.`value`, `at_tax_class_id_default`.`value`) = `cpip`.`tax_class_id`) AND `cpip`.`website_id` = ".self::$websiteId." AND `cpip`.`customer_group_id` = ".self::$groupId;
*/


				$query = "
SELECT d.entity_id,
  CASE WHEN tp.value IS NULL AND gp.value IS NOT NULL THEN Least( gp.value, MIN(d.value) )
       WHEN gp.value IS NULL AND tp.value IS NOT NULL THEN Least( tp.value, MIN(d.value) )
       WHEN tp.value IS NULL AND gp.value IS NULL THEN MIN(d.value)
       ELSE Least(tp.value, gp.value, MIN(d.value))
  END
  AS minimal_price,
  CASE WHEN tp.value IS NULL AND gp.value IS NOT NULL THEN GREATEST( gp.value, MAX(d.value) )
       WHEN gp.value IS NULL AND tp.value IS NOT NULL THEN GREATEST( tp.value, MAX(d.value) )
       WHEN tp.value IS NULL AND gp.value IS NULL THEN MAX(d.value)
       ELSE GREATEST(tp.value, gp.value, MAX(d.value))
  END
   AS max_price
FROM ".$resource->getTableName('catalog_product_entity_decimal')." d
      LEFT JOIN ".$resource->getTableName('catalog_product_entity_tier_price')." tp
              ON ( tp.entity_id = d.entity_id )
      LEFT JOIN ".$resource->getTableName('catalog_product_entity_group_price')." gp
              ON ( gp.entity_id = d.entity_id )
      INNER JOIN ".$resource->getTableName('catalog_product_entity_int')." AS `at_status_default`
              ON ( ( `at_status_default`.`entity_id` = d.`entity_id` )
                    AND ( `at_status_default`.`attribute_id` = '".self::$statusId."' )
                    AND `at_status_default`.`store_id` = 0 )
      LEFT JOIN ".$resource->getTableName('catalog_product_entity_int')." AS `at_status`
              ON ( ( `at_status`.`entity_id` = d.`entity_id` )
                   AND ( `at_status`.`attribute_id` = '".self::$statusId."' )
                   AND ( `at_status`.`store_id` = ".self::$storeId." ) )
      INNER JOIN ".$resource->getTableName('catalog_product_entity_int')." AS `at_tax_class_id_default`
              ON ( ( `at_tax_class_id_default`.`entity_id` = d.`entity_id` )
                    AND ( `at_tax_class_id_default`.`attribute_id` = '".self::$taxClassId."' )
                    AND `at_tax_class_id_default`.`store_id` = 0 )
      LEFT JOIN ".$resource->getTableName('catalog_product_entity_int')." AS `at_tax_class_id`
              ON ( ( `at_tax_class_id`.`entity_id` = d.`entity_id` )
                   AND ( `at_tax_class_id`.`attribute_id` = '".self::$taxClassId."' )
                   AND ( `at_tax_class_id`.`store_id` = ".self::$storeId." ) )
WHERE d.attribute_id
    IN (

      SELECT attribute_id
      FROM ".$resource->getTableName('eav_attribute')."
      WHERE attribute_code IN ('price','special_price')
      AND entity_type_id = (
        SELECT entity_type_id
        FROM ".$resource->getTableName('eav_entity_type')."
        WHERE entity_type_code = 'catalog_product' )
    )
    AND `d`.`entity_id`
    IN (

      SELECT `cpsl`.`product_id`
      FROM ".$resource->getTableName('catalog_product_super_link')." `cpsl`
      WHERE `cpsl`.`parent_id` =".$productId."
    )
    AND ( IF(`at_status`.`value_id` > 0, `at_status`.`value`,`at_status_default`.`value`) = '1' )
GROUP BY d.entity_id";


				$priceCollection = $readConnection->fetchAll($query);
				foreach($priceCollection as $price)
				{
					self::$minPriceArrey[$productId][$price['entity_id']][] = $price['minimal_price'];
					self::$maxPriceArrey[$productId][$price['entity_id']] = $price['max_price'];
				}
				//echo $query; die;
			}
			if(count(self::$minPriceArrey[$productId]) > 0)
			{
				foreach (self::$minPriceArrey[$productId] as $entity_id => $minPrices) {
					self::$minPriceArrey[$productId][$entity_id] = min($minPrices);
				}
			}
		}
	}

	private function getconfig($fieldName, $fcpm = 'cpsp', $basic_options = 'settings')
	{
		return Mage::getStoreConfig($fcpm.'/'.$basic_options.'/'.$fieldName, $this->_store);
	}

	public function getWishlistItemByProduct($product)
	{
		$customer = Mage::getSingleton('customer/session')->getCustomer();
		if($customer->getId())
		{
			$wishlist = Mage::getModel('wishlist/wishlist')->loadByCustomer($customer, true);
			$wishListItemCollection = $wishlist->getItemCollection();
			foreach ($wishListItemCollection as $item)
			{
				if($item->representProduct($product))
				{
					return $item;
				}
			}
		} else {
			return false;
		}
	}

	private function generatedPriceFormatArray($key = 0)
	{
		if($key == null) $key = 0;
		$precision = 2;
		if($this->isRemoveDecimalPoint()) $precision = 0;
		$arrOfPriceFormat = array(
			'0' => array(
				'precision' => $precision,
				'requiredPrecision' => $precision,
				'decimalSymbol' => '.',
				'groupSymbol' => ',',
				'groupLength' => 3
			),
			'1' => array(
				'precision' => $precision,
				'requiredPrecision' => $precision,
				'decimalSymbol' => ',',
				'groupSymbol' => '',
				'groupLength' => 3
			),
			'2' => array(
				'precision' => $precision,
				'requiredPrecision' => $precision,
				'decimalSymbol' => '.',
				'groupSymbol' => '',
				'groupLength' => 3
			)
		);

		return (array_key_exists($key,$arrOfPriceFormat) ? $arrOfPriceFormat[$key] : $arrOfPriceFormat[0]);
	}

	protected $_simpleSustomOptions = array();
	public function getCustomOptions(Mage_Catalog_Model_Product $product, $itemInfo)
    {
		$usedKey = 'simple_custom_option_'.$product->getid();

		if(!isset($this->_simpleSustomOptions[$usedKey]))
		{
			$options = array();
			foreach ($product->getOptions() as $option)
			{
			    /* @var $option Mage_Catalog_Model_Product_Option */
			    $group = $option->groupFactory($option->getType())
			        ->setOption($option)
			        ->setProduct($product)
			        ->setRequest($itemInfo)
			        ->validateUserValue($itemInfo->getOptions());

			    $optionValue = $itemInfo->getData('options');
			    $optionValue = $optionValue[$option->getId()];

			    $price = 0;
	            foreach ($option->getValues() as $key => $value) {
	            	if(is_array($optionValue)){
	            		if(in_array($value->getId(), $optionValue))
	            		{
		                    $price += $value->getPrice(true);
		                }
	            	} elseif($value->getId() == $optionValue){
	                	$price += $value->getPrice(true);
	                }
	            }

	            $optionValue = (is_array($optionValue) ? implode(',',$optionValue) : $optionValue);
			    $options[] = array(
			        'label' => $option->getTitle(),
			        'value' => $group->getFormattedOptionValue($optionValue),
			        'print_value' => $group->getPrintableOptionValue($optionValue),
			        'option_id' => $option->getId(),
			        'option_type' => $option->getType(),
			        'price' => $price,
			    );
			}
			$this->_simpleSustomOptions[$usedKey] = $options;
		}

		return $this->_simpleSustomOptions[$usedKey];
    }

    public function getItemBuyRequest($_itemId) {

    	$resource = Mage::getSingleton('core/resource');
		$readConnection = $resource->getConnection('core_read');
		if($_itemId) {
			$query = "SELECT value
FROM ".$resource->getTableName("sales_flat_quote_item_option")."
WHERE `code` = 'info_buyRequest'
AND `item_id` = ".$_itemId;

			$_buyReq = $readConnection->fetchOne($query);
			return $_buyReq;
		}
    }
}
