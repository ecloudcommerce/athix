<?php

class Brandlive_Facturante_Helper_Data extends Facturante_Invoicing_Helper_Data{

	public function getEnviarComprobanteFacturante($website = null){
		return (bool)Mage::app()->getWebsite($website)->getConfig('facturante/connection2/enviar_comprobante');
	}

	public function getEnviarComprobanteMagento($website = null){
		return (bool)Mage::app()->getWebsite($website)->getConfig('facturante/connection2/enviar_comprobante_magento');
	}

	public function getDefaultTemplateEmail($website = null){
		return (bool)Mage::app()->getWebsite($website)->getConfig('facturante/connection2/comprobante_magento_default_email_template');
	}	
	
	public function getEmailSubject($website = null){
		return Mage::app()->getWebsite($website)->getConfig('facturante/connection2/comprobante_magento_email_subject');
	}

	public function getSenderName($website = null){
		return Mage::app()->getWebsite($website)->getConfig('trans_email/ident_general/name');
	}

	public function getSenderEmail($website = null){
		return Mage::app()->getWebsite($website)->getConfig('trans_email/ident_general/email');
	}

}
