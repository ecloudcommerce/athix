<?php 
#File: app/code/local/Brandlive/Mpexpress/Model/System/Validation/Expirationduration.php
class Brandlive_Mpexpress_Model_System_Validation_Expirationduration extends Mage_Core_Model_Config_Data
{
    public function save()
    {
        $minutes = $this->getValue(); //get the value from our config

        if(!is_numeric($minutes)){
            Mage::throwException("Error: La Duración de la ventana de pago debe ser un número mayor o igual a 5 minutos.");                     
        }else{
            if($minutes < 5){
                Mage::throwException("Error: La Duración de la ventana de pago debe ser mínimo de 5 minutos.");         
            }else{
                //call original save method so whatever happened 
                //before still happens (the value saves)
                return parent::save();  
            }
        }
        
    }
}