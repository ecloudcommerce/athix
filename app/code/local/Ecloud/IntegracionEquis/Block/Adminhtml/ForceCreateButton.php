<?php

class Ecloud_IntegracionEquis_Block_Adminhtml_ForceCreateButton extends Mage_Adminhtml_Block_System_Config_Form_Field
{
	protected function _getElementHtml(Varien_Data_Form_Element_Abstract $element)
	{
		$this->setElement($element);
		$buttonHtml = $this->_getAddRowButtonHtml("Crear productos");
		return $buttonHtml;
	}


	protected function _getAddRowButtonHtml($title)
	{
		$url = Mage::helper('adminhtml')->getUrl("*/integracionEquis/forceCreate");

		$buttonHtml = $this->getLayout()->createBlock('adminhtml/widget_button')
			->setType('button')
			->setLabel($this->__($title))
			->setOnClick("window.location.href='" . $url . "'")
			->toHtml();

		return $buttonHtml;
	}
}
