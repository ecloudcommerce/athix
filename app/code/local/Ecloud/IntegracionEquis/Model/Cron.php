<?php

class Ecloud_IntegracionEquis_Model_Cron
{
	const DATE_FORMAT = "Y-m-d-H:i:s";

	protected $debugMode;

	public function __construct()
	{
		$this->initLog();
	}

	public function syncPendingOrders()
	{
		// Obtener IDs de pedidos con equis_status = "error" y acreditados
		$orderIds = $this->getOrdersWithSyncError();

		// Si no hay pedidos acreditados con error de sincronización
		if (!$orderIds || count($orderIds) <= 0) {
			$message = "No hay pedidos acreditados sin sincronizar";
			$this->log($message);
			return;
		}

		foreach ($orderIds as $orderId) {
			// Carga el pedido de Magento
			$order = Mage::getModel("sales/order")->load($orderId);
			// Dispara el evento para sincronizar el pedido
			Mage::dispatchEvent("sales_order_cron_sync", array("order" => $order));
		}
	}

	protected function getOrdersWithSyncError()
	{
		$resource = Mage::getSingleton('core/resource');
		$readConnection = $resource->getConnection('core_read');
		$query = "SELECT `sales_flat_order`.`entity_id`, `sales_flat_order`.`created_at` 
		FROM `sales_flat_order` 
		WHERE (
			`sales_flat_order`.`equis_status` <> '" . Ecloud_IntegracionEquis_Model_Observer::STATUS_OK . "'
			OR `sales_flat_order`.`equis_status` IS NULL
		)
		AND `sales_flat_order`.`status` = 'processing' 
		AND `sales_flat_order`.`created_at` > '2021-05-01'
		AND `sales_flat_order`.`entity_id` NOT IN (
			SELECT `sales_flat_order_status_history`.`parent_id` from `sales_flat_order`
			inner join `sales_flat_order_status_history`
			on `sales_flat_order`.`entity_id` = `sales_flat_order_status_history`.`parent_id`
			WHERE `sales_flat_order_status_history`.`comment` like '%Pedido sincronizado correctamente con Equis%'
		);";
		$results = $readConnection->fetchAll($query);

		if ($results && $results) {
			return array_map(function ($element) {
				return $element["entity_id"];
			}, $results);
		}
		return [];
	}

	/* ---------------- FUNCIONES DE LOG ---------------- */

	protected function initLog()
	{
		$folder = Mage::getBaseDir('var') . "/log/equis";
		if (!is_dir($folder)) {
			mkdir($folder);
		}
		$logPath = $folder . "/_equis_cron.log";

		// Crea el archivo si no existe
		file_put_contents($logPath, "", FILE_APPEND);
		chmod($logPath, 0666);		// rw-rw-rw-
	}

	protected function log($message)
	{
		Mage::log($message, null, "equis/_equis_cron.log", true);
	}
}
