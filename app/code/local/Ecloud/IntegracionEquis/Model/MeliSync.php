<?php

class Ecloud_IntegracionEquis_Model_MeliSync
{
	public function syncPendingOrder($observer)
	{
		$msj = "Sincronizando pedido " . $observer->getOrder()->getIncrementId();
		$this->log($msj);
		$orderManager = new Ecloud_IntegracionEquis_Model_Observer();
		$result = $orderManager->index($observer);
		$msj = $observer->getOrder()->getIncrementId() . ": ";
		$msj .= $result ? "Sincronizado correctamente" : "No sincronizado";
		$this->log($msj);
	}

	protected function log($message)
	{
		Mage::log($message, null, "equis/_equis_cron.log", true);
	}
}
