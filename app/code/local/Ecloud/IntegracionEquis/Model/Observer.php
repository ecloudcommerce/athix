<?php

class Ecloud_IntegracionEquis_Model_Observer extends Mage_Core_Model_Session_Abstract
{
	const CURRENT_SITE = "MN";
	const STATUS_OK = "Sincronizado";
	const STATUS_ERROR = "Error";
	const DIADORA_WEBSITE = "Diadora";
	const ATHIX_WEBSITE = "Athix";

	// Objetos de Magento
	protected $order;
	protected $orderItems = [];
	protected $customer;
	protected $shippingAddress;
	protected $billingAddress;

	// Objetos para la query
	protected $orderData;
	protected $linesData;
	protected $customerData;
	protected $shippingData;
	protected $incrementId;
	protected $invoiceIncrementId;
	protected $customerId;

	// Query final
	protected $erpQuery;

	// Otras variables
	protected $pedidoWeb;
	protected $debugMode;
	protected $manualSync;
	protected $invoiceSync;
	protected $cronSync;
	protected $syncTypeLabel = "";
	protected $nonExistingSkus = [];
	protected $offsetInterval;


	/* ---------------- FUNCIONES DE LOG ---------------- */

	protected function initLog()
	{
		$folder = Mage::getBaseDir('var') . "/log/equis";
		if (!is_dir($folder)) {
			mkdir($folder);
		}
		$logPath = $folder . "/_equis_orders.log";
		$debugPath = $folder . "/_equis_debug.log";

		// Crea el archivo si no existe
		file_put_contents($logPath, "", FILE_APPEND);
		file_put_contents($debugPath, "", FILE_APPEND);
		chmod($logPath, 0666);		// rw-rw-rw-
		chmod($debugPath, 0666);	// rw-rw-rw-
	}

	protected function log($message)
	{
		Mage::log($message, null, "equis/_equis_orders.log", true);
	}

	protected function debugLog($message)
	{
		if ($this->debugMode) {
			Mage::log($message, null, "equis/_equis_debug.log", true);
		}
	}


	/* ---------------- FUNCIONES DE SET DE VARIABLES ---------------- */

	protected function setDebugMode()
	{
		$this->debugMode = Mage::getStoreConfigFlag('equisconfig/salesimport/debug_mode');
	}

	protected function setPedidoWeb()
	{
		$this->pedidoWeb = false;
		if ($this->order->getStoreId() != 0) {
			$this->pedidoWeb  = true;
		}
	}

	protected function setSyncType($observer)
	{
		$eventName = $observer->getEvent()->getName();
		$this->manualSync = $eventName == "sales_order_force_sync";
		$this->cronSync = $eventName == "sales_order_cron_sync";
		$this->invoiceSync = $eventName == "sales_order_invoice_save_after";
	}

	protected function setSyncLabel()
	{
		if ($this->manualSync) {
			$this->syncTypeLabel = "Sincronización Manual";
		} else if ($this->cronSync) {
			$this->syncTypeLabel = "Sincronización Cron";
		} else if ($this->invoiceSync) {
			$this->syncTypeLabel = "Sincronización Automática";
		}
	}

	protected function setCustomerId()
	{
		$this->customerId = $this->order->getCustomerId();
	}

	protected function setIncrementIds($observer)
	{
		// Obtiene el increment ID del pedido, dependiendo del tipo de evento
		if (self::CURRENT_SITE == "MN" && $this->invoiceSync) {
			$this->incrementId = $observer->getInvoice()->getOrder()->getIncrementId();
			$this->invoiceIncrementId = $observer->getInvoice()->getIncrementId();
		} else {
			$this->incrementId = $observer->getOrder()->getIncrementId();
		}
	}

	protected function setBillingAddress()
	{
		$this->billingAddress = $this->order->getBillingAddress();
	}

	protected function setShippingAddress()
	{
		$this->shippingAddress = $this->order->getShippingAddress();
	}

	protected function setHourDiff()
	{
		// Fecha en timezone del servidor
		$serverDate = new DateTime("now", new DateTimeZone("UTC"));
		// Fecha en timezone de los pedidos
		$actualDate = new DateTime("now", new DateTimeZone("America/Sao_Paulo"));

		$actualDateOffset = $actualDate->getOffset();
		$serverDateOffset = $serverDate->getOffset();
		// La diferencia de horarios se tiene que restar de las fechas de pedidos
		$offsetDiff =  ($serverDateOffset - $actualDateOffset) / 3600;
		$this->offsetInterval = date_interval_create_from_date_string($offsetDiff . ' hours');
	}


	/* ---------------- FUNCIONES DE LOADS DE MAGENTO ---------------- */

	protected function loadOrder()
	{
		$this->order = Mage::getModel('sales/order')->loadByIncrementId($this->incrementId);
		if (!$this->order->getId()) {
			$this->order = null;
			throw new Exception("No se encuentra el pedido con increment ID " . $this->incrementId);
		}
	}

	protected function loadCustomer()
	{
		$this->customer = Mage::getModel('customer/customer')->load($this->customerId);
		if (!$this->customer->getId()) {
			$this->customer = null;
			if (self::CURRENT_SITE == "MY") {
				throw new Exception("No se encuentra el cliente con ID " . $this->customerId);
			}
		}
	}

	protected function loadOrderItems()
	{
		foreach ($this->order->getAllItems() as $item) {

			if ($item->getProductType() == 'bundle') {
				$list_prod = Mage::getModel('catalog/product')->getCollection()->addAttributeToFilter('name', $item->getName());
				foreach ($list_prod as $list_item) {
					if ($list_item->getData('type_id') == 'simple') {
						$sku = $list_item->getSku();
					}
				}
			} else {
				$sku = $item->getSku();
			}


			$qty = $item->getData('qty_ordered');
			$price = $item->getPrice();
			if ($price <= 0) {
				continue;
			}

			$product = Mage::getModel("catalog/product")->loadByAttribute("sku", $sku);
			if (!$product) {
				$this->nonExistingSkus[] = $sku;
				continue;
			}
			$this->orderItems[$sku] = array(
				"product" => $product,
				"qty" => $qty,
				"price" => $price
			);
		}
	}

	protected function loadOrderData()
	{

		$origen = $this->getOrigen();

		$opcion = $this->getOpcion();

		$paymentType = "";
		$paymentId = "";
		$postCode = "";
		$invoiceId = "";
		if (self::CURRENT_SITE == "MN") {
			$clienteId = 9990;	// Venta online
			$sucursalId = 9990;	// Venta online
			$paymentType = $this->getPaymentType();

			// Los pedidos de checkmo no mandan el ID de pago
			if ($paymentType != "MPC") {
				// Los pagos de Mobbex mandan el DNI del cliente como ID de pago
				$paymentId = ($paymentType == "MO" || $paymentType == "MOD") ? $this->billingAddress->getDni() : $this->getPaymentId();
				if ($paymentId == "") {
					$paymentId = $this->order->getIncrementId();
				}
			}
			$postCode = $this->shippingAddress->getPostcode();
			$invoiceId = $this->getLastInvoiceId();
		} else {
			$clienteId = $this->customer->getData("cta_equivalencia_equis");
			$sucursalId = $this->customer->getData("cod_cta");
			$clienteId = $clienteId && $clienteId != "" ? $clienteId : $sucursalId;
		}
		$orderDate = (new DateTime($this->order->getUpdatedAt()))->sub($this->offsetInterval);


		$this->orderData = array(
			"Origen" => $origen,
			"Opcion" => $opcion,
			"ClienteId" => $clienteId,
			"ClienteSucursalId" => $sucursalId,
			"ClienteEmail" => $this->order->getCustomerEmail(),
			"PedidoNumero" => $this->order->getIncrementId(),
			"PedidoFecha" => $orderDate->format("Y-m-d"),
			"PedidoImporteBonificacion" => $this->order->getDiscountAmount() * (-1),
			"PedidoImporteEnvio" => $this->order->getShippingAmount(),
			"PedidoPagoTipo" => $paymentType,
			"PedidoPagoId" => $paymentId,
			"PedidoVoucherNumero" => $invoiceId,
			"DomEntregaCodPostal" => $postCode
		);
	}

	protected function loadLinesData()
	{
		$this->linesData = [];
		foreach ($this->orderItems as $sku => $itemData) {
			$product = $itemData["product"];
			$qty = $itemData["qty"];
			$price = $itemData["price"];

			if (self::CURRENT_SITE == "MN") {
				$itemVendedor = "1";	// Valor por defecto para pedidos de minorista
			} else {
				$itemVendedor = $this->getItemVendedor($product);
			}

			$code = $product->getData("equivalencia_equis");
			$code = $code ? $code : $sku;

			$this->linesData[] = array(
				"ProductoId" => $code,
				"VendedorId" => $itemVendedor,
				"Cantidad" => (int) $qty,
				"Precio" => (float) $price,
			);
		}
	}


	/* ---------------- FUNCIONES DE GET DE ATRIBUTOS ---------------- */

	protected function getOrigen()
	{
		if (self::CURRENT_SITE == "MN") {
			$shippingDescription = $this->order->getShippingDescription();
			if ($this->contains($shippingDescription, "Mercadolibre") && $this->contains($shippingDescription, "Envío Full")) {
				return "FU";
			}
		}
		return self::CURRENT_SITE;
	}

	protected function getWebsite()
	{
		if (self::CURRENT_SITE == "MN") {
			$storeName = $this->order->getStoreName();
			if ($storeName) {
				if ($this->startsWith($storeName, self::ATHIX_WEBSITE)) {
					return self::ATHIX_WEBSITE;
				}
				if ($this->startsWith($storeName, self::DIADORA_WEBSITE)) {
					return self::DIADORA_WEBSITE;
				}
			}
		}
		return null;
	}

	protected function getOpcion()
	{
		if (!$this->pedidoWeb) {
			// La opción es siempre la misma para todos los productos del pedido
			foreach ($this->orderItems as $sku => $data) {
				$product = $data["product"];
				break;
			}
			if ($product->getPresale()) {
				return "PV";
			} else {
				return "ST";
			}
		} else {
			$storeName = explode("\n", $this->order->getStoreName())[2];
			if (strtolower($storeName) == "preventa") {
				return "PV";
			} else {
				return "ST";
			}
		}
	}

	protected function getPaymentType()
	{
		$website = $this->getWebsite();
		$method = $this->order->getPayment()->getMethodInstance()->getCode();
		if ($website == self::ATHIX_WEBSITE) {
			if ($this->startsWith($method, "mercadopago")) {
				// Mercado pago
				return "MPA";
			} else if ($this->startsWith($method, "mercadolibre")) {
				// Mercado libre
				return "MP";
			} elseif ($this->startsWith($method, "mobbex")) {
				// Mobbex
				return "MO";
			} elseif ($this->startsWith($method, "modulodepago")) {
				// Todo pago
				return "TP";
			} elseif ($this->startsWith($method, "checkmo")) {
				// Check money order (cambio o devolución)
				return "MPC";
			}
		} else if ($website == self::DIADORA_WEBSITE) {
			if ($this->startsWith($method, "mercadopago") || $this->startsWith($method, "mercadolibre")) {
				// Mercado pago o Mercado libre
				return "MPD";
			} elseif ($this->startsWith($method, "mobbex")) {
				// Mobbex
				return "MOD";
			} elseif ($this->startsWith($method, "modulodepago")) {
				// Todo pago
				return "TP";
			} elseif ($this->startsWith($method, "checkmo")) {
				// Check money order (cambio o devolución)
				return "MPAC";
			}
		} else
			return "";
	}

	protected function getPaymentId()
	{
		$resource = Mage::getSingleton('core/resource');
		$readConnection = $resource->getConnection('core_read');
		$query = "SELECT `sales_flat_order_payment`.`last_trans_id` FROM `sales_flat_order_payment` INNER JOIN `sales_flat_order` on `sales_flat_order_payment`.`parent_id` = `sales_flat_order`.`entity_id` WHERE `sales_flat_order`.`increment_id` ='" . $this->order->getIncrementId() . "'";
		$results = $readConnection->fetchAll($query);

		if ($results[0] && $results[0]['last_trans_id']) {
			$paymentId = $results[0]['last_trans_id'];
			if ($this->contains($paymentId, "|")) {
				$ids = explode("|", $paymentId);
				$idCount = count($ids);
				return trim($ids[$idCount - 1]);
			}
			return $paymentId;
		}

		return "";
	}

	protected function getLastInvoiceId()
	{
		if ($this->invoiceIncrementId && $this->invoiceIncrementId != "") {
			return $this->invoiceIncrementId;
		}
		$invoiceCollection = $this->order->getInvoiceCollection();
		if (!$invoiceCollection)
			return "";

		$latestDate = null;
		$latestInvoiceId = "";
		foreach ($invoiceCollection as $invoice) {
			$invoiceDate = new DateTime($invoice->getCreatedAt());

			if (!$latestDate || $latestDate < $invoiceDate) {
				$latestDate = $invoiceDate;
				$latestInvoiceId = $invoice->getIncrementId();
			}
		}
		return $latestInvoiceId;
	}

	protected function getCustomerVendedores()
	{
		$resource = Mage::getSingleton('core/resource');
		$readConnection = $resource->getConnection('core_read');
		$customerId = $this->customer->getId();
		$query = "SELECT value 
		FROM customer_entity_text 
		WHERE entity_id = $customerId 
			AND attribute_id IN (
				SELECT attribute_id 
				FROM eav_attribute 
				WHERE attribute_code = 'vendedores'
					AND entity_type_id IN (
						SELECT entity_type_id 
						FROM eav_entity_type 
						WHERE entity_type_code = 'customer'
					)
			)";
		$results = $readConnection->fetchAll($query);
		if ($results && count($results) > 0) {
			return explode(",", $results[0]["value"]);
		}
	}

	protected function getItemVendedor($product)
	{
		if (!$this->pedidoWeb) {
			return $this->billingAddress->getPostcode();
		}
		$marca = null;
		if ($product->getAttributeText('manufacturer')) {
			$marca = $product->getAttributeText('manufacturer');
		} else {
			if ($product->getData('marca') != '') {
				$marca = $product->getData('marca') != '';
			} else {
				return "111";
			}
		}

		if (!$this->customer && self::CURRENT_SITE == "MN") {
			return "111";
		}

		$vendedores = array(
			"Via Marte" => $this->customer->getVendedorCalzados(),
			"Modare" => $this->customer->getVendedorCalzados(),
			"Namoro" => $this->customer->getVendedorCalzados(),
			"Kidy" => $this->customer->getVendedorCalzados(),
			"Vanner" => $this->customer->getVendedorCalzados(),
			"Hard Top" => $this->customer->getVendedorCalzados(),
			"Diadora" => $this->customer->getVendedorDiadora(),
			"Diadora College" => $this->customer->getVendedorDiadora(),
			"Diadora Lifestyle" => $this->customer->getVendedorDiadora(),
			"Athix" => $this->customer->getVendedorAthix(),
			"Athix College" => $this->customer->getVendedorAthix(),
			"Athix Flexy" => $this->customer->getVendedorAthix(),
			"Athix Winter" => $this->customer->getVendedorAthix(),
			"Usaflex" => $this->customer->getVendedorAthix(),
		);

		return $vendedores[$marca] ? $vendedores[$marca] : "111";
	}


	/* ---------------- FUNCIONES DE REQUEST ---------------- */

	protected function initQuery()
	{
		$objData = $this->orderData;
		$objData["Items"] = $this->linesData;
		$this->debugLog("#" . $this->order->getIncrementId() . ": Request enviada a Equis");
		$this->debugLog($objData);
		$jsonData = json_encode($objData);
		$this->erpQuery = array("Pedido" => $jsonData);
	}

	protected function startCurl()
	{
		$credentials = Mage::getStoreConfig('equisconfig/credentials');
		$url =  $credentials["url_new_order"];

		$curl = curl_init();


		$curlOptions = array(
			CURLOPT_URL => $url,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 120,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "POST",
			CURLOPT_POSTFIELDS => $this->erpQuery,
			CURLOPT_HTTPHEADER => array(
				"Cookie: GX_SESSION_ID=g2UBn7YXh%2B5VTev2w1j5qe8TYWmb8d4OX4vKS3fiGN4%3D; JSESSIONID=81EA751EBC9C986CA5EF1E4FA61C76C1"
			),
		);

		curl_setopt_array($curl, $curlOptions);

		return $curl;
	}

	protected function handleCurl($curl)
	{

		$curlResponse = curl_exec($curl);
		$curlError = curl_error($curl);

		curl_close($curl);

		$this->debugLog("#" . $this->order->getIncrementId() . ": Error de Equis:");
		$this->debugLog($curlError);
		$this->debugLog("#" . $this->order->getIncrementId() . ": Response de Equis:");
		$this->debugLog($curlResponse);

		if ($curlError) {
			throw new Exception("Error estableciendo conexion con Equis (CURL): " . $curlError);
		} else {
			if ($curlResponse == "") {
				$status = $this->order->getData('status');
				$msj = $this->syncTypeLabel . ": " . "Pedido sincronizado correctamente con Equis";
				$this->order->addStatusHistoryComment($msj, $status);
				$this->order->setEquisStatus(self::STATUS_OK);
				$this->order->save();
				// Redundante - lo hace por magento y por db
				$this->setEquisStatus($this->order, self::STATUS_OK);
				$msj = "#" . $this->incrementId . ": " . "Pedido sincronizado correctamente con Equis";
				$this->log($msj);
				$this->debugLog($msj);
			} else {
				$msj = "Pedido no sincronizado por error devuelto por equis: " . $curlResponse;
				throw new Exception($msj);
			}
		}
	}

	public function index($observer)
	{
		try {
			$this->initLog();
			$this->setDebugMode();
			$this->setHourDiff();
			$this->setSyncType($observer);
			$this->setSyncLabel();
			$this->validateActiveIntegration();

			$this->setIncrementIds($observer);
			$this->loadOrder();
			$this->validateMeliSync();
			$this->loadOrderItems();
			$this->validateProductsExist();

			$this->setCustomerId();

			$this->setPedidoWeb();

			$this->validateManualSync();
			// $this->validateProcessingOrder();

			$this->loadCustomer();

			$this->setBillingAddress();
			$this->setShippingAddress();

			$this->loadOrderData();
			$this->loadLinesData();

			// $this->validateDevolucion();

			$this->initQuery();

			$curl = $this->startCurl();
			$this->handleCurl($curl);

			$msj = $this->syncTypeLabel . ": Pedido sincronizado";
			$this->addSuccessMessage($msj);
			return true;
		} catch (Exception $e) {
			$msj = "#" . $this->incrementId . ": " . $e->getMessage();
			$this->log($msj);
			$this->debugLog($msj);
			if ($this->order) {
				$status = $this->order->getData('status');
				$msj = $this->syncTypeLabel . ": " . $e->getMessage();
				if (!$this->cronSync) {
					$this->order->addStatusHistoryComment($msj, $status);
					$this->order->save();
				}
				if ($this->order->getEquisStatus() != self::STATUS_OK) {
					$this->setEquisStatus($this->order, self::STATUS_ERROR);
				}
			}
			$this->addWarningMessage($e->getMessage());
			return false;
		}
	}


	/* ---------------- CHECKS DE INTEGRACIÓN ---------------- */

	protected function validateActiveIntegration()
	{
		// Verificar que la integración esté activada
		$active = Mage::getStoreConfigFlag('equisconfig/salesimport/active');
		if (!$active) {
			throw new Exception("Sincronización de pedidos no activada");
		}
	}

	protected function validateManualSync()
	{
		// Si no es una sincronización manual, verificar que la sincronización automática esté activada
		if (!$this->manualSync && !Mage::getStoreConfigFlag('equisconfig/salesimport/automatic_sync_active')) {
			throw new Exception("Sincronización automática de pedidos no activada");
		}
	}

	protected function validateMeliSync()
	{
		// Si es un pedido de Mercado Libre, verificar que sea una sincronización manual o disparada por un cron
		$method = $this->order->getPayment()->getMethodInstance()->getCode();
		$isMeliOrder = $this->startsWith($method, "mercadolibre");
		if ($isMeliOrder && $this->invoiceSync) {
			throw new Exception("Sncronización automática salteada para pedidos de Mercado Libre");
		}
	}

	protected function validateProcessingOrder()
	{
		// Si es una sincronización manual de Minorista, verificar que el pedido esté facturado
		if (self::CURRENT_SITE == "MN" && $this->manualSync) {
			// Si el pedido se sincroniza manualmente, verificar que esté facturado
			if ($this->order->getStatus() != "processing") {
				throw new Exception("Sólo se pueden sincronizar pedidos ya facturados");
			}
		}
	}

	protected function validateDevolucion()
	{
		if (self::CURRENT_SITE == "MN" && $this->orderData["PedidoPagoTipo"] == "MPC") {
			throw new Exception("Cambio o devolución. Pedido no sincronizado");
		}
	}

	protected function validateProductsExist()
	{
		foreach ($this->nonExistingSkus as $sku) {
			$msj = "No existe el producto con SKU " . $sku;
			$this->addWarningMessage($msj);
			$this->log("#" . $this->incrementId . ": " . $msj);
			$this->debugLog("#" . $this->incrementId . ": " . $msj);
		}
		if (count($this->orderItems) <= 0) {
			throw new Exception("El pedido no tiene productos existentes o con precio");
		}
	}


	/* ---------------- OTRAS FUNCIONES ---------------- */

	public function startsWith($string, $startString)
	{
		$len = strlen($startString);
		return (substr($string, 0, $len) === $startString);
	}

	public function contains($string, $containedString)
	{
		$pos = strpos($string, $containedString);
		return !($pos === false);
	}

	protected function setEquisStatus($order, $status)
	{
		$incrementId = $order->getIncrementId();
		$resource = Mage::getSingleton('core/resource');
		$writeConnection = $resource->getConnection('core_write');
		$query = "UPDATE sales_flat_order SET equis_status = '" . $status . "' WHERE increment_id = '" . $incrementId . "';";
		return $writeConnection->exec($query);
	}

	protected function addSuccessMessage($message)
	{
		if ($this->manualSync) {
			// Si el pedido se sincroniza desde el admin, agregar una alerta
			Mage::getSingleton('adminhtml/session')->addSuccess($message);
		}
	}

	protected function addWarningMessage($message)
	{
		if ($this->manualSync) {
			// Si el pedido se sincroniza desde el admin, agregar una alerta
			Mage::getSingleton('adminhtml/session')->addWarning($message);
		}
	}
	public function runCatalogSync()
	{
		$catalogEnable = Mage::getStoreConfigFlag('equisconfig/catalogimport/cron_update');
		if ($catalogEnable) {
			$shellPath = Mage::getBaseDir() . "/shell/ecloud/equis/config-sync.php";
			// Obtiene el comando de ejecución de la config
			$command = Mage::getStoreConfig('equisconfig/credentials/command');
			$command = (!$command || $command == "") ? "php" : trim($command);
			$command .= " " . $shellPath;
			// Ejecuta la integración y captura la salida

			exec($command);
		}
	}
}
