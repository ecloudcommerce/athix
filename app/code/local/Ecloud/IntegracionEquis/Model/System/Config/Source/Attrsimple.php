<?php
class Ecloud_IntegracionEquis_Model_System_Config_Source_Attrsimple
{
    public function toOptionArray()
    {
        return array(
            array('value' => "equivalencia_equis",  'label' => Mage::helper('integracionequis')->__('Equivalencia Equis')),
            array('value' => "price",               'label' => Mage::helper('integracionequis')->__('Price')),
            array('value' => "stock",               'label' => Mage::helper('integracionequis')->__('Stock')),
            //array('value' => "descripcion",       'label' => Mage::helper('integracionequis')->__('Nombre')),
            //array('value' => "attributeset",      'label' => Mage::helper('integracionequis')->__('AttributeSet')),
            //array('value' => "linea",             'label' => Mage::helper('integracionequis')->__('Genero')),
            //array('value' => "sublinea",          'label' => Mage::helper('integracionequis')->__('Linea')),
            //array('value' => "marca",             'label' => Mage::helper('integracionequis')->__('Marca')),
            //array('value' => "proveedor",         'label' => Mage::helper('integracionequis')->__('Proveedor')),
            //array('value' => "material",          'label' => Mage::helper('integracionequis')->__('Material')),
            //array('value' => "color",             'label' => Mage::helper('integracionequis')->__('Color')),
            //array('value' => "curva",             'label' => Mage::helper('integracionequis')->__('Talle')),
            //array('value' => "website",           'label' => Mage::helper('integracionequis')->__('Website')),

        );
    }
    public function toArray()
    {
        return array(
            "equivalencia_equis"        => Mage::helper('integracionequis')->__('Equivalencia Equis'),
            "price"						=> Mage::helper('integracionequis')->__('Price'),
            "stock"						=> Mage::helper('integracionequis')->__('Stock'),
            // "descripcion"				=> Mage::helper('integracionequis')->__('Descripcion'),
            // "attributeset"				=> Mage::helper('integracionequis')->__('AttributeSet'),
            // "linea"					    => Mage::helper('integracionequis')->__('Linea'),
            // "sublinea"					=> Mage::helper('integracionequis')->__('Sublinea'),
            // "marca"						=> Mage::helper('integracionequis')->__('Marca'),
            // "proveedor"					=> Mage::helper('integracionequis')->__('Proveedor'),
            // "material"					=> Mage::helper('integracionequis')->__('Material'),
            // "color"						=> Mage::helper('integracionequis')->__('Color'),
            // "curva"						=> Mage::helper('integracionequis')->__('Curva'),
            // "website"					=> Mage::helper('integracionequis')->__('Website'),
        );
    }
}
