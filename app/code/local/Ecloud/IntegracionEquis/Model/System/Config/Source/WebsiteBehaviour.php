<?php
class Ecloud_IntegracionEquis_Model_System_Config_Source_WebsiteBehaviour
{
	public function toOptionArray()
	{
		$options = array(
			array('value' => '0', 'label' => 'No actualizar el producto'),
			array('value' => '1', 'label' => 'Actualizar el website del producto'),
			array('value' => '2', 'label' => 'Ignorar el website y actualizar el producto'),
		);
		return $options;
	}
}
