<?php

class Ecloud_IntegracionEquis_Adminhtml_IntegracionEquisController extends Mage_Adminhtml_Controller_Action
{
    public function runNowAction()
    {
        $shellPath = Mage::getBaseDir() . "/shell/ecloud/equis/config-sync.php";
        $output = array();
        // Obtiene el comando de ejecución de la config
        $command = Mage::getStoreConfig("equisconfig/credentials/command");
        $command = (!$command || $command == "") ? "php" : trim($command);
        $command .= " " . $shellPath;
        // Ejecuta la integración y captura la salida
        exec($command, $output);
        Mage::getSingleton('adminhtml/session')->addWarning($output[0]);

        $this->_redirectReferer();
    }

    public function forceCreateAction()
    {
        $shellPath = Mage::getBaseDir() . "/shell/ecloud/equis/force-create.php";
        $output = array();
        // Obtiene el comando de ejecución de la config
        $command = Mage::getStoreConfig("equisconfig/credentials/command");
        $command = (!$command || $command == "") ? "php" : trim($command);
        $command .= " " . $shellPath;
        // Ejecuta la integración y captura la salida
        exec($command, $output);
        Mage::getSingleton('adminhtml/session')->addWarning($output[0]);

        $this->_redirectReferer();
    }

}

