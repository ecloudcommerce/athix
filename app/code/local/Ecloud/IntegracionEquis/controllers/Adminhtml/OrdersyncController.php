<?php

class Ecloud_IntegracionEquis_Adminhtml_OrdersyncController extends Mage_Adminhtml_Controller_Action
{
	public function indexAction()
	{
		$orderId = $this->getRequest()->getParam("order_id");
		$order = Mage::getModel("sales/order")->load($orderId);
		Mage::dispatchEvent("sales_order_force_sync", array("order" => $order));

        $this->_redirect('/sales_order/view/', array("order_id" => $orderId));
	}
}
