<?php


class Ids_Andreani_Helper_Curl extends Mage_Core_Helper_Abstract
{
	const BASE_HEADERS = array();

	protected $baseUrl;
	protected $appKey;
	protected $appToken;
	protected $headers;

	public function init($baseUrl)
	{
		$this->baseUrl = $baseUrl;
		$this->headers = self::BASE_HEADERS;
		return $this;
	}

	public function removeHeader($headerName)
	{
		unset($this->headers[$headerName]);
	}

	public function addHeader($headerName, $headerValue)
	{
		$this->headers[$headerName] = $headerValue;
		return $this;
	}

	/* REQUEST FUNCTIONS */

	/**
	 * @param Array $request curl options array
	 * @param bool $customHandle if true, returns raw data, error and status, if false, returns parsed json response or throws exception handling
	 * @param bool $getHeaders if true, returns raw data, adding headers to the response
	 * @return Array
	 * @throws Exception
	 */
	public function execute($optionArray, $customHandle = false, $getHeaders = false)
	{
		return self::staticExecute($optionArray, $customHandle, $getHeaders);
	}

	/**
	 * @param Array $request curl options array
	 * @param bool $customHandle if true, returns raw data, error and status, if false, returns parsed json response or throws exception handling
	 * @param bool $getHeaders if true, returns raw data, adding headers to the response
	 * @return Array
	 * @throws Exception
	 */
	public static function staticExecute($optionArray, $customHandle = false, $getHeaders = false)
	{
		$curl = curl_init();

		curl_setopt_array($curl, $optionArray);

		if ($getHeaders) {
			$rawResponse = curl_exec($curl);
			$curlError = curl_error($curl);
			$header_size = curl_getinfo($curl, CURLINFO_HEADER_SIZE);
			$curlHeaders = substr($rawResponse, 0, $header_size);
			$curlResponse = substr($rawResponse, $header_size);
		} else {
			$curlResponse = curl_exec($curl);
			$curlError = curl_error($curl);
			$curlStatus = curl_getinfo($curl, CURLINFO_HTTP_CODE);
			$curlHeaders = null;
		}

		curl_close($curl);

		if ($customHandle || $getHeaders) {
			return array(
				$curlStatus,
				$curlResponse,
				$curlError,
				$curlHeaders,
			);
		}

		if ($curlError && $curlError != "") {
			throw new Exception($curlError);
		}

		if ($curlStatus < 200 || $curlStatus > 299) {
			throw new Exception("Status: " . $curlStatus);
		}

		try {
			$responseObj = json_decode($curlResponse, true);
			return $responseObj;
		} catch (Exception $e) {
			throw new Exception("Response is not json: " . $curlResponse);
		}
	}

	/**
	 * @param string $method HTTP method name (GET|POST|PUT|DELETE|PATCH)
	 * @param string $url full URL to request
	 * @param string|null the json parsed body of the request (default = null)
	 * @param bool if true, the request will return headers along with the response (default = false)
	 * @return Array
	 */
	public function getRequest($method, $url, $body = null, $returnHeaders = false, $userPwd = null)
	{
		return $this->prepareRequest($method, $url, $body, $this->headers, $returnHeaders, $userPwd);
	}

	public static function prepareRequest($method, $url, $body = null, $headers, $returnHeaders = false, $userPwd = null)
	{
		$headerString = array_reduce(array_keys($headers), function ($carry, $headerName) use ($headers) {
			$carry .= "$headerName: {$headers[$headerName]}";
			return $carry;
		}, "");
		return array(
			CURLOPT_URL => $url,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => 'CURL_HTTP_VERSION_1_1',
			CURLOPT_CUSTOMREQUEST => strtoupper($method),
			CURLOPT_USERPWD => $userPwd,
			CURLOPT_POSTFIELDS => $body,
			CURLOPT_HTTPHEADER => [$headerString],
			CURLOPT_HEADER => $returnHeaders
		);
	}

	/**
	 * @param string $headerName
	 * @param string $allHeaders
	 * @return string
	 */
	public static final function getHeader($headerName, $allHeaders)
	{
		$initialPos = strpos($allHeaders, $headerName) + strlen($headerName . ": ");
		$finalPos = strpos($allHeaders, "\n", $initialPos);
		if ($finalPos === false) $finalPos = null;
		$myHeader = substr($allHeaders, $initialPos, ($finalPos) - $initialPos);
		return $myHeader;
	}

	/* -REQUEST FUNCTIONS- */


	/* URL FUNCTIONS */

	public function getBaseUrl($uri, $params = array())
	{
		return $this->getUrl($this->baseUrl, $uri, $params);
	}

	public function getUrl($baseUrl, $uri, $params)
	{
		$url = $baseUrl . $uri;
		$char = "?";
		foreach ($params as $name => $value) {
			if (!$value) continue;
			$url .= $char . $name . "=" . $value;
			$char = "&";
		}
		return str_replace(" ", "%20", $url);
	}

	/* -URL FUNCTIONS- */

	public static final function strContains($needle, $haystack)
	{
		$pos = strpos($haystack, $needle);
		return !($pos === false);
	}
}
