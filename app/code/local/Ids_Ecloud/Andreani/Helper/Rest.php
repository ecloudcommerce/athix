<?php


class Ids_Andreani_Helper_Rest extends Mage_Core_Helper_Abstract
{
	const URL_LOGIN = "login/";
	const URL_COTIZAR = "v1/tarifas";
	const URL_CREAR_ENVIO = "v2/ordenes-de-envio";
	const URL_CONSULTAR_SUCURSALES = "v2/sucursales";
	const URL_ETIQUETA = "v2/ordenes-de-envio/{agrupador}/etiquetas";
	const URL_TRAZABILIDAD = "envios/{numeroAndreani}/trazas";

	const LOGIN_HEADER_NAME = "x-authorization-token";

	protected $loginHeader = null;
	protected $curlHelper;

	public function __construct()
	{
		if(Mage::getStoreConfig("carriers/andreaniconfig/testmode")){
			$base_url = Mage::getStoreConfig("carriers/andreaniconfig/wstesting");
		}
		else{
			$base_url = Mage::getStoreConfig("carriers/andreaniconfig/wsprod");
		}
		$this->curlHelper = Mage::helper("andreani/curl")->init($base_url);
	}

	public function loginHeader()
	{
		$this->curlHelper->removeHeader(self::LOGIN_HEADER_NAME);

		$uri = self::URL_LOGIN;
		$url = $this->curlHelper->getBaseUrl($uri);

		$username = Mage::getStoreConfig("carriers/andreaniconfig/usuario");
		$password = Mage::getStoreConfig("carriers/andreaniconfig/password");

		$userPwd = $username . ":" . $password;
		$request = $this->curlHelper->getRequest("GET", $url, null, true, $userPwd);

		try {
			list($status, $response, $error, $headers) = $this->curlHelper->execute($request, false, true);


			if ($error && $error != "") {
				throw new Exception("Error en login. $error");
			}

			if (!$response || $response == "") {
				return [];
			}

			if (strpos($headers, self::LOGIN_HEADER_NAME) === false) {
				throw new Exception("Error en login. Falta el token de autorizacion en la respuesta");
			}

			$this->loginHeader = $this->curlHelper->getHeader(self::LOGIN_HEADER_NAME, $headers);
			$this->curlHelper->addHeader(self::LOGIN_HEADER_NAME, $this->loginHeader);
		} catch (Exception $e) {
			throw new Exception("Error en login. {$e->getMessage()}");
		}
	}

	public function login()
	{
		$this->curlHelper->removeHeader(self::LOGIN_HEADER_NAME);

		$uri = self::URL_LOGIN;
		$url = $this->curlHelper->getBaseUrl($uri);

		$username = Mage::getStoreConfig("carriers/andreaniconfig/usuario");
		$password = Mage::getStoreConfig("carriers/andreaniconfig/password");

		$userPwd = $username . ":" . $password;
		$request = $this->curlHelper->getRequest("GET", $url, null, false, $userPwd);

		try {
			$response = $this->curlHelper->execute($request);

			if (!array_key_exists("token", $response)) {
				throw new Exception("Error en login. No se puede obtener el token de autenticación");
			}

			$this->loginHeader = $response["token"];
			$this->curlHelper->addHeader(self::LOGIN_HEADER_NAME, $this->loginHeader);
		} catch (Exception $e) {
			throw new Exception("Error en login. {$e->getMessage()}");
		}
	}

	public function cotizar($bultos)
	{
		if (!$this->loginHeader) {
			$this->login();
		}

		try {
			$uri = SELF::URL_COTIZAR;
			$queryParams = array();
			foreach ($bultos as $key => $value) {
				if (!is_array($value)) {
					$queryParams[$key] = $value;
				} else {
					foreach ($value as $index => $bultoData) {
						foreach ($bultoData as $bultoKey => $bultoValue) {
							$newKey = $key . "[$index][$bultoKey]";
							$queryParams[$newKey] = $bultoValue;
						}
					}
				}
			}
			$url = $this->curlHelper->getBaseUrl($uri, $queryParams);
			$request = $this->curlHelper->getRequest("GET", $url);
		} catch (Exception $e) {
			throw new Exception("Error en cotizador. No se pueden procesar los parámetros: {$e->getMessage()}");
		}

		try {
			return $this->curlHelper->execute($request);
		} catch (Exception $e) {
			throw new Exception("Error en cotizador. {$e->getMessage()}");
		}
	}

	public function consultarSucursales($codigoPostal = null, $localidad = null, $provincia = null)
	{
		if (!$this->loginHeader) {
			$this->login();
		}

		$uri = SELF::URL_CONSULTAR_SUCURSALES;
		$queryParams = array(
			"codigoPostal" => $codigoPostal,
			"localidad" => $localidad,
			"provincia" => $provincia
		);
		$url = $this->curlHelper->getBaseUrl($uri, $queryParams);
		$request = $this->curlHelper->getRequest("GET", $url);

		try {
			return $this->curlHelper->execute($request);
		} catch (Exception $e) {
			throw new Exception("Error en consultar sucursales. {$e->getMessage()}");
		}
	}

	public function crearEnvio($envio)
	{

		if (!$this->loginHeader) {
			$this->login();
		}

		$uri = SELF::URL_CREAR_ENVIO;

		$url = $this->curlHelper->getBaseUrl($uri);
		$request = $this->curlHelper->getRequest("POST", $url, json_encode($envio));

		$sucursales = $this->curlHelper->execute($request);
		return $sucursales;
	}

	public function getEtiqueta($agrupador, $numBulto)
	{

		if (!$this->loginHeader) {
			$this->login();
		}

		$uri = str_replace("{agrupador}", $agrupador, self::URL_ETIQUETA);
		$queryParams = array(
			"bulto" => $numBulto,
		);
		$url = $this->curlHelper->getBaseUrl($uri, $queryParams);
		$request = $this->curlHelper->getRequest("GET", $url);

		list($status, $response, $error, $headers) = $this->curlHelper->execute($request, true);
		if ($error && $error != "") {
			throw new Exception($error);
		}

		if ($status < 200 || $status > 299) {
			throw new Exception("Status: " . $status);
		}

		return $response;
	}

	public function obtenerTrazabilidad($numeroAndreani)
	{
		if (!$this->loginHeader) {
			$this->login();
		}

		$uri = str_replace("{agrupador}", $numeroAndreani, self::URL_TRAZABILIDAD);
		$url = $this->curlHelper->getBaseUrl($uri);
		$request = $this->curlHelper->getRequest("GET", $url);

		try {
			return $this->curlHelper->execute($request);
		} catch (Exception $e) {
			throw new Exception("Error en consultar sucursales. {$e->getMessage()}");
		}
	}
}
