<?php require_once Mage::getBaseDir('lib') . '/Andreani/wsseAuth.php';
class Ids_Andreani_Model_Carrier_Andreani extends Mage_Shipping_Model_Carrier_Abstract implements Mage_Shipping_Model_Carrier_Interface {

    protected $_code = '';
    protected $distancia_final_txt  = '';
    protected $duracion_final       = '';
    protected $mode  = '';
    protected $envio = '';

    /**
     * Recoge las tarifas del método de envío basados ​​en la información que recibe de $request
     *
     * @param Mage_Shipping_Model_Rate_Request $data
     * @return Mage_Shipping_Model_Rate_Result
     */

    public function collectRates(Mage_Shipping_Model_Rate_Request $request) {
        $datos["peso"]              = 0;
        $datos["valorDeclarado"]    = 0;
        $datos["volumen"]           = 0;
        $detalleProductos           = array();
        $sku                        = "";
        $freeBoxes                  = 0;
        $pesoMaximo = Mage::getStoreConfig('carriers/andreaniconfig/pesomax',Mage::app()->getStore());

        Mage::getSingleton('core/session')->unsAndreani();

        // Reiniciar variable Sucursales para descachear las Sucursales.
        if(!Mage::getStoreConfig('carriers/andreaniconfig/cache',Mage::app()->getStore())) {
            Mage::getSingleton('core/session')->unsSucursales();
        }

        // Tomamos el attr "medida" segun la configuracion del cliente
        if (Mage::getStoreConfig('carriers/andreaniconfig/medida',Mage::app()->getStore())=="") {
            $datos["medida"] = "gramos";
        } else {
            $datos["medida"] = Mage::getStoreConfig('carriers/andreaniconfig/medida',Mage::app()->getStore());
        }

        if ($datos["medida"]=="kilos") {
            $datos["medida"] = 1;
        } elseif ($datos["medida"]=="gramos") {
            $datos["medida"] = 1000;
        } else {
            $datos["medida"] = 1000; //si está vacio: "gramos"
        }
        foreach ($request->getAllItems() as $_item) {
            if($sku != $_item->getSku()) {
                $sku                     = $_item->getSku();
                $price           = floor($_item->getPrice());
                $datos["peso"]           = ($_item->getQty() * $_item->getWeight() / $datos["medida"]) + $datos["peso"];
                $datos["valorDeclarado"] = ($_item->getQty() * $price) + $datos["valorDeclarado"];

                $product = Mage::getModel('catalog/product')->loadByAttribute('sku', $_item->getSku(), array('volumen'));
                $datos["volumen"] += ($_item->getQty() * $product->getVolumen());

                //$datos["volumenstring"] = $_item->getQty() . " x " . $product->getVolumen()  . " x " .  $datos["medida"];
                //Mage::log("Volumen String: " . print_r($datos["volumenstring"],true));
                //Mage::log("Volumen: " . print_r($datos["volumen"],true));

                // Creamos un array con el detalle de cada producto
                $detalleProductos[] = "(" . $_item->getQty() . ") " .$_item->getName();
                // Si la condicion de free shipping está seteada en el producto
                if ($_item->getFreeShippingDiscount() && !$_item->getProduct()->isVirtual()) {
                    Mage::log("getFreeShippingDiscount: " . print_r($_item->getQty(),true));
                    $freeBoxes += $_item->getQty();
                }
            }
        }

        //Detalle de productos
        $datos["DetalleProductos"] = implode(' + ',$detalleProductos);

        // Seteamos las reglas
        if(isset($freeBoxes))   $this->setFreeBoxes($freeBoxes);

        $cart   = Mage::getSingleton('checkout/cart');
        $quote  = $cart->getQuote();
        $shippingAddress        = $quote->getShippingAddress();
        $datos["cpDestino"]     = intval($request->getDestPostcode());
        $datos["localidad"]     = $request->getDestCity();
        $datos["provincia"]     = $request->getDestRegionCode();
        $datos["direccion"]     = $request->getDestStreet();
        $datos["nombre"]        = $shippingAddress->getData('firstname');
        $datos["apellido"]      = $shippingAddress->getData('lastname');
        $datos["telefono"]      = $shippingAddress->getData('telephone');
        $datos["email"]         = $shippingAddress->getData('email');
        $datos["dni"]           = ($shippingAddress->getData('dni'))? $shippingAddress->getData('dni') : '';

        $datos["username"]      = Mage::getStoreConfig('carriers/andreaniconfig/usuario',Mage::app()->getStore());
        $datos["password"]      = Mage::getStoreConfig('carriers/andreaniconfig/password',Mage::app()->getStore());
        $datos["cliente"]       = Mage::getStoreConfig('carriers/andreaniconfig/nrocliente',Mage::app()->getStore());
        $datos["contrato"]      = Mage::getStoreConfig('carriers/andreaniconfig/contrato',Mage::app()->getStore());

        $result = Mage::getModel('shipping/rate_result');
        $method = Mage::getModel('shipping/rate_result_method');

        $error_msg = Mage::helper('andreani')->__("Completá los datos para poder calcular el costo de su pedido.");

        // Optimizacion con OneStepCheckout
        if ($datos["cpDestino"]=="" && $datos["localidad"]=="" && $datos["provincia"]=="" && $datos["direccion"]=="") {
            $error = Mage::getModel('shipping/rate_result_error');
            $error->setCarrier($this->_code);
            $error->setCarrierTitle($this->getConfigData('title'));
            $error->setErrorMessage($error_msg);
            return $error;
        }

        $error_msg = Mage::helper('andreani')->__("Su pedido supera el peso máximo permitido por Andreani. Por favor divida su orden en más pedidos o consulte al administrador de la tienda. Gracias y disculpe las molestias.");

        if ($this->_code == "andreaniestandar" & Mage::getStoreConfig('carriers/andreaniestandar/active',Mage::app()->getStore()) == 1) {

            // Esta validacion no se hace en M2, habria que volver a activarla si mandamos un unico bulto para el envio?
            // if($datos["peso"] >= $pesoMaximo){
            //     $error = Mage::getModel('shipping/rate_result_error');
            //     $error->setCarrier($this->_code);
            //     $error->setCarrierTitle($this->getConfigData('title'));
            //     $error->setErrorMessage($error_msg);
            //     return $error;
            // } else {
                $response = $this->_getAndreaniEstandar($datos,$request);
                if(is_string($response)){
                    $error = Mage::getModel('shipping/rate_result_error');
                    $error->setCarrier($this->_code);
                    $error->setCarrierTitle($this->getConfigData('title'));
                    $error->setErrorMessage($response);
                    return $error;
                } else {
                    $result->append($response);
                }
            // }
        }
        if ($this->_code == "andreaniurgente" & Mage::getStoreConfig('carriers/andreaniurgente/active',Mage::app()->getStore()) == 1) {
            // if($datos["peso"] >= $pesoMaximo){
            //     $error = Mage::getModel('shipping/rate_result_error');
            //     $error->setCarrier($this->_code);
            //     $error->setCarrierTitle($this->getConfigData('title'));
            //     $error->setErrorMessage($error_msg);
            //     return $error;
            // } else {
                $response = $this->_getAndreaniUrgente($datos,$request);
                if(is_string($response)){
                    $error = Mage::getModel('shipping/rate_result_error');
                    $error->setCarrier($this->_code);
                    $error->setCarrierTitle($this->getConfigData('title'));
                    $error->setErrorMessage($response);
                    return $error;
                } else {
                    $result->append($response);
                }
            // }
        }
        if ($this->_code == "andreanisucursal" & Mage::getStoreConfig('carriers/andreanisucursal/active',Mage::app()->getStore()) == 1) {
            // if($datos["peso"] >= $pesoMaximo){
            //     $error = Mage::getModel('shipping/rate_result_error');
            //     $error->setCarrier($this->_code);
            //     $error->setCarrierTitle($this->getConfigData('title'));
            //     $error->setErrorMessage($error_msg);
            //     return $error;
            // } else {
                if(!Mage::getStoreConfig('carriers/andreaniconfig/ws_cache',Mage::app()->getStore()))
                {
                    $response = $this->_getAndreaniSucursal($datos,$request);
                }
                else
                {
                    $response = $this->_getAndreaniSucursalDummy($datos,$request);
                }

                if(is_string($response)){
                    $error = Mage::getModel('shipping/rate_result_error');
                    $error->setCarrier($this->_code);
                    $error->setCarrierTitle($this->getConfigData('title'));
                    $error->setErrorMessage($response);
                    return $error;
                } else {
                    $result->append($response);
                }
            // }
        }

        return $result;
    }

    /**
     * Arma el precio y la información del servicio "Estandar" de Andreani según el parametro $data
     *
     * @param Datos del usuario y el carrito de compras $data
     * @return Los datos para armar el Método de envío $rate
     */
    protected function _getAndreaniEstandar($datos,$request){
        Mage::log("Andreani Estandar");
        $rate = Mage::getModel('shipping/rate_result_method');
        /* @var $rate Mage_Shipping_Model_Rate_Result_Method */
        $rate->setCarrier($this->_code);
        $rate->setCarrierTitle("Andreani");
        $rate->setMethod($this->_code);

        $datos["contrato"]      = Mage::getStoreConfig('carriers/andreaniestandar/contrato',Mage::app()->getStore());

        if (Mage::getStoreConfig('carriers/andreaniconfig/testmode',Mage::app()->getStore()) == 1) {
            $datos["urlCotizar"]        = Mage::helper('andreani')->getWSMethodUrl(Ids_Andreani_Helper_Data::COTIZACION,Ids_Andreani_Helper_Data::ENVMODTEST);
            $datos["urlSucursal"]       = Mage::helper('andreani')->getWSMethodUrl(Ids_Andreani_Helper_Data::SUCURSALES,Ids_Andreani_Helper_Data::ENVMODTEST);
        } else {
            $datos["urlCotizar"]        = Mage::helper('andreani')->getWSMethodUrl(Ids_Andreani_Helper_Data::COTIZACION,Ids_Andreani_Helper_Data::ENVMODPROD);
            $datos["urlSucursal"]       = Mage::helper('andreani')->getWSMethodUrl(Ids_Andreani_Helper_Data::SUCURSALES,Ids_Andreani_Helper_Data::ENVMODPROD);
        }

        if($request->getFreeShipping() == true || $request->getPackageQty() == $this->getFreeBoxes()) {
            // Envio gratis
            $shippingPrice = '0.00';
            $texto = Mage::helper('andreani')->__('Envío gratis.');
        }
        else if(Mage::getStoreConfig('carriers/andreaniestandar/cacheactive',Mage::app()->getStore())==1) {
            // Cache activada
            $shippingPrice = (float)Mage::getStoreConfig('carriers/andreaniestandar/cacheaverage',Mage::app()->getStore());
            $texto  = Mage::getStoreConfig('carriers/andreaniestandar/description',Mage::app()->getStore());
        }
        else{
            // Cotizar con API
            $shippingPrice = $this->cotizarEnvio($datos);
            if ($shippingPrice == 0) {
                return $texto  = Mage::helper('andreani')->__("Error en la conexión con Andreani. Por favor chequee los datos ingresados en la información de envio y vuelva a intentar.");
            } else {
                $texto  = Mage::getStoreConfig('carriers/andreaniestandar/description',Mage::app()->getStore());
            }
            $shippingPrice = $this->getFinalPriceWithHandlingFee($shippingPrice);
        }

        $datos["precio"] = $shippingPrice;
        Mage::getSingleton('core/session')->setAndreaniEstandar($datos);
        $shippingPrice = $shippingPrice + ($shippingPrice * Mage::getStoreConfig('carriers/andreaniestandar/regla') / 100);
        $rate->setMethodTitle($texto);
        $rate->setPrice($shippingPrice);
        $rate->setCost($shippingPrice);

        return $rate;
    }

    /**
     * Arma el precio y la información del servicio "Urgente" de Andreani según el parametro $data
     *
     * @param Datos del usuario y el carrito de compras $data
     * @return Los datos para armar el Método de envío $rate
     */
    protected function _getAndreaniUrgente($datos,$request){
        Mage::log("Andreani Urgente");

        $rate = Mage::getModel('shipping/rate_result_method');
        /* @var $rate Mage_Shipping_Model_Rate_Result_Method */
        $rate->setCarrier($this->_code);
        $rate->setCarrierTitle("Andreani");
        $rate->setMethod($this->_code);

        $datos["contrato"]      = Mage::getStoreConfig('carriers/andreaniurgente/contrato',Mage::app()->getStore());

        if (Mage::getStoreConfig('carriers/andreaniconfig/testmode',Mage::app()->getStore()) == 1) {
            $datos["urlCotizar"]        = Mage::helper('andreani')->getWSMethodUrl(Ids_Andreani_Helper_Data::COTIZACION,Ids_Andreani_Helper_Data::ENVMODTEST);
            $datos["urlSucursal"]       = Mage::helper('andreani')->getWSMethodUrl(Ids_Andreani_Helper_Data::SUCURSALES,Ids_Andreani_Helper_Data::ENVMODTEST);
        } else {
            $datos["urlCotizar"]        = Mage::helper('andreani')->getWSMethodUrl(Ids_Andreani_Helper_Data::COTIZACION,Ids_Andreani_Helper_Data::ENVMODPROD);
            $datos["urlSucursal"]       = Mage::helper('andreani')->getWSMethodUrl(Ids_Andreani_Helper_Data::SUCURSALES,Ids_Andreani_Helper_Data::ENVMODPROD);
        }


        //Si la config "Cache Activada" esta como Activa el precio del envio es el de la config "Cache Valor Promedio". Evita consultar la api de andreani.
        if (Mage::getStoreConfig('carriers/andreaniurgente/cacheactive',Mage::app()->getStore())==1) {
            $priceConfig = (float)Mage::getStoreConfig('carriers/andreaniurgente/cacheaverage',Mage::app()->getStore());

            $rate->setPrice($priceConfig);
            $rate->setCost($priceConfig);
            $texto  = Mage::getStoreConfig('carriers/andreaniurgente/description',Mage::app()->getStore());
            $rate->setMethodTitle($texto);

        }else{
            // Buscamos en eAndreani el costo del envio segun los parametros enviados
            $datos["precio"]                = $this->cotizarEnvio($datos);
            // Estos datos no se obtienen en la respuesta de la API REST
            // $datos["CategoriaDistanciaId"]  = $this->envio->CategoriaDistanciaId;
            // $datos["CategoriaPeso"]         = $this->envio->CategoriaPeso;

            Mage::getSingleton('core/session')->setAndreaniUrgente($datos);

            if ($datos["precio"] == 0) {
                return $texto  = Mage::helper('andreani')->__("Error en la conexión con Andreani. Por favor chequee los datos ingresados en la información de envio y vuelva a intentar.");
            } else {
                $texto  = Mage::getStoreConfig('carriers/andreaniurgente/description',Mage::app()->getStore());
            }

            $rate->setMethodTitle($texto);

            if($request->getFreeShipping() == true || $request->getPackageQty() == $this->getFreeBoxes()) {
                $shippingPrice = '0.00';
                // cambiamos el titulo para indicar que el envio es gratis
                $rate->setMethodTitle(Mage::helper('andreani')->__('Envío gratis.'));
            } else {
                $shippingPrice = $this->getFinalPriceWithHandlingFee($datos["precio"]);
            }

            $shippingPrice = $shippingPrice + ($shippingPrice * Mage::getStoreConfig('carriers/andreaniurgente/regla') / 100);

            $rate->setPrice($shippingPrice);
            $rate->setCost($shippingPrice);
        }

        return $rate;
    }

    /**
     * Arma el precio y la información del servicio "Sucursal" de Andreani según el parametro $data
     *
     * @param Datos del usuario y el carrito de compras $data
     * @return Los datos para armar el Método de envío $rate
     */
    protected function _getAndreaniSucursal($datos,$request){


        $rate = Mage::getModel('shipping/rate_result_method');
        /* @var $rate Mage_Shipping_Model_Rate_Result_Method */
        $rate->setCarrier($this->_code);
        $rate->setCarrierTitle("Andreani");
        $rate->setMethod($this->_code);
        $metodo = Mage::getStoreConfig('carriers/andreaniconfig/metodo',Mage::app()->getStore());

        $datos["contrato"]      = Mage::getStoreConfig('carriers/andreanisucursal/contrato',Mage::app()->getStore());

        if (Mage::getStoreConfig('carriers/andreaniconfig/testmode',Mage::app()->getStore()) == 1) {
            $datos["urlCotizar"]        = Mage::helper('andreani')->getWSMethodUrl(Ids_Andreani_Helper_Data::COTIZACION,Ids_Andreani_Helper_Data::ENVMODTEST);
            $datos["urlSucursal"]       = Mage::helper('andreani')->getWSMethodUrl(Ids_Andreani_Helper_Data::SUCURSALES,Ids_Andreani_Helper_Data::ENVMODTEST);
        } else {
            $datos["urlCotizar"]        = Mage::helper('andreani')->getWSMethodUrl(Ids_Andreani_Helper_Data::COTIZACION,Ids_Andreani_Helper_Data::ENVMODPROD);
            $datos["urlSucursal"]       = Mage::helper('andreani')->getWSMethodUrl(Ids_Andreani_Helper_Data::SUCURSALES,Ids_Andreani_Helper_Data::ENVMODPROD);
        }

        if (Mage::getStoreConfig('carriers/andreanisucursal/cacheactive',Mage::app()->getStore())==1) {
            // Cache activada
            $shippingPrice = (float)Mage::getStoreConfig('carriers/andreanisucursal/cacheaverage',Mage::app()->getStore());
            $texto  = Mage::getStoreConfig('carriers/andreanisucursal/description',Mage::app()->getStore());

            $datos["sucursalRetiro"] = "";  // Se llena en base a configs
            $datos["DireccionSucursal"] = "";   // Se llena en base a configs

            if($request->getFreeShipping() == true || $request->getPackageQty() == $this->getFreeBoxes()) {
                // Envio gratis
                $shippingPrice = '0.00';

                $direSucu  = " Sucursal: " . $datos["DireccionSucursal"];
                $texto = Mage::helper('andreani')->__('Envío gratis.') . $direSucu;
            }
        }
        else {
            $sucursal = $this->consultarSucursales($datos,"sucursal");

            if($sucursal=="nosucursal") {
                return "No hay sucursales cerca de tu domicilio.";
            } elseif (count((array)$sucursal) == 0) {
                return "Lo siento ha fallado la comunicación con Andreani, por favor vuelve a intentarlo.";
            }

            $datos["sucursalRetiro"] = $sucursal["id"];
            $datos["DireccionSucursal"] = $sucursal["direccion"]["calle"] . " " . $sucursal["direccion"]["numero"] . " " . $sucursal["direccion"]["localidad"] . " " . $sucursal["direccion"]["provincia"];

            if($request->getFreeShipping() == true || $request->getPackageQty() == $this->getFreeBoxes()) {
                // Envio gratis
                $shippingPrice = '0.00';

                $direSucu  = " Sucursal: {$sucursal["descripcion"]} ({$datos['DireccionSucursal']}).";
                $texto = Mage::helper('andreani')->__('Envío gratis.') . $direSucu;
            }
            else{
                // Cotizar con API
                // Buscamos en eAndreani el costo del envio segun los parametros enviados
                $shippingPrice = $this->cotizarEnvio($datos);
                
                if ($shippingPrice == 0) {
                    return Mage::helper('andreani')->__("Error en la conexión con Andreani. Por favor chequee los datos ingresados en la información de envio y vuelva a intentar.");
                } else {
                    $texto  = Mage::getStoreConfig('carriers/andreanisucursal/description',Mage::app()->getStore()) . " {$sucursal["descripcion"]} ({$datos["DireccionSucursal"]}).";
                }
                $shippingPrice = $this->getFinalPriceWithHandlingFee($shippingPrice);
            }
        }

        $datos["precio"] = $shippingPrice;
        Mage::getSingleton('core/session')->setAndreaniSucursal($datos);
        $shippingPrice = $shippingPrice + ($shippingPrice * Mage::getStoreConfig('carriers/andreanisucursal/regla') / 100);
        $rate->setMethodTitle($texto);
        $rate->setPrice($shippingPrice);
        $rate->setCost($shippingPrice);
        return $rate;
    }

    protected function _getAndreaniSucursalDummy($datos,$request){
        $shipping_amount = Mage::getSingleton('core/session')->getData('shipping_amount');
        $shipping_description = Mage::getSingleton('core/session')->getData('shipping_description');
        $shipping_method = Mage::getSingleton('checkout/session')->getQuote()->getShippingAddress()->getShippingMethod();

        $rate = Mage::getModel('shipping/rate_result_method');
        /* @var $rate Mage_Shipping_Model_Rate_Result_Method */
        $rate->setCarrier($this->_code);
        $rate->setCarrierTitle("Andreani");
        $rate->setMethod($this->_code);
        $metodo = Mage::getStoreConfig('carriers/andreaniconfig/metodo',Mage::app()->getStore());

        $datos["contrato"]      = Mage::getStoreConfig('carriers/andreanisucursal/contrato',Mage::app()->getStore());


            // Estos datos no se obtienen en la respuesta de la API REST
            // $datos["CategoriaDistanciaId"]  = $this->envio->CategoriaDistanciaId;
            // $datos["CategoriaPeso"]         = $this->envio->CategoriaPeso;

        // Buscamos la sucursal mas cercana del cliente segun el CP ingresado
        $dataSucursalAndreani = array();

        $dataSucursalAndreani['cpDestino'] = Mage::getSingleton('core/session')->getAndreaniSucursal()['cpDestino'];
        $datos['cpDestino'] = Mage::getSingleton('core/session')->getAndreaniSucursal()['cpDestino'];
        $sucursal             = $this->consultarSucursales($dataSucursalAndreani,"sucursal");

        if($sucursal=="nosucursal"){
            return "No hay sucursales cerca de tu domicilio.";
        }elseif (count((array)$sucursal) == 0) {
            return "Lo siento ha fallado la comunicación con Andreani, por favor vuelve a intentarlo.";
        }

        $datos["sucursalRetiro"]        = $sucursal["id"];
        $datos["DireccionSucursal"]     = $sucursal["direccion"]["calle"] . " " . $sucursal["direccion"]["numero"] . " " . $sucursal["direccion"]["localidad"] . " " . $sucursal["direccion"]["provincia"];
        $datos["precio"]                = $this->cotizarEnvio($datos);
        Mage::getSingleton('core/session')->setAndreaniSucursal($datos);

        if($shipping_description !='' && $shipping_method == 'andreanisucursal_andreanisucursal')
        {
            $methodTitle = $shipping_description;
            //$rate->setCarrierTitle("Andreani");
        }
        else
        {
            $methodTitle = 'Sucursal Andreani';
        }

        $rate->setMethodTitle($methodTitle);

        if($request->getFreeShipping() == true || $request->getPackageQty() == $this->getFreeBoxes()) {
            $shippingPrice = '0.00';
            // cambiamos el titulo para indicar que el envio es gratis
            $direSucu  = " Sucursal: {$methodTitle}.";
            $rate->setMethodTitle(Mage::helper('andreani')->__('Envío gratis.') );
        } else {
            //$shippingPrice = $this->getFinalPriceWithHandlingFee(0);
            if($shipping_method == 'andreanisucursal_andreanisucursal')
            {
                $shippingPrice = $datos["precio"];
            }else{
                $shippingPrice = 0;
            }

        }

        $shippingPrice = $shippingPrice + ($shippingPrice * Mage::getStoreConfig('carriers/andreanisucursal/regla') / 100);

        $rate->setPrice($shippingPrice);
        $rate->setCost($shippingPrice);

        return $rate;
    }

    /**
     * Get allowed shipping methods
     *
     * @return array
     */
    public function getAllowedMethods() {
        return array($this->_code    => $this->getConfigData('name'));
    }

    /**
     * Cotiza el envio de los productos segun los parametros
     *
     * @param $params
     * @return $costoEnvio
     */
    public function cotizarEnvio($params) {
        try {

            if (Mage::getStoreConfig('carriers/andreaniconfig/testmode',Mage::app()->getStore()) == 1) {
                $urlCotizar     = Mage::helper('andreani')->getWSMethodUrl(Ids_Andreani_Helper_Data::COTIZACION,Ids_Andreani_Helper_Data::ENVMODTEST);
                $soapVersion    = Mage::helper('andreani')->getSoapVersion(Ids_Andreani_Helper_Data::COTIZACION,Ids_Andreani_Helper_Data::ENVMODTEST);
            } else {
                $urlCotizar     = Mage::helper('andreani')->getWSMethodUrl(Ids_Andreani_Helper_Data::COTIZACION,Ids_Andreani_Helper_Data::ENVMODPROD);
                $soapVersion    = Mage::helper('andreani')->getSoapVersion(Ids_Andreani_Helper_Data::COTIZACION,Ids_Andreani_Helper_Data::ENVMODPROD);
            }

            $options = array(
                'soap_version' => $soapVersion,
                'exceptions' => true,
                'trace' => 1,
                'wdsl_local_copy' => true
            );

            $userCredentials = Mage::helper('andreani')->getUserCredentials();

            $wsse_header = new WsseAuthHeader($userCredentials['username'], $userCredentials['password']);
            $client = new SoapClient($urlCotizar, $options);
            $client->__setSoapHeaders(array($wsse_header));
            //$sucursalRetiro     = array('sucursalRetiro' => "");
            //$params = array_merge($sucursalRetiro, $params);
            
            // $phpresponse = $client->CotizarEnvio(array(
            //     'cotizacionEnvio' =>array(
            //         'CPDestino'     =>$params["cpDestino"],
            //         'Cliente'       =>$params["cliente"],
            //         'Contrato'      =>$params["contrato"],
            //         'Peso'          =>$params["peso"],
            //         'SucursalRetiro'=>$params["sucursalRetiro"],
            //         'ValorDeclarado'=>$params["valorDeclarado"],
            //         'Volumen'       =>$params["volumen"]
            //     )));
            // $costoEnvio  = floatval($phpresponse->CotizarEnvioResult->Tarifa);
            // $this->envio = $phpresponse->CotizarEnvioResult;
            // Mage::log("Cotizar envio: " . print_r($phpresponse->CotizarEnvioResult,true));
            
            $sucursalRetiro = array_key_exists("sucursalRetiro", $params) ? $params["sucursalRetiro"] : null;
            try{
                $phpresponse = Mage::helper('andreani/rest')->cotizar(
                    array(
                        'cpDestino'     =>$params["cpDestino"],
                        'contrato'      =>$params["contrato"],
                        'cliente'       =>$params["cliente"],
                        'sucursalOrigen'=>$sucursalRetiro,
                        
                        'bultos'          =>[
                            [
                                "valorDeclarado" => $params['valorDeclarado'],//total de la compra
                                "volumen" => $params['volumen'],//volumen
                                "kilos" => $params['peso']//peso
                            ]
                        ]
                    )
                );
            }
            catch(Exception $e){
                Mage::log($e->getMessage());
                return 0;
            }
            $costoEnvio = $phpresponse["tarifaConIva"]["total"];
            $this->envio = $phpresponse;

            Mage::log("Cotizar envio: " . print_r($costoEnvio,true));

            return $costoEnvio;

        } catch (SoapFault $e) {
            Mage::log("Error: " . $e);
        }
    }

    /**
     * Trae las sucursales de Andreani segun los parametros
     *
     * @param $params
     * @return $costoEnvio
     */
    public function consultarSucursales($params,$modo) {

        try {
            // Nos fijamos si ya consultamos la sucursal en Andreani
            if(is_object(Mage::getSingleton('core/session')->getSucursales())) {
                if($modo != "sucursal") {
                    Mage::log("Ya buscó la sucursal en Andreani");
                    return Mage::getSingleton('core/session')->getSucursales();
                } else {
                    //Mage::getSingleton('core/session')->unsGoogleDistance();
                    Mage::log("Google Distance: " . print_r(Mage::getSingleton('core/session')->getGoogleDistance(),true));
                    if(is_object(Mage::getSingleton('core/session')->getGoogleDistance())) {
                        Mage::log("Ya buscó la sucursal en Google Maps");
                        $this->distancia_final_txt = Mage::getSingleton('core/session')->getDistancia();
                        $this->duracion_final      = Mage::getSingleton('core/session')->getDuracion();
                        $this->mode                = Mage::getSingleton('core/session')->getMode();

                        return Mage::getSingleton('core/session')->getGoogleDistance();
                    }
                }
            }

            // if (is_object($phpresponse->ConsultarSucursalesResult->ResultadoConsultarSucursales)) {
            $sucursalesPorCP = Mage::helper('andreani/rest')->consultarSucursales($params["cpDestino"]);
            if(!is_array($sucursalesPorCP) || !array_key_exists(0, $sucursalesPorCP))
                return [];
            // Devuelve solo la primer sucursal
            $sucursales = $sucursalesPorCP[0];

            // SE PUEDE HACER POR LAS DUDAS: SI NO HAY SUCURSALES FILTRANDO POR CP, CONSULTAR POR LOCALIDAD O PROVINCIA

            Mage::log("Sucursal: " . print_r($sucursales, true));
            Mage::getSingleton('core/session')->setSucursales($sucursales);

            return $sucursales;

        } catch (Exception $e) {
            Mage::log("Error: " . $e);
            //Mage::getSingleton('core/session')->addError('Error en la conexión con eAndreani. Disculpe las molestias.. vuelva a intentar! <br> En caso de persistir el error contacte al administrador de la tienda.');
        }
    }

}

