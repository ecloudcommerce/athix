<?php

/**
 * Created by PhpStorm.
 * Date: 11/08/16
 * Class Ids_Andreani_Model_Config_Metodo
 */

class Ids_Andreani_Model_Config_Documenttype
{

    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        return [
            array("value" => "DNI", "label" => 'DNI'),
            array("value" => "CUIT", "label" => 'CUIT'),
            array("value" => "CUIL", "label" => 'CUIL'),
        ];
    }

    /**
     * Get options in "key-value" format
     *
     * @return array
     */
    public function toArray()
    {
        return [
            'DNI' => 'DNI',
            'CUIT' => 'CUIT',
            'CUIL' => 'CUIL',
        ];
    }
}
