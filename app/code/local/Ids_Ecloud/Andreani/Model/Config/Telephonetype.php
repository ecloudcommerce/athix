<?php

/**
 * Created by PhpStorm.
 * Date: 11/08/16
 * Class Ids_Andreani_Model_Config_Metodo
 */

class Ids_Andreani_Model_Config_Telephonetype
{

    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        return [
            array("value" => 1, "label" => 'Trabajo'),
            array("value" => 2, "label" => 'Celular'),
            array("value" => 3, "label" => 'Casa'),
            array("value" => 4, "label" => 'Otros'),
        ];
    }

    /**
     * Get options in "key-value" format
     *
     * @return array
     */
    public function toArray()
    {
        return [
            1 => 'Trabajo',
            2 => 'Celular',
            3 => 'Casa',
            4 => 'Otros',
        ];
    }
}
