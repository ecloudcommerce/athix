<?php require_once Mage::getBaseDir('lib') . '/Andreani/wsseAuth.php';
class Ids_Andreani_Adminhtml_PedidosController extends Mage_Adminhtml_Controller_Action
{

    public function indexAction()
    {
    	$this->_title($this->__('Andreani'))->_title($this->__('Estado de pedidos de Andreani'));
        $this->loadLayout();
        $this->_setActiveMenu('andreani/andreani');
        $this->_addContent($this->getLayout()->createBlock('andreani/adminhtml_pedidos'));
        $this->renderLayout();
    }

    public function gridAction()
    {
		$this->_title($this->__('Andreani'))->_title($this->__('Estado de pedidos'));
        $this->loadLayout();
        $this->_setActiveMenu('andreani/andreani');
        $this->getResponse()->setBody(
            $this->getLayout()->createBlock('andreani/adminhtml_pedidos_grid')->toHtml()
        );
    }

    public function exportCsvAction()
    {
        $fileName = 'pedidos_andreani.csv';
        $grid = $this->getLayout()->createBlock('andreani/adminhtml_andreani_pedidos_grid');
        $this->_prepareDownloadResponse($fileName, $grid->getCsvFile());
    }

    public function exportExcelAction()
    {
        $fileName = 'pedidos_andreani.xml';
        $grid = $this->getLayout()->createBlock('andreani/adminhtml_andreani_pedidos_grid');
        $this->_prepareDownloadResponse($fileName, $grid->getExcelFile($fileName));
    }

    public function massEliminarAction()
	{
		$ids = $this->getRequest()->getParam('id');
		if(!is_array($ids)) {
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('andreani')->__('Por favor seleccionar una orden!'));
		} else {
			try {
				foreach ($ids as $id) {
					//Mage::getModel('andreani/order')->load($id)->delete();
					Mage::getModel('andreani/order')->load($id)->setData("estado","Eliminada")->save();
				}
				Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('andreani')->__('Se han eliminado %d registro(s).', count($ids)));
			} catch (Exception $e) {
				Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
			}
		}
		$this->_redirect('*/*/index');
	}

	public function massEntregadoAction()
	{
		$ids = $this->getRequest()->getParam('id');

		if(!is_array($ids)) {
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('andreani')->__('Por favor seleccionar una orden!'));
		} else {
			try {
				date_default_timezone_set('America/Argentina/Buenos_Aires');
				$date = date('d/m/Y h:i:s A', time());
				foreach ($ids as $id) {
					Mage::getModel('andreani/order')->load($id)->setData("entrega",$date)->save();
					Mage::getModel('andreani/order')->load($id)->setData("estado","Entregado")->save();
				}
				Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('andreani')->__('Se han actualizado %d registro(s).', count($ids)));
			} catch (Exception $e) {
				Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
			}
		}
		$this->_redirect('*/*/index');
	}

	public function massPendienteAction()
	{
		$ids = $this->getRequest()->getParam('id');
		if(!is_array($ids)) {
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('andreani')->__('Por favor seleccionar una orden!'));
		} else {
			try {
				foreach ($ids as $id) {
					Mage::getModel('andreani/order')->load($id)->setData("estado","Pendiente")->save();
				}
				Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('andreani')->__('Se han actualizado %d registro(s).', count($ids)));
			} catch (Exception $e) {
				Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
			}
		}
		$this->_redirect('*/*/index');
	}

    public function viewAction()
    {
        $id = (int) $this->getRequest()->getParam('id');

        // 1. Traemos los datos de la tabla "andreani_order" según el OrderId[0] y asignarla a $datos
		$collection = Mage::getModel('andreani/order')->getCollection()
        	->addFieldToFilter('id', $id);
        $collection->getSelect()->limit(1);

        if (!$collection) {
        	Mage::log("Andreani :: no existe la orden en la tabla andreani_order.");
        	return;
        }
		$ordenAndreani = null;
        foreach($collection as $thing) {
		    $ordenAndreani = $thing;
		}

		if ( $ordenAndreani->getCodTracking() != '' ) {
			try {
				$restHelper = Mage::helper("andreani/rest");
				try{
					// $trazas = $restHelper->obtenerTrazabilidad($ordenAndreani->getCodTracking());
					$trazas = json_decode('{
						"eventos": [
						  {
							"Fecha": "2021-03-09T11:08:03",
							"Estado": "Pendiente de ingreso",
							"EstadoId": 1,
							"Traduccion": "ENVIO INGRESADO AL SISTEMA",
							"Sucursal": "Sucursal Genérica",
							"SucursalId": 999,
							"Ciclo": "Distribution"
						  },
						  {
							"Fecha": "2021-03-09T11:08:09",
							"Estado": "Ingreso al circuito operativo",
							"EstadoId": 5,
							"Traduccion": "ENVIO INGRESADO AL SISTEMA",
							"Sucursal": "Monserrat",
							"SucursalId": 12,
							"Ciclo": "Distribution"
						  },
						  {
							"Fecha": "2021-03-09T11:53:55",
							"Estado": "En distribución",
							"EstadoId": 6,
							"Traduccion": "ENVIO CON SALIDA A REPARTO",
							"Sucursal": "Monserrat",
							"SucursalId": 12,
							"Ciclo": "Distribution"
						  },
						  {
							"Fecha": "2021-03-09T11:59:04",
							"Estado": "Visita", 
							"EstadoId": 11,  
							"Motivo": "No se encuentra el titular",
							"MotivoId": 36, 
							"Traduccion": "No se encuentra el titular",
							"Sucursal": "Monserrat", 
							"SucursalId": 12,  
							"Ciclo": "Distribution"
						  }
						]
					  }', true);
				}
				catch(Exception $e) {
					Mage::log("Andreani :: No se puede obtener la trazabilidad del pedido.");
					Mage::log(print_r($e,true));
					return;
				}
				
				if(!array_key_exists("eventos", $trazas) || count($trazas["eventos"]) <= 0) {
					Mage::log($trazas["eventos"]);
					Mage::log("Andreani :: No se puede obtener los eventos en la trazabilidad del pedido.");
					return;
				}

				$eventos = $trazas["eventos"];
				$fecha = "";
				if(array_key_exists("Fecha", $eventos[0])) {
					$fecha = $eventos[0]["Fecha"];
				}

				$nombreEnvio = "";
				switch($ordenAndreani->getContrato()){
					case Mage::getStoreConfig('carriers/andreaniestandar/contrato',Mage::app()->getStore()):
						$nombreEnvio = "Domicilio Estandar";
						break;
					case Mage::getStoreConfig('carriers/andreanisucursal/contrato',Mage::app()->getStore()):
						$nombreEnvio = "Entrega Sucursal";
						break;
					case Mage::getStoreConfig('carriers/andreaniurgente/contrato',Mage::app()->getStore()):
						$nombreEnvio = "Entrega Urgente";
						break;
				}

				$texto	=  $ordenAndreani->getNumeroBulto() . "\n";
				$texto .= "Nombre del Envio: " . $nombreEnvio .  "\n";
				$texto .= "Código de tracking: " . $ordenAndreani->getCodTracking() . "\n";
				$texto .= "Fecha de alta: " . $fecha . "\n";
				

				$texto .= "\nEventos: " . "\n\n"; 
				foreach( $eventos as $indice => $valor ) 
				{
					$texto .= "Fecha del evento: " . (array_key_exists("Fecha", $valor) ? $valor["Fecha"] : "") . "\n";
					$texto .= "Estado del envio: " . (array_key_exists("Estado", $valor) ? $valor["Estado"] : "") . "\n";
					$texto .= "Motivo: " . (array_key_exists("Motivo", $valor) ? $valor["Motivo"] : "") . "\n";
					$texto .= "Sucursal: " . (array_key_exists("Sucursal", $valor) ? $valor["Sucursal"] : "") . "\n";
					$texto .= "------------------ \n";
				}
			
				Mage::getModel('andreani/order')->load($id)->setData("tracking",$texto)->save();

			} catch (Exception $e) {
				Mage::log(print_r($e,true));
			}
		} else {
			$texto =  "El envío se encuentra pendiente. Diríjase a 'Ventas->Pedidos' para dar comienzo al proceso cuando el mismo se haya realizado";
			Mage::getModel('andreani/order')->load($id)->setData("tracking",$texto)->save();
		}

        if ($id) {
            $order = Mage::getModel('andreani/order')->load($id);
            if (!$order || !$order->getId()) {
                Mage::getSingleton('adminhtml/session')->addError(Mage::helper('andreani')->__('No se encontró el ID de la orden'));
                $this->_redirect('*/*/');
            }
        }
        
        Mage::register('order_data', $order);
 
		$this->loadLayout();
		$block = $this->getLayout()->createBlock('andreani/adminhtml_pedidos_edit');
		$this->getLayout()->getBlock('content')->append($block);
		$this->renderLayout();
    }

    public function saveAction() {
		if ($data = $this->getRequest()->getPost()) {
			$model = Mage::getModel('andreani/order');
			$model->setData($data)->setId($this->getRequest()->getParam('id'));
			$model->save();
			Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('andreani')->__('El pedido fue editado con éxito.'));
			Mage::getSingleton('adminhtml/session')->setFormData(false);
		}
			
        $this->_redirect('*/*/');
	}

	public function getConstanciaAction() {

		$id = $this->getRequest()->getParam('id');
        $order = Mage::getModel('andreani/order')->load($id);

        $datos = $order->getData();

	
		if (Mage::getStoreConfig('carriers/andreaniconfig/testmode',Mage::app()->getStore()) == 1) {
			$datos["urlConfirmar"]  = Mage::helper('andreani')->getWSMethodUrl(Ids_Andreani_Helper_Data::IMPRESIONCONSTANCIA,Ids_Andreani_Helper_Data::ENVMODTEST);
			$soapVersion  			= Mage::helper('andreani')->getSoapVersion(Ids_Andreani_Helper_Data::IMPRESIONCONSTANCIA,Ids_Andreani_Helper_Data::ENVMODTEST);
		} else {
			$datos["urlConfirmar"]  = Mage::helper('andreani')->getWSMethodUrl(Ids_Andreani_Helper_Data::IMPRESIONCONSTANCIA,Ids_Andreani_Helper_Data::ENVMODPROD);
			$soapVersion	  		= Mage::helper('andreani')->getSoapVersion(Ids_Andreani_Helper_Data::IMPRESIONCONSTANCIA,Ids_Andreani_Helper_Data::ENVMODPROD);
		}


		$datos["username"] = Mage::getStoreConfig('carriers/andreaniconfig/usuario',Mage::app()->getStore());
		$datos["password"] = Mage::getStoreConfig('carriers/andreaniconfig/password',Mage::app()->getStore());


		if ($datos["username"] == "" OR $datos["password"] == "") {
			Mage::log("Andreani :: no existe nombre de usuario o contraseña para eAndreani");
			die('Andreani :: no existe nombre de usuario o contraseña para eAndreani');
		}

		// 2. Conectarse a eAndreani
		try {
			$options = array(
				'soap_version'		=> $soapVersion,
				'exceptions' 		=> true,
				'trace' 			=> 1,
				'wdsl_local_copy'	=> true
			);

			$userCredentials = Mage::helper('andreani')->getUserCredentials();

			$wsse_header = new WsseAuthHeader($userCredentials['username'], $userCredentials['password']);
            $client      = new SoapClient($datos["urlConfirmar"], $options);
            $client->__setSoapHeaders(array($wsse_header));

            $constanciaResponse = $client->ImprimirConstancia(array(
					'entities' =>array(
								'ParamImprimirConstancia' =>array(
										'NumeroAndreani' => $datos['cod_tracking']
									))));
			$ConstanciaURL = $constanciaResponse->ImprimirConstanciaResult->ResultadoImprimirConstancia->PdfLinkFile;

			$this->_redirectUrl($ConstanciaURL);

			Mage::getModel('andreani/order')->load($id)->setData('constancia',$ConstanciaURL)->save();

		} catch (SoapFault $e) {
			Mage::log("Error: " . $e);
			Mage::getSingleton('adminhtml/session')->addError('Error Andreani: '.$e->getMessage().' - Por favor vuelva a intentar en unos minutos.');
			$this->_redirect('*/*/index');			
		}

	}

}
?>