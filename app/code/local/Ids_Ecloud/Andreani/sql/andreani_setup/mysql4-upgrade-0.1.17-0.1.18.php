<?php

$setup = $this;

$setup->startSetup();

$setup->getConnection()
  ->addColumn($setup->getTable('andreani/order'), 'agrupador_bultos', array(
    'type'      => Varien_Db_Ddl_Table::TYPE_TEXT,
    'length'    => '255',
    'nullable' => true,
    'comment'   => 'Agrupador de bultos',
    'label'   => 'Agrupador de bultos'
  ));
$setup->getConnection()
  ->addColumn($setup->getTable('andreani/order'), 'numero_bulto', array(
    'type'      => Varien_Db_Ddl_Table::TYPE_INTEGER,
    'comment'   => 'Numero de bulto',
    'label'   => 'Numero de bulto'
  ));
$setup->getConnection()
  ->addColumn($setup->getTable('andreani/order'), 'totalizador', array(
    'type'      => Varien_Db_Ddl_Table::TYPE_TEXT,
    'length'    => '10',
    'nullable' => true,
    'comment'   => 'Totalizador',
    'label'   => 'Totalizador'
  ));
$setup->endSetup();

$setup->endSetup();
