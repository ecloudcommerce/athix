<?php
/**
 * 
 * @category    Ulula
 * @package     Ulula_Mercadolibre
 * @copyright   Copyright (c) 2017 Ulula IT (http://ulula.net)
 * @author      Gaston De Marsico <gdemarsico@ulula.net>
 */
class Ulula_Mercadolibre_Block_Adminhtml_Catalog_Categories extends Mage_Adminhtml_Block_Widget_Form {

	protected function _prepareForm() {
        parent::_prepareForm();
    }

    public function getCategoriesTree($id = ''){
        $categories = $this->getCategoriesFromMeli($id);    
        return $this->categoriesToTree($categories, $id);
    }

    public function getCategoriesFromMeli($categoryId){
    	$result = Mage::helper('ulula_mercadolibre/api')->getCategory($categoryId);
        return $result['body'];
    }

    private function categoriesToTree($categories=array(), $id){
        $htmlTree = '<ul>';
        $categoryArray = ($id == '')?$categories:$categories['children_categories'];
        foreach ($categoryArray as $category) {
            $htmlTree .= '<li id="'.$category['id'].'" class="jstree-closed">'.$category['name'].'</li>';
        }
        return $htmlTree .'</ul>';
    }
}