<?php
/**
 * 
 * @category    Ulula
 * @package     Ulula_Mercadolibre
 * @copyright   Copyright (c) 2017 Ulula IT (http://ulula.net)
 * @author      Gaston De Marsico <gdemarsico@ulula.net>
 */

class Ulula_Mercadolibre_Block_Adminhtml_Catalog_Product_Tab extends Mage_Adminhtml_Block_Template
implements Mage_Adminhtml_Block_Widget_Tab_Interface
{
    protected $_product;
    /**
     * Set the template for the block
     *
     */
    public function _construct()
    {
        parent::_construct();
        
        $this->setTemplate('ulula/mercadolibre/catalog/product/tab.phtml');

        $this->setChild('shipping', new Ulula_Mercadolibre_Block_Adminhtml_Catalog_Product_Tab_Shipping);
        $this->setChild('store', new Ulula_Mercadolibre_Block_Adminhtml_Catalog_Product_Tab_Store);
    }

    public function getProdcut()
    {
        if (!$this->_product || (!$this->_product instanceof Mage_Catalog_Model_Product)) {
            $this->_product = Mage::registry('current_product');
        }

        return $this->_product;
    }
    
    /**
     * Retrieve the label used for the tab relating to this block
     *
     * @return string
     */
    public function getTabLabel()
    {
        return $this->__('Mercadolibre');
    }
    
    /**
     * Retrieve the title used by this tab
     *
     * @return string
     */
    public function getTabTitle()
    {
        return $this->__('Configure Mercadolibre Options');
    }
    
    /**
     * Determines whether to display the tab
     * Add logic here to decide whether you want the tab to display
     *
     * @return bool
     */
    public function canShowTab()
    {
        return true;
    }
    
    /**
     * Stops the tab being hidden
     *
     * @return bool
     */
    public function isHidden()
    {
        return false;
    }

    public function canList()
    {
        return (bool)($this->getProdcut()->getvisibility() != 1);
    }

    public function getConditionCombo()
    {
        $product = Mage::registry('product');
        $attributteData = array('code'=>'meli_condition', 'label', 'input');
        if (!Mage::helper('ulula_mercadolibre/attribute')->attributeExists($attributteData['code'])) {
            return '';
        }

        $label = Mage::helper('ulula_mercadolibre/product')->getCondition($product);
        $selectedValue = Mage::helper('ulula_mercadolibre/product')
            ->getAttributeValueFromLabel($attributteData["code"], $label);
        $html = '<select id="'.$attributteData["code"].'" name="product['
            .$attributteData["code"].']" class="select '.$required.'">';
        $attribute = $this->getMageAttribute($attributteData['code']);
        $attributeOptions = $attribute->getSource()->getAllOptions();
        foreach ($attributeOptions as $option) {
               $selected = ($option['value'] == $selectedValue)? 'selected="selected"':'';
               $html .= '<option '.$selected.' value="'.$option["value"].'">'.$option["label"].'</option>';
        }

        $attributteData['input'] = $html.'</select>';
        $attributteData['label'] = $attribute->getStoreLabel();
        $attributteData['label'] .= ($isRequired)? '<span class="required">*</span>':'';
        return $attributteData;
    }

    public function getMeliAttributes($_category)
    {
        $attributes = $this->getCategoryAttributes($_category);
        $product = Mage::registry('product');
        $attributtes = array();
        if (!is_array($attributes)) return array();
        $_helper = Mage::helper('ulula_mercadolibre/attribute');
        foreach ($attributes as $attribute) {
            if ($_helper->allowVariations($attribute) || $_helper->isHidden($attribute) ||  $_helper->isVariationAttribute($attribute)) continue;
            $code = $_helper->getAttributeCode($attribute);
            if ( $_helper->attributeExists($code)) {
                $mageAttributes = $this->getMageAttribute($code);
                $attributtes[] = array(
                    'code'  => $code, 
                    'label' => $mageAttributes->getStoreLabel(), 
                    'value' => $product->getData($code),
                    'input' => $mageAttributes->getFrontendInput(),
                    'options' =>  $mageAttributes->getSource()->getAllOptions(false)
                );
            }
        }

        return $attributtes;
    }

    public function getCategoryAttributes($_category)
    {
        return json_decode($_category->getCategoryAttributes(), true);
    }

    public function getCategoryVariants($_category)
    {
        $attributes = $this->getCategoryAttributes($_category);
        $_helper = Mage::helper('ulula_mercadolibre/attribute');
        $categoryAttr = array();
        $product = Mage::registry('product');
        if (!is_array($attributes)) {
            return $categoryAttr;
        }

        foreach ($attributes as $attribute) {
            if ($_helper->isHidden($attribute)) continue;
            $code = $_helper->getAttributeCode($attribute);
            $isRequired = $_helper->isRequired($attribute);
            if ( ($_helper->allowVariations($attribute) || $_helper->isVariationAttribute($attribute)) && $_helper->attributeExists($code) ) {
                $attributteData = array('code'=>$code, 'label', 'input');
                $attributteData = $this->getAttributeData($product, $attributteData, $isRequired);
                $categoryAttr[] = $attributteData;
            }
        }

        return $categoryAttr;
    }

    protected function getMageAttribute($attrCode)
    {
        $attributeId = Mage::getResourceModel('eav/entity_attribute')
            ->getIdByCode('catalog_product', $attrCode);
        return Mage::getModel('catalog/resource_eav_attribute')->load($attributeId);
    }

    /**
     * Creates html input for variant [text, select] 
     * @param  Mage_Catalog_Model_Product $product
     * @param array $attributteData
     * @param bool $isRequired
     * @return array
     */
    protected function getAttributeData($product, $attributteData,  $isRequired)
    {
        if (!Mage::helper('ulula_mercadolibre/attribute')
            ->attributeExists($attributteData['code'])) {
            return '';
        }

        $attribute = $this->getMageAttribute($attributteData['code']);
        $attributteData['input'] = $this->_getInputHtml( 
            $attribute,
            $product,
            $isRequired
        );
        $attributteData['label'] = $attribute->getStoreLabel();
        $attributteData['label'] .= ($isRequired)? '<span class="required">*</span>':'';
        return $attributteData;
    }

    protected function _getInputHtml($attribute, $product, $isRequired)
    {
        $html = '';
        $code = $attribute->getAttributeCode();
        $required = ($isRequired)? 'required-entry':'';
        switch ($attribute->getFrontendInput()) {
            case 'select':
             $selectedValue = $product->getAttributeText($code);
                $selectedValue = $product->getAttributeText($code);
                $html = '<select id="'.$code.'" name="product['.$code.
                    ']" class="select '.$required.'">';
                $attributeOptions = $attribute->getSource()->getAllOptions();
                foreach ($attributeOptions as $option) {
                       $selected = ($option['label'] == $selectedValue)? 'selected="selected"':'';
                       $html .= '<option '.$selected.' value="'.$option["value"].'">'.$option["label"].'</option>';
                }

                $html.'</select>';
                break;
            
            default:
                $html = '<input type="text" id="'.$code.'" name="product['.$code.
                    ']" class="input-text '.$required.'" value="'.$product->getData($code).'"/>';
                break;
        }
        
        return $html;
    }

    public function getPublishCombo()
    {
        $product = Mage::registry('product');
        $selectedValue = $product->getMeliPublish();
        $html = '<select id="meli_publish" name="product[meli_publish]" class="select">';
        $attributeOptions = array(
            array('value'=>0, 'label'=>$this->__('No')),
            array('value'=>1, 'label'=>$this->__('Yes'))
            );
        foreach ($attributeOptions as $option) {
               $selected = ($option['value'] == $selectedValue)? 'selected="selected"':'';
               $html .= '<option '.$selected.' value="'.$option["value"].'">'.$option["label"].'</option>';
        }

        return $html.'</select>';
    }
}