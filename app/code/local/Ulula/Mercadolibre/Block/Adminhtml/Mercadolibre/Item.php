<?php
/**
 * 
 * @category    Ulula
 * @package     Ulula_Mercadolibre
 * @copyright   Copyright (c) 2017 Ulula IT (http://ulula.net)
 * @author    	Gaston De Marsico <gdemarsico@ulula.net>
 */
 
class Ulula_Mercadolibre_Block_Adminhtml_Mercadolibre_Item extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_blockGroup = 'ulula_mercadolibre';
        $this->_controller = 'adminhtml_mercadolibre_item';
        $this->_headerText = Mage::helper('ulula_mercadolibre')->__('Mercadolibre - Publications');
        parent::__construct();
        $this->_removeButton('add');
    }
}