<?php
/**
 * 
 * @category    Ulula
 * @package     Ulula_Mercadolibre
 * @copyright   Copyright (c) 2017 Ulula IT (http://ulula.net)
 * @author    	Gaston De Marsico <gdemarsico@ulula.net>
 */
 
class Ulula_Mercadolibre_Block_Adminhtml_Mercadolibre_Item_Grid_Renderer_Sync extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
    	$value =  $row->getData($this->getColumn()->getIndex());
       	return ($value == 0)?Mage::helper('ulula_mercadolibre')->__('Disabled'):
       		Mage::helper('ulula_mercadolibre')->__('Enabled');
    }

}