<?php
/**
 * 
 * @category    Ulula
 * @package     Ulula_Mercadolibre
 * @copyright   Copyright (c) 2017 Ulula IT (http://ulula.net)
 * @author        Gaston De Marsico <gdemarsico@ulula.net>
 */
 
class Ulula_Mercadolibre_Block_Adminhtml_Mercadolibre_Order_Grid_Renderer_Incremental extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    /**
     * Renders grid column
     *
     * @param   Varien_Object $row
     * @return  string
     */
    public function render(Varien_Object $row)
    {
        $dataArr = $this->getColumn()->getIndex();
        $id = $row->getData('id');
        $orderId = $row->getData($dataArr);
        $record = Mage::getModel('ulula_mercadolibre/order')->load($id);
        if ($orderId) {
            $incrementId = Mage::getModel('sales/order')->load($orderId)->getIncrementId();
            $url = Mage::helper('adminhtml')
               ->getUrl('adminhtml/sales_order/view/order_id/', array('order_id'=>$orderId));
            return '<a href="'.$url.'" target="_blank">'.$incrementId.'</a>';
        } elseif ($record->getMeliOrderData()) {
            $url = $this->getRetriveUrl($record->getMeliOrderId());
            return '<a href="'.$url.'">'.$this->__('Retrieve order ').$record->getMeliOrderId().'</a>';;
        }

        return $this->__('Waiting data from order');
    }

    protected function getRetriveUrl($orderId)
    {
        $params = array('order_id' => $orderId );
        return Mage::helper('adminhtml')
               ->getUrl('adminhtml/mercadolibre_orders/retrive', $params);
    }
}