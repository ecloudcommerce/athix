<?php
/**
 * 
 * @category    Ulula
 * @package     Ulula_Mercadolibre
 * @copyright   Copyright (c) 2017 Ulula IT (http://ulula.net)
 * @author        Gaston De Marsico <gdemarsico@ulula.net>
 */

class Ulula_Mercadolibre_Helper_Price extends Ulula_Mercadolibre_Helper_Data
{

    protected $_table = 'ulula_meli_price';
    protected $_tableItems = 'ulula_mercadolibre_item';
    protected $_tableIdx = 'catalog_product_index_price';

    public function truncateMeliPrice()
    {
        $sql = 'TRUNCATE TABLE '.$this->_table;
        $resource = Mage::getSingleton('core/resource');
        $writeConnection = $resource->getConnection('core_write');
        $writeConnection->raw_query($sql);
    }

    public function getPrices()
    {
        if (!$websiteIds = $this->getEnabledWebsiteIds()) {
            return false;
        }

        $sql = "SELECT b.sku, a.price, a.final_price, a.group_price from ".$this->_tableIdx." as a
		RIGHT JOIN ".$this->_table." AS b ON a.entity_id=b.pid
		RIGHT JOIN ".$this->_tableItems." AS c ON b.sku=c.sku
		 WHERE a.website_id IN (".implode(',', $websiteIds).") 
		 AND a.customer_group_id = (SELECT customer_group_id 
		 FROM customer_group WHERE customer_group_code = 'MeLi Group Price');" ;
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $prices = $readConnection->fetchAll($sql);
        if (is_array($prices) && !empty($prices)) {
            $this->truncateMeliPrice();
            return $prices;
        }

        return false;
    }

    public function getPriceJson()
    {
        $prices = $this->getPrices();
        $pricesList = array();
        $bag = 0;
        $counter = 0;
        if ($prices === false) {
            return $prices;
        }

        foreach ($prices as $price) {
            
            if (($bag+1) * 50 <= $counter++) {
                $bag++;
            }
            $pricesList[$bag][] = $price;
        }

        return $pricesList;
    }

    public function getEnabledWebsiteIds()
    {
        $websiteIds = false;
        $priceConf = 'mercadolibre/synchronization/price_enabled';
        foreach (Mage::app()->getWebsites() as $website) {
            if ($website->getConfig($priceConf)) {
                $websiteIds[] = $website->getId();
            }
        }

        return $websiteIds;
    }
}