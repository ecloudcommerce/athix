<?php
/**
 * 
 * @category    Ulula
 * @package     Ulula_Mercadolibre
 * @copyright   Copyright (c) 2017 Ulula IT (http://ulula.net)
 * @author      Gaston De Marsico <gdemarsico@ulula.net>
 */

class Ulula_Mercadolibre_Helper_Product extends Ulula_Mercadolibre_Helper_Data
{

    protected $_additionalImages = array();
    protected $_helperAttr;

    public function __construct()
    {
        $this->_helperAttr = Mage::helper('ulula_mercadolibre/attribute');
    }

    public function getTitle($product){
        if($product->getMeliTitle()){
            return $product->getMeliTitle();
        }
        return $product->getName();
    }

    public function getDescription($product){
        if($product->getMeliDescription()){
            return $product->getMeliDescription();
        }
        $text = "\r\n";
        $attributes = $this->getAdditionalData($product);
        foreach ($attributes as $attribute) {
            $text .= $attribute['label'].': '.$attribute['value']."\r\n";
        }
        return $product->getDescription().$text;
    }

    public function getCondition($product){
        if($product->getAttributeText('meli_condition')){
            return $product->getAttributeText('meli_condition');
        }
        return 'new';
    }

    public function getShippingMode($product){
        if($product->getAttributeText('meli_ship_mode')){
            return $product->getAttributeText('meli_ship_mode');
        }
        return 'me2';
    }

    public function getLocalPickUp($product){
        return $product->getAttributeText('meli_local_pick_up');
    }

    public function getAddtionalImages($product){
        $aditionalImages = $product->getMeliAdditionalImages();
        return ($aditionalImages && strlen($aditionalImages)) ? $aditionalImages : false;
    }

    public function getAddtionalImagesArray($product){
        $aditionalImages = $this->getAddtionalImages($product);
        return ($aditionalImages == false)? array():explode(',', trim($aditionalImages));
    }

    protected function getAdditionalData($product, $excludeAttr = array())
    {
        $data = array();
        $attributes = $product->getAttributes();
        foreach ($attributes as $attribute) {
            if ($attribute->getIsVisibleOnFront() && !in_array($attribute->getAttributeCode(), $excludeAttr)) {
                $value = $attribute->getFrontend()->getValue($product);

                if (!$product->hasData($attribute->getAttributeCode())) {
                    continue;
                } elseif ((string)$value == '') {
                    $value = Mage::helper('catalog')->__('No');
                } elseif ($attribute->getFrontendInput() == 'price' && is_string($value)) {
                    $value = Mage::app()->getStore()->convertPrice($value, true);
                }

                if (is_string($value) && strlen($value)) {
                    $data[$attribute->getAttributeCode()] = array(
                        'label' => $attribute->getStoreLabel(),
                        'value' => $value,
                        'code'  => $attribute->getAttributeCode()
                    );
                }
            }
        }
        return $data;
    }

    public function getProductJson($product){
        $attributes = $this->getProductAttributes($product);
        $data = $this->setProductData($product, $attributes);
        return json_encode($data);
    }

    public function getProductSyncJson($product){
        $data = array();
        $data['title'] = $this->getTitle($product);
        $data['description'] = $this->getDescription($product);
        if($meliStoreId = $this->getMeliStoreId($this->getProductStoreId($product))){
            $data['official_store_id'] = $meliStoreId;
        }
        $data['shipping'] = $this->getShippingData($product);
        $attributes = $this->getMeliAttributes($product);
        if(!empty($attributes))
            $data['attributes'] = $attributes;
        return json_encode($data);
    }

    protected function setProductData($product, $attributes){
        $data = array();
        $helper = Mage::helper('ulula_mercadolibre');
        foreach ($attributes as $attributeCode) {
            $value = $this->getAttributeValue($product, $attributeCode);
            if($value)
                $data[$attributeCode] = $value;
        }
        $this->_additionalImages = $this->getAddtionalImagesArray($product);
        $data['meli_title'] = $this->getTitle($product);
        $data['meli_description'] = $this->getDescription($product);
        $data['meli_category_id'] = $helper->getMeliCategory($product)->getMeliCategoryId();
        $data['qty'] = $this->getProductQty($product);
        $data['price'] = $this->getProductPrice($product);
        $data['currency'] = Mage::app()->getStore()->getCurrentCurrencyCode();
        $data['images'] = $this->getProductImages($product);
        $data['listing_type'] = 'gold_special';
        $data['shipping'] = $this->getShippingData($product);
        if($meliStoreId = $helper->getMeliStoreId($this->getProductStoreId($product))){
            $data['official_store_id'] = $meliStoreId;
        }

        $variants = $this->getVariants($product);
        if(!empty($variants))
            $data['variants'] = $variants;

        $attributes = $this->getMeliAttributes($product, $variants);
        if(!empty($attributes))
            $data['attributes'] = $attributes;
        
        return $data;
    }

    public function getShippingData($product){
        $mode = $this->getShippingMode($product);
        $shipping = [
            'mode' => $mode,
            'local_pick_up' => (bool)$product->getMeliLocalPickUp()
        ];
        if($mode == 'custom'){
            $costs = $this->getCustomShipping($product);
            if(empty($costs)){
                $shipping['mode'] = 'not_specified';
            }
            else{
                $shipping['costs'] = $this->getCustomShipping($product);
            }
        }
        return $shipping;
    }

     public function getCustomShipping($product){
        $customShipping = array();
        for ($i=1; $i <=10; $i++) { 
            if($product->getData('meli_shipping_custom_desc'.$i) && $product->getData('meli_shipping_custom_cost'.$i)){
                $customShipping[] = array('description'=>$product->getData('meli_shipping_custom_desc'.$i),'cost'=>$product->getData('meli_shipping_custom_cost'.$i));
            }
        }
        return $customShipping;
     }

    public function getMeliAttributes($product, $variants=false){
        $meliAttributes = array();
        $categoryId = $this->getMeliCategory($product)->getMeliCategoryId();
        $attributes = $this->_helperAttr->getCategoriesAttrFromMeli($categoryId);
        foreach ($attributes as $attribute) {
            $code = $this->_helperAttr->getAttributeCode($attribute);
            $value = $product->getData($code);
            if (!$value) continue;

            if ($this->_helperAttr->isVariationAttribute($attribute) && $variants) continue;

            if(!$this->_helperAttr->isVariation($attribute)){
                $valueLabel = ($attribute['value_type'] == 'boolean' || $attribute['value_type'] == 'list' ) ? 'value_id' : 'value_name';
                $meliAttributes[] = array('id'=>$attribute['id'], $valueLabel=>$this->getValueFromMeliAttr($attribute, $code, $product));
            }
        }
        return $meliAttributes;
    }

    public function getValueFromMeliAttr($attribute, $code, $product)
    {
        switch ($attribute['value_type']) {
            case 'boolean':
                $val = $this->getAttributeValue($product, $code);
                $value = $this->getValueId($val, $attribute);
                break;
            case 'list':
                $val = $this->getAttributeValue($product, $code);
                $value = $this->getValueId($val, $attribute);
                break;
            default:
                $value = $product->getData($code);
                break;
        }
        return $value;
    }

    public function getValueId($value, $attribute)
    {
        foreach ($attribute['values'] as $attrValue) {
            if($attrValue['name'] == $value) return $attrValue['id'];
        }
        return '';
    }

    protected function getAttributeValue($product, $attributeCode){
        $attr = $product->getResource()->getAttribute($attributeCode);
        if(!$attr) return null;
        return ($attr->usesSource()) ?
                $attr->getSource()->getOptionText($product->getData($attributeCode)):
                $product->getData($attributeCode);
    }

    public function getAttributeValueFromLabel($attributeCode, $label){
        $model = Mage::getModel('catalog/product')->getResource()
            ->getAttribute($attributeCode);
        if ($model){
            $sourceModel = $model->getSource();
            return $sourceModel->getOptionId($label);
        }
        return '';
    }


    /*
    * Returns product price for Mercadolibre
    * @param Mage_Catalog_Model_Product 
    * @return float
    */
    protected function getProductPrice($product){
        $customer_group_code = Mage::helper('ulula_mercadolibre')->getGroupPriceName();
        $group = Mage::getModel('customer/group')->load($customer_group_code, 'customer_group_code');
        $product->setCustomerGroupId($group->getId());
        return $product->getFinalPrice();
    }

    protected function getProductQty($product){
        $stock = Mage::getModel('cataloginventory/stock_item')->loadByProduct($product);
        return $stock->getQty();
    }

    protected function getProductImages($product){
        $imageHelper = Mage::helper('ulula_mercadolibre/image');
        return $imageHelper->getMediaGalleryImages($product, $this->_additionalImages);
    }

    protected function getVariants($product){
        $variants = array();

        if($product->getTypeId() == 'configurable'){
            $conf = Mage::getModel('catalog/product_type_configurable')->setProduct($product);
            $childrenCollection = $conf->getUsedProductCollection()->addAttributeToSelect('*');
            foreach ($childrenCollection as $simpleProduct) {
                $variantProduct = $this->getProductVariants($simpleProduct,$variants, $product);
                $variants = $variants + $variantProduct;
                
            }
        }
        else{
            $variants = $this->getProductVariants($product);
        }
        return $variants;
    }

    /**
     * Returns product variants for meli
     * @param  Mage_Catalog_Model_Product $product
     * @param  array  $variants
     * @param  Mage_Catalog_Model_Product $parent
     * @return array
     */
    protected function getProductVariants($product, $variants = array(), $parent=null){
        $_helperAttr = Mage::helper('ulula_mercadolibre/attribute');
        $meliCategory = $this->getMeliCategory($product);
        if(!$meliCategory->getMeliCategoryId())
            return array();
        $attributes = $_helperAttr->getCategoriesAttrFromMeli($meliCategory->getMeliCategoryId());
        
        $variants[$product->getSku()]['price'] = $this->getProductPrice($product);
        $variants[$product->getSku()]['qty'] = $this->getProductQty($product);
        if($parent==null){
            $variants[$product->getSku()]['image'] = $this->getProductImages($product);
        }
        else{
            if(count($this->getProductImages($product)) > 0){
                $variants[$product->getSku()]['image'] = $this->getProductImages($product);
            }
            else{
                $variants[$product->getSku()]['image'] = $this->getProductImages($parent);
            }
        }
        $hasVariant = false;
        foreach ($attributes as $attribute) {
            $attributeCode = $_helperAttr->getAttributeCode($attribute);
            $attrValue = $this->getAttributeValue($product, $attributeCode);
            if ($attrValue == null) continue;

            $meliValue = $this->getMeliValue($attribute, $attrValue);
            if ($meliValue === false) continue;

            if ($_helperAttr->isVariation($attribute)) {
                $variants[$product->getSku()]['variant'][] = $meliValue;
                    $hasVariant = true;
                    continue;
                if ($_helperAttr->isRequired($attribute) && !$hasVariant) {
                        return array();
                }
            }

            if ($_helperAttr->isVariationAttribute($attribute)) {
                $variants[$product->getSku()]['attributes'][] = $meliValue;
            }
        }
        if($hasVariant){
            return $variants;
        }
        else{
            return array();
        }
    }

    protected function getMeliValue($attribute, $attrValue)
    {
        if ($attribute['value_type'] == 'string') {
            return array('name'=>$attribute['id'], 'value_name'=>$attrValue);
        }
        foreach ($attribute['values'] as $value) {
            if ($attrValue == $value['name']) {
                return array('name'=> $attribute['id'], 'value_id'=> $value['id']);
            }
        }
        return false;
    }

    protected function getProductAttributes($product){
        $attributes = array(
            'sku', 'meli_condition'
        );

        return $attributes;
    }

    protected function getMeliAttributesByCategoryPk($categoryId){
        $_helperAttr = Mage::helper('ulula_mercadolibre/attribute');
        $attributes = $_helperAttr->getCategoriesAttrFromMeli($categoryId);
        $categoryAttributes = array();
        foreach ($attributes as $attribute) {
            if($_helperAttr->isRequired($attribute) || $_helperAttr->hasNoTags($attribute)){
                $code = $_helperAttr->getAttributeCode($attribute);
                $categoryAttributes[] = $code;
            }
        }
        return $categoryAttributes;
    }

    protected function getMeliAttributesByCategory($categoryId){
        $_helperAttr = Mage::helper('ulula_mercadolibre/attribute');
        $attributes = $_helperAttr->getCategoriesAttrFromMeli($categoryId);
        $categoryAttributes = array();
        foreach ($attributes as $attribute) {
            if($_helperAttr->mustCreateAttribute($attribute)){
                $code = $_helperAttr->getAttributeCode($attribute);
                $categoryAttributes[] = $code;
            }
        }
        return $categoryAttributes;
    }

    /*
    * Checks if a magento product can be published in Mercadolibre
    * @return bool 
    */
    public function canPublish($product){
        return (bool)(
            $product->getMeliPublish() 
            && Mage::helper('ulula_mercadolibre')->getMeliCategory($product)->getMeliCategoryId()
            && $this->getTitle($product) 
            && $this->getDescription($product) 
            && (!$this->getMeliPubId($product))
        );
    }

    public function canSync($product){
        return (bool)(
            $product->getMeliPublish()
            && Mage::helper('ulula_mercadolibre')->getMeliCategory($product)->getMeliCategoryId()
            && $this->getTitle($product) 
            && $this->getDescription($product)
        );
    }

    public function getMeliPubId($product){
        $item = Mage::getModel('ulula_mercadolibre/item')->load($product->getSku(), 'sku');
        if($item && $item->getMeliId()){
            return $item->getMeliId();
        }
        return false;
    }

    public function unsetPublish($sku){
        $product = Mage::getModel('catalog/product')->loadByAttribute('sku', $data['seller_custom_field']);
        if(!$product)
            return;
        $product->setData('meli_publish', 0);
        $product->save();
    }

    public function updateProduct($data){
        $product = Mage::getModel('catalog/product')->loadByAttribute('sku', $data['seller_custom_field']);
        if(!$product)
            return;
        Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
        $pid = $product->getId();
        $this->updateAttributes($pid, 'meli_title', $data['title']);
        $this->updateAttributes($pid, 'meli_description', $data['description']);
        $this->updateAttributes($pid, 'meli_publish', 1);
        $this->setMeliCategory($data['category_id']);
        $this->updateAttributes($pid, 'meli_category_id', trim($data['category_id']));
        $condition = (int)$this->getAttributeValueFromLabel('meli_condition', $data['condition']);
        $this->updateAttributes($pid, 'meli_condition', $condition);
        $_helperAttr = Mage::helper('ulula_mercadolibre/attribute');
        if(isset($data['attributes'])){
            foreach ($data['attributes'] as $attribute) {
                $attributeCode = $_helperAttr->getAttributeCode($attribute);
                $this->updateAttributes($pid, $attributeCode, $attribute['value_name']);
            }
        }
        if(isset($data['variations'])){
            foreach ($data['variations'] as $variation) {
                if($variation['seller_custom_field'] != $product->getSku()){
                    $variantProduct = Mage::getModel('catalog/product')->loadByAttribute('sku', $variation['seller_custom_field']);
                    if($variantProduct){
                       $this->updateAttributes($variantProduct->getId(), 'meli_category_id', $data['category_id']);
                    }
                }
                else{
                    $variantProduct = $product;
                }
                foreach ($variation['attribute_combinations'] as $attr) {
                    $attributeCode = $_helperAttr->getAttributeCode($attr, true);
                    if($attributeCode)
                       $optionId = $this->getAttributeValueFromLabel($attributeCode, $attr['value_name']);
                    if($variantProduct && $optionId != ''){
                       $this->updateAttributes($variantProduct->getId(), $attributeCode, $optionId);
                    }
                }
            }
        }
    }

    protected function setMeliCategory($meliCategoryId){
        $categoryModel = Mage::getModel('ulula_mercadolibre/category')
            ->load($meliCategoryId, 'meli_category_id');
        if(!$categoryModel->getId()){
            $result = Mage::helper('ulula_mercadolibre/api')->getCategory($meliCategoryId);
            $categoryData = $result['body'];
            $categoryModel->setMeliCategoryId($meliCategoryId);
            $categoryModel->setCategoryName($this->getCategoryName($categoryData));
            $categoryModel->setCategoryData(json_encode($categoryData));
            $attributes = Mage::helper('ulula_mercadolibre/attribute')->setAttributes($meliCategoryId);
            $categoryModel->setCategoryAttributes(json_encode($attributes));
            $categoryModel->save();
        }
    }

    protected function getCategoryName($categoryData){
        $separator = ' -> ';
        $categoryName = '';
        foreach ($categoryData['path_from_root'] as $path) {
            $categoryName .= $path['name'].$separator;
        }
        return substr($categoryName, 0, (strlen($categoryName) - 4) );
    }

    public function updateAttributes($pid, $attributeCode, $value){
        $attribute =  $this->getAttribute($attributeCode);
        if(!isset($attribute[0])){
            return;
        }
        $attrId = $attribute[0]['attribute_id'];
        $type =  $attribute[0]['backend_type'];
        $table = $this->getAttributeTable($type);
        $sql = "UPDATE ".$table." SET value = :value WHERE entity_id = ".$pid." AND attribute_id=".$attrId." AND entity_type_id = 4";
        $binds = array('value' => $value);
        $this->getWriteConnection()->query($sql, $binds);
    }

    protected function getAttribute($attributeCode){
        $sql = "SELECT * FROM eav_attribute WHERE attribute_code ='".$attributeCode."'";
        return $this->getReadConnection()->fetchAll($sql);
    }

    protected function getReadConnection(){
        $resource = Mage::getSingleton('core/resource');
        return $resource->getConnection('core_read');
    }

    protected function getWriteConnection(){
        $resource = Mage::getSingleton('core/resource');
        return $resource->getConnection('core_write');
    }

    public function getAttributeTable($type){
        switch ($type) {
            case 'int':
                return 'catalog_product_entity_int';
                break;
            case 'varchar':
                return 'catalog_product_entity_varchar';
                break;
            default:
                return 'catalog_product_entity_text';
                break;
        }
    }

    /**
     * Return request to send
     * @param  Mage_Catalog_Model_Product $product Magento product
     * @param  null | int $meliId  meli list id
     * @return array request
     */
    public function getRequest($product, $meliId = null)
    {
        $request = array();
        $request['key'] = $this->getAll2MeliKey($this->getProductStoreId($product));
        $request['sku'] = $product->getSku();
        $request['meli_pub_id'] = $meliId;
        if ($meliId) {
            $request['in_data'] = $this->getProductSyncJson($product);
            
        } else {
            $request['in_data'] = $this->getProductJson($product);
        }
        return $request;
    }

    public function getProductStoreId($product)
    {
        $storeIds = $product->getStoreIds();
        return (isset($storeIds[0])) ? $storeIds[0] : null;
    }
}