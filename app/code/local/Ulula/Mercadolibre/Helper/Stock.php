<?php
/**
 * 
 * @category    Ulula
 * @package     Ulula_Mercadolibre
 * @copyright   Copyright (c) 2017 Ulula IT (http://ulula.net)
 * @author    	Gaston De Marsico <gdemarsico@ulula.net>
 */

class Ulula_Mercadolibre_Helper_Stock extends Ulula_Mercadolibre_Helper_Data
{

	protected $_stockTable = 'ulula_meli_stock';

	public function truncateMeliStock(){
		$sql = 'TRUNCATE TABLE '.$this->_stockTable;
		$resource = Mage::getSingleton('core/resource');
		$writeConnection = $resource->getConnection('core_write');
		$writeConnection->raw_query($sql);
	}

	public function getStock(){
		$sql = 'SELECT * from '.$this->_stockTable;
		$resource = Mage::getSingleton('core/resource');
		$readConnection = $resource->getConnection('core_read');
		$stock = $readConnection->fetchAll($sql);
		if(is_array($stock) && !empty($stock)){
			$this->truncateMeliStock();
			return $stock;
		}
		return false;
	}

	public function getStockJson(){
		$stocks = $this->getStock();
		$stockList = array();
        $bag = 0;
        $counter = 0;
        if ($stocks === false) {
            return $stocks;
        }

        foreach ($stocks as $stock) {
            
            if (($bag+1)* 50 <= $counter++) {
                $bag++;
            }

            $stockList[$bag][] = $stock;
        }

        return $stockList;
	}
}