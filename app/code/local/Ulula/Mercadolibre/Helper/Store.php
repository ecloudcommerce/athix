<?php
/**
 * 
 * @category    Ulula
 * @package     Ulula_Mercadolibre
 * @copyright   Copyright (c) 2017 Ulula IT (http://ulula.net)
 * @author    	Gaston De Marsico <gdemarsico@ulula.net>
 */

class Ulula_Mercadolibre_Helper_Store extends Ulula_Mercadolibre_Helper_Data
{
	public function checkStores(){
		$_helper = Mage::helper('ulula_mercadolibre/attribute');
		$attrCode = 'meli_stores';
		$key = Mage::helper('ulula_mercadolibre')->getAll2MeliKey();
		$stores = Mage::helper('ulula_mercadolibre/api')->getStores($key);
		$storeIds = $this->getStoresId($stores);
		$options = Mage::helper('ulula_mercadolibre/attribute')->getAttrOptions($attrCode);
		unset($options[0]);
		foreach ($options as $option) {
			$storeId =  $this->getStoreId($option['label']);
			if(array_key_exists($storeId, $storeIds)){
				$sql = "UPDATE eav_attribute_option_value SET value =  '".$storeIds[$storeId]."' WHERE option_id =".$option['value'];
				$resource = Mage::getSingleton('core/resource');
				$writeConnection = $resource->getConnection('core_write');
				$writeConnection->raw_query($sql);
				unset($storeIds[$storeId]);
			}
			else{
				$_helper->deleteOption($attrCode, $option['label']);
			}
		}
		$attributeId = $_helper->getAttributeIdByCode($attrCode);
		foreach ($storeIds as $meliStore) {
			$optionNew['attribute_id'] = $attributeId;
    		$optionNew['value'][$meliStore] =  array($meliStore);
    		$installer = new Mage_Eav_Model_Entity_Setup('core_setup');
    		$installer->addAttributeOption($optionNew);
		}
	}

	protected function getStoresId($labels){
    	$ids = array();
    	foreach ($labels as $label) {
		    $labelId = $this->getStoreId($label);
		    $ids[$labelId] = $label;
		}
		return $ids;
    }

    protected function getStoreId($label){
    	$labelArray = explode('-', $label);
		return trim(str_replace(array('[',']'), '', $labelArray[0]));
    }

    public function getMeliStoreIds($optionIds=array()){
    	$ids = array();
    	$attrCode = 'meli_stores';
    	$options = Mage::helper('ulula_mercadolibre/attribute')->getAttrOptions($attrCode);
    	foreach ($options as $option) {
    		if(in_array($option['value'], $optionIds)){
    			$ids[] = $this->getStoreId($option['label']);
    		}
    	}
    	return $ids;
    }
}