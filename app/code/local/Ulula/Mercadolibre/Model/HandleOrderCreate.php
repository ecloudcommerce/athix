<?php
/**
 * 
 * @category    Ulula
 * @package     Ulula_Mercadolibre
 * @copyright   Copyright (c) 2017 Ulula IT (http://ulula.net)
 * @author      Gaston De Marsico <gdemarsico@ulula.net>
 */

class Ulula_Mercadolibre_Model_HandleOrderCreate extends Mage_Core_Model_Abstract
{
    const PAYMENT_METHOD = 'mercadolibre';
    const SHIPPING_METHOD = 'mercadolibre_mercadoenvios';
    const DEFAULT_ORDER_COMMENT = 'This order has been created by MERCADOLIBRE.';
    const CUSTOMER_GROUP = 'MeLi Group Price';

    private $_sendConfirmation = '0';
     
    protected $_orderData = array();
    private $_product;
     
    private $_sourceCustomer;
    private $_sourceOrder;
 
    /**
    * Retrieve order create model
    *
    * @return Mage_Adminhtml_Model_Sales_Order_Create
    */
    protected function _getOrderCreateModel()
    {
        return Mage::getSingleton('adminhtml/sales_order_create');
    }
 
    /**
    * Retrieve session object
    *
    * @return Mage_Adminhtml_Model_Session_Quote
    */
    protected function _getSession()
    {
        return Mage::getSingleton('adminhtml/session_quote');
    }
 
    /**
    * Initialize order creation session data
    *
    * @param  array $data
    * @return Mage_Adminhtml_Sales_Order_CreateController
    */
    protected function _initSession($data)
    {
        if ($data['customer_id']) {
              $this->_getSession()->setCustomerId(0);
        } else {
            $this->_getSession()->setCustomerId($data['customer_id']);
        }
 
        /* Get/identify store */
        if (!empty($data['store_id'])) {
            $this->_getSession()->setStoreId((int) $data['store_id']);
        }
     
        return $this;
    }
 
    /**
    * Creates order
    */
    public function create()
    {
        $orderData = $this->_orderData;
        
        if (!empty($orderData)) {
            $this->_initSession($orderData['session']);

            try {
                $_order = $this->_getOrderCreateModel();
                Mage::register('shipping-data', $orderData['shipping_data']);
                $_order->getQuote()->getShippingAddress()->setShippingAmount($orderData['shipping_data']['cost']);
                $this->_processQuote($orderData);

                if (!empty($orderData['payment'])) {
                    $_order->setPaymentData($orderData['payment']);
                    $_order->getQuote()->getPayment()->addData($orderData['payment']);
                }
             
                Mage::app()->getStore()->setConfig(Mage_Sales_Model_Order::XML_PATH_EMAIL_ENABLED, "0");
            
                $_order->importPostData($orderData['order']);
                $order = $_order->createOrder();
                $this->_getSession()->clear();
                Mage::unregister('rule_data');
             
                return $order;
            }
            catch (Exception $e){
                Mage::log("Order save error...");
                Mage::log($e);
            }
        }

        return null;
    }
 
    protected function _processQuote($data = array())
    {
        /* Saving order data */
        if (!empty($data['order'])) {
            $this->_getOrderCreateModel()->importPostData($data['order']);
        }
         
        $this->_getOrderCreateModel()->getBillingAddress();
        $this->_getOrderCreateModel()->setShippingAsBilling(true);
         
        /* Just like adding products from Magento admin grid */
        if (!empty($data['add_products'])) {
            foreach ($data['add_products'] as $key => $value) {
                $this->_getOrderCreateModel()->addProduct($key, $value);
            }

            foreach ($this->_getOrderCreateModel()->getQuote()->getItemsCollection() as $item) {
                if (isset($data['add_products'][$item->getProductId()])) {
                    if ($itemPrice = $data['add_products'][$item->getProductId()]['custom_price']) {
                        $item->setCustomPrice($itemPrice);
                        $item->setOriginalCustomPrice($itemPrice);
                        $item->getProduct()->setIsSuperMode(true);
                        $item->getProduct()->unsSkipCheckRequiredOption();
                        $item->checkData();
                        $item->save();
                    }
                }
            }
        }
         
        /* Collect shipping rates */
        $this->_getOrderCreateModel()->collectShippingRates();
         
        /* Add payment data */
        if (!empty($data['payment'])) {
            $this->_getOrderCreateModel()->getQuote()
                ->getPayment()->addData($data['payment']);
        }

        $this->_getOrderCreateModel()->getQuote()->setCustomerIsGuest(true);
        $this->_getOrderCreateModel()->getQuote()
            ->setCustomerFirstname($data['order']['account']['customer_firstname']);
        $this->_getOrderCreateModel()->getQuote()
            ->setCustomerLastname($data['order']['account']['customer_lastname']);
        $this->_getOrderCreateModel()->getQuote()->setCustomerSuffix('Mercadolibre');
        $this->_getOrderCreateModel()
            ->initRuleData()
            ->saveQuote();

        if (!empty($data['payment'])) {
            $this->_getOrderCreateModel()->getQuote()->getPayment()->addData($data['payment']);
        }
         
        return $this;
    }

    protected function _getCurrency($infoArray)
    {
        return isset($infoArray['currency_id']) ? 
            $infoArray['currency_id'] : 
            self::DEFAULT_CURRENCY;
    }

    public function _getProducts($infoArray)
    {
        $products = false;
        if (isset($infoArray['order_items'])) {
            foreach ($infoArray['order_items'] as $order_item) {
                if (isset($order_item['item'])) {
                    $item = $order_item['item'];
                    if (($_productid = $this->skuExist($item)) && ($_qty = $this->getQty($order_item))) {
                        $products[$_productid] = array(
                            'qty' => $_qty, 
                            'custom_price' => $this->getPrice($order_item)
                        );
                    }
                }
            }
        }

        return $products;
    }

    public function skuExist(array $item)
    {
        if (isset($item['seller_custom_field']) && $item['seller_custom_field']) {
            return Mage::getModel('catalog/product')->getIdBySku($item['seller_custom_field']);
        }

        return false;
    }

    public function getQty(array $item)
    {
        return (isset($item['quantity']))?$item['quantity']:0;
    }

    /**
     * return meli item price
     * 
     * @param array $item Meli item
     * 
     * @return float|false price of item
     */
    public function getPrice(array $item)
    {
        return (isset($item['unit_price']) && $item['unit_price'] > 0) ? (float)$item['unit_price']:false;
    }

    protected function _getAddress(array $infoArray)
    {
        $regionData = $this->getRegionData($infoArray);
        return array(
         'customer_address_id' => '',
         'prefix' => '',
         'firstname' => $infoArray['first_name'],
         'middlename' => '',
         'lastname' => $infoArray['last_name'],
         'suffix' => '',
         'company' => ( isset($infoArray['company']) ) ? $infoArray['company'] : '',
         'street' => $infoArray['street'],
         'city' => $infoArray['city'],
         'country_id' => $infoArray['country_id'],
         'region' => $regionData['state_name'],
         'region_id' =>  $regionData['state_id'],
         'postcode' => $infoArray['zip_code'],
         'telephone' => $infoArray['phone'],
         'fax' => '',
         'vat_id' =>  $infoArray['vat_id']
        );            
    }

    public function getOrderData()
    {
        return $this->_orderData;
    }

    /**
     * Returns payment details
     * 
     * @param array $infoArray Array of order data
     * 
     * @return array  Array of payment data
     */
    public function getPaymentDetails(array $infoArray)
    {
        return array(
         'payment_id' => $infoArray['payment_id'],
         'issuer' => $infoArray['payment_method'],
         'status' => $infoArray['payment_status'],
        );
    }

    /**
     * Set order data
     * 
     * @param array $infoArray Array of order data
     * 
     * @return void
     */
    public function setOrderData(array $infoArray)
    {
        $this->_orderData = array(
            'shipping_data' => array(
                'name' => $infoArray['shipping_method'],
                'mode' => $infoArray['shipping_mode'],
                'cost' => $infoArray['shipping_cost']
            ),
            'add_products' => $this->_getProducts($infoArray),
            'order' => array(
                  'account' => array(
                    'group_id' => $this->getCustomerGroupId(),
                    'email' => $infoArray['email'],
                    'customer_firstname' => $infoArray['first_name'],
                    'customer_lastname' => $infoArray['last_name']
                ),
                'currency' => $infoArray['currency'],
                'billing_address' => $this->_getAddress($infoArray),
                'shipping_address' => $this->_getAddress($infoArray),
                'shipping_method' => self::SHIPPING_METHOD,
                'comment' =>  array(
                    'customer_note' => self::DEFAULT_ORDER_COMMENT
                ),
                'send_confirmation' => $this->_sendConfirmation
            ),
            'payment' => array(
                'method' => self::PAYMENT_METHOD,
                'details' => $this->getPaymentDetails($infoArray)
            ),
            'session' => array(
                'customer_id' => (int)$this->getCustomerByEmail($infoArray['email'])
                    ->getId(),
                'store_id' => Mage::app()->getStore()->getId(),
            ),      
        );
    }

    /**
     * Returns customer by email
     *
     * @param string $email 
     * 
     * @return Mage_Customer_Model_Customer $customer
     */
    protected function getCustomerByEmail($email)
    {
        $website = Mage::app()->getWebsite();
        return Mage::getModel('customer/customer')
         ->setWebsiteId($website->getId())
         ->loadByEmail($email);
    }

    /**
     * Returns customer group id
     *
     * @return int customer_group_id
     */
    public function getCustomerGroupId()
    {
        $group = Mage::getModel('customer/group');
        $group->load(self::CUSTOMER_GROUP, 'customer_group_code');
        return ($group->getId()) ? (int)$group->getId() : 0;
    }

    /**
     * Get Region name by iso2 code
     *
     * @param array $addresData
     *
     * @return  array
     */
    public function getRegionData($addresData)
    {
        $data = array('state_id' => 'N/A', 'state_name' => $addresData['state']);
        if (!isset($addresData['state_id'])) return $data;
        if (!isset($addresData['country_id'])) return $data;
        $regionModel = Mage::getModel('directory/region')
            ->loadByCode($addresData['state_id'], $addresData['country_id']);
        if ($regionModel->getId()) {
            $data['state_id'] = $regionModel->getId();
            $data['state_name'] = $regionModel->getDefaultName();
        }
        return $data;
    }

}
