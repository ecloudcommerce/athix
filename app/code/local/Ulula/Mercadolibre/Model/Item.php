<?php
/**
 * 
 * @category    Ulula
 * @package     Ulula_Mercadolibre
 * @copyright   Copyright (c) 2017 Ulula IT (http://ulula.net)
 * @author      Gaston De Marsico <gdemarsico@ulula.net>
 */

class Ulula_Mercadolibre_Model_Item extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {
        $this->_init('ulula_mercadolibre/item');
    }

    public function getDescription(){
        $info = json_decode($this->info,true);
        return (isset($info['description']))? $info['description'] : '';
    }

    public function getSkus(){
    	if($this->sku) {
            $skus[] = $this->sku;
        };
    	$info = json_decode($this->info,true);
        if(isset($info['variations'])){
        	foreach ($info['variations'] as $variation) {
        		if($variation['seller_custom_field']){
        			$skus[] = $variation['seller_custom_field'];
        		}
        	}
        }
    	return $skus;
    }

    public function getPrice($newPrice){
        $info = json_decode($this->getInfo(), true);
        $result['price'] = $info['price'];
        $info['price'] = $newPrice;
        if(isset($info['variations'])){
            foreach ($info['variations'] as $key => $variation) {
                $info['variations'][$key]['price'] = $newPrice;
                $result['variations'][] = $variation['id'];
            }
        }
        $this->setInfo(json_encode($info));
        $this->save();
        return $result;
    }

    public function getQty($sku, $newQty){
    	$info = json_decode($this->getInfo(), true);
    	$result = ['variation_id'=>null, 'qty'=>null, 'variations'=>null];
    	if(isset($info['variations'])){
    		foreach ($info['variations'] as $key => $variation) {
    			if($variation['seller_custom_field'] == $sku){
    				$result['variation_id'] = $variation['id'];
    				$result['qty'] = $variation['available_quantity'];
    				if($result['qty'] != $newQty){
    					$info['variations'][$key]['available_quantity'] = $newQty;
    					$this->setInfo(json_encode($info));
    					$this->save();
    				}
    			}
    			else{
    				$result['variations'][] = $variation['id'];
    			}
    		}
    		return $result;
    	}
 		$result['qty'] = $info['available_quantity'];
 		if($result['qty'] != $newQty){
 			$info['available_quantity'] = $newQty;
 			$this->setInfo(json_encode($info));
 			$this->save();
 		}
    	return $result;
    }

    public function updateSkuVariants($skuVariants){
        $info = json_decode($this->getInfo(), true);
        if(isset($info['variations'])){
            foreach ($info['variations'] as $key => $variation) {
                if(isset($skuVariants[$variation['id']])){
                    if($skuVariants[$variation['id']] != $variation['seller_custom_field']){
                        $info['variations'][$key]['seller_custom_field'] = $skuVariants[$variation['id']];
                        $this->setInfo(json_encode($info));
                    }
                }
            }
        }
        $this->setInfo(json_encode($info));
        $this->save();
    }

    /**
     * Returns product from item 
     * 
     * @return mixed Mage_Catalog_Model_Product | null
     */
    public function getProduct()
    {
        if ($productSku = $this->getSku())
            return Mage::getModel('catalog/product')->load($productSku, 'sku');
        return null;
    }
}