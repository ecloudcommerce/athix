<?php
/**
 * 
 * @category    Ulula
 * @package     Ulula_Mercadolibre
 * @copyright   Copyright (c) 2017 Ulula IT (http://ulula.net)
 * @author      Gaston De Marsico <gdemarsico@ulula.net>
 */


 class Ulula_Mercadolibre_Adminhtml_CategoriesController  extends Mage_Adminhtml_Controller_Action{

    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')
            ->isAllowed('mercadolibre/item');
    }
    
    public function indexAction()
    {
        $layout = Mage::getSingleton('core/layout');
        $pid = Mage::app()->getRequest()->getParam('pid');
        $html = $layout
            ->createBlock('ulula_mercadolibre/adminhtml_catalog_categories', 'melicategories', array('pid'=>$pid))
            ->setTemplate('ulula/mercadolibre/categories.phtml')
            ->toHtml();
        echo $html;
    }

    public function getAjaxCategoriesAction()
    {
    	$id = ($_GET['id'] == '#')?'':$_GET['id'];    	
    	$layout = Mage::getSingleton('core/layout');
    	$block = $layout->createBlock('ulula_mercadolibre/adminhtml_catalog_categories');
        echo $block->getCategoriesTree($id);        
    }

    public function saveCategoryAction()
    {
        $meli_category_id = $this->getRequest()->getParam('meli_category_id');
        $mage_category_id = $this->getRequest()->getParam('mage_category_id');
        $category_name = $this->getRequest()->getParam('category_name');
        $categoryModel = Mage::getModel('ulula_mercadolibre/category')->load($mage_category_id, 'mage_category_id');
        $categoryModel->setMageCategoryId($mage_category_id);
        $categoryModel->setMeliCategoryId($meli_category_id);
        $layout = Mage::getSingleton('core/layout');
        $block = $layout->createBlock('ulula_mercadolibre/adminhtml_catalog_categories');
        $category = $block->getCategoriesFromMeli($meli_category_id); 
        $categoryModel->setCategoryName($category_name);
        $categoryModel->setCategoryData(json_encode($category));
        $attributes = Mage::helper('ulula_mercadolibre/attribute')->setAttributes($meli_category_id);
        $categoryModel->setCategoryAttributes(json_encode($attributes));
        if ($categoryModel->save()){
            echo $this->__('Category mapped successfully.');
        }
        else{
            echo $this->__('There was an error trying to map the category.');
        };
    }

    public function saveProductAction()
    {
        $meli_category_id = $this->getRequest()->getParam('meli_category_id');
        $mage_product_id = $this->getRequest()->getParam('pid');
        $category_name = $this->getRequest()->getParam('category_name');
        $categoryModel = Mage::getModel('ulula_mercadolibre/category')->load($meli_category_id, 'meli_category_id');
        $categoryModel->setMeliCategoryId($meli_category_id);
        $layout = Mage::getSingleton('core/layout');
        $block = $layout->createBlock('ulula_mercadolibre/adminhtml_catalog_categories');
        $category = $block->getCategoriesFromMeli($meli_category_id); 
        $categoryModel->setCategoryName($category_name);
        $categoryModel->setCategoryData(json_encode($category));
        $attributes = Mage::helper('ulula_mercadolibre/attribute')->setAttributes($meli_category_id);
        $categoryModel->setCategoryAttributes(json_encode($attributes));
        if ($categoryModel->save()){
            $product = Mage::getModel('catalog/product')->load($mage_product_id);
            $product->setMeliCategoryId($meli_category_id);
            $product->save();
            echo $this->__('Category mapped successfully.');
        }
        else{
            echo $this->__('There was an error trying to map the category.');
        };
    }
 }
 