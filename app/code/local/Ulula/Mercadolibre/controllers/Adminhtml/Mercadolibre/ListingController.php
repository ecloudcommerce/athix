<?php
/**
 * 
 * @category    Ulula
 * @package     Ulula_Mercadolibre
 * @copyright   Copyright (c) 2017 Ulula IT (http://ulula.net)
 * @author      Gaston De Marsico <gdemarsico@ulula.net>
 */

class Ulula_Mercadolibre_Adminhtml_Mercadolibre_ListingController extends Mage_Adminhtml_Controller_Action
{

	protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')
            ->isAllowed('mercadolibre/listing');
    }

    public function indexAction()
    {
    	$_helper = Mage::helper('ulula_mercadolibre/attribute');
        $this->_title($this->__('Mercadolibre'))->_title($this->__('Listing Tags'));
        $this->loadLayout();
        $this->_setActiveMenu('mercadolibre/listing');
        $this->_addContent($this->getLayout()->createBlock('ulula_mercadolibre/adminhtml_mercadolibre_listing'));
        $this->renderLayout();
    }
}