<?php
/**
 * 
 * @category    Ulula
 * @package     Ulula_Mercadolibre
 * @copyright   Copyright (c) 2017 Ulula IT (http://ulula.net)
 * @author      Gaston De Marsico <gdemarsico@ulula.net>
 */

class Ulula_Mercadolibre_Adminhtml_NotificationsController extends Mage_Adminhtml_Controller_Action
{

    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')
            ->isAllowed('mercadolibre/notifications');
    }

    public function indexAction()
    {
        $this->_title($this->__('Mercadolibre'))->_title($this->__('Notifications'));
        $this->loadLayout();
        $this->_setActiveMenu('mercadolibre/notification');
        $this->_addContent($this->getLayout()->createBlock('ulula_mercadolibre/adminhtml_mercadolibre_notification'));
        $this->renderLayout();
    }

    public function gridAction()
    {
        $this->loadLayout();
        $this->getResponse()->setBody(
            $this->getLayout()->createBlock('ulula_mercadolibre/adminhtml_mercadolibre_notification_grid')->toHtml()
        );
    }
 
    public function exportIemCsvAction()
    {
        $fileName = 'meli_Notifications.csv';
        $grid = $this->getLayout()->createBlock('ulula_mercadolibre/adminhtml_mercadolibre_notification_grid');
        $this->_prepareDownloadResponse($fileName, $grid->getCsvFile());
    }
 
    public function exportnotificationExcelAction()
    {
        $fileName = 'meli_Notifications.xml';
        $grid = $this->getLayout()->createBlock('ulula_mercadolibre/adminhtml_mercadolibre_notification_grid');
        $this->_prepareDownloadResponse($fileName, $grid->getExcelFile($fileName));
    }

    public function deleteallAction()
    {
        $sql = 'TRUNCATE TABLE ulula_mercadolibre_notification';
        $resource = Mage::getSingleton('core/resource');
        $writeConnection = $resource->getConnection('core_write');
        $writeConnection->raw_query($sql);
        $this->_redirect('*/*/index');
    }

    public function massDeleteAction()
    {
        $ids = $this->getRequest()->getParam('id');      
        if(!is_array($ids)) {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('ulula_mercadolibre')->__('Please select notification(s).'));
        } 
        else {
            try {
                $notification = Mage::getModel('ulula_mercadolibre/notification');
                foreach ($ids as $id) {
                    $notification->load($id)->delete();
                }
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('ulula_mercadolibre')->__(
                        'Total of %d record(s) were deleted.', count($ids)
                ));
            } 
            catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        } 
        $this->_redirect('*/*/index');
    }

}