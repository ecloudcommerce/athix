<?php
/**
 * 
 * @category    Ulula
 * @package     Ulula_Mercadolibre
 * @copyright   Copyright (c) 2018 Ulula IT (http://ulula.net)
 * @author      Gaston De Marsico <gdemarsico@ulula.net>
 */

$installer = $this;
$installer->startSetup();

$installer->getConnection()
	->addColumn(
		$installer->getTable('ulula_mercadolibre/item'),
		'product_id', 
		array(
		    'type'      => Varien_Db_Ddl_Table::TYPE_INTEGER,
		    'nullable'  => true,
		    'after'     => 'permalink', 
		    'comment'   => 'Product Id'
		    )
);

$installer->endSetup(); 