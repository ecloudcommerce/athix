﻿<?php


class Vbw_Vartools_Helper_Data
    extends Mage_Core_Helper_Abstract
{

    /**
     * get the files from a var directory
     *
     * @param $dir
     * @return array
     */
    public function getVarDirectoryFiles ($dir)
    {
        /**
         * @var $iterator DirectoryIterator
         * @var $item DirectoryIterator
         */
        $return = array();
        if ($this->isVarDirectory($dir)) {
            $iterator = new DirectoryIterator($this->getVarDirectoryPath($dir));
            foreach ($iterator AS $item) {
                if (!$item->isDir()
                        && !$item->isDot()
                        && !$item->isLink()
                        && substr($item->getFilename(),0,1) != '.') {
                    $return[] = array (
                        'name' => $item->getFilename(),
                        'size' => $item->getSize(),
                        'modified' => $item->getMTime()
                        );
                }
            }
            usort($return,array($this,'sortByName'));
        }
        return $return;
    }

    /**
     * sorting method for
     *
     * @param $a
     * @param $b
     * @return bool
     */
    public function sortByName ($a,$b)
    {
        if (isset($a['name'])) {
            if ($a['name'] > $b['name']) {
                return 1;
            } elseif ($a['name'] < $b['name']) {
                return -1;
            }
        } else {
            if ($a > $b) {
                return 1;
            } elseif ($a < $b) {
                return -1;
            }
        }
        return 0;
    }

    /**
     * get all the directories in the var
     *
     * @return array
     */
    public function getVarDirectories ()
    {
        /**
         * @var $dirIterator DirectoryIterator
         * @var $dir DirectoryIterator
         */
        $dirIterator = new DirectoryIterator($this->getVarRoot());
        $return = array();
        foreach ($dirIterator AS $dir) {
            if ($dir->isDir()
                    && !$dir->isDot()) {
                $return[$dir->getFilename()] = $dir->getFilename();
            }
        }
        $return['import'] = 'log/import';
        $return["log/equis"] = "log/equis";
        usort($return,array($this,'sortByName'));
        return $return;
    }

    /**
     * get the var root directory, may need changing for windows compat.
     *
     * @param boolean $realpath
     * @return string
     */
    public function getVarRoot ($realpath = false)
    {
        if ($realpath) {
            return realpath(Mage::getRoot() ."/../var");
        }
        return Mage::getRoot() ."/../var";
    }

    /**
     * get a directory path
     *
     * @param $dir
     * @return string
     */
    public function getVarDirectoryPath($dir)
    {
        return $this->getVarRoot() ."/". $dir;
    }

    /**
     * get var file path
     *
     * @param $dir
     * @param $file
     * @return string
     */
    public function getVarFilePath($dir,$file)
    {
        return $this->getVarDirectoryPath($dir) ."/". $file;
    }


    /**
     * var root must exist, dir must exist and
     * the dir must be a sub directory of var. (prevents hacking with ..)
     *
     * @param $dir
     * @return bool
     */
    public function isVarDirectory($dir)
    {
        $dirPath = $this->getVarDirectoryPath($dir);
        $realRoot = $this->getVarRoot(true);
        $realDir = realpath($dirPath);
        if ($realRoot && $realDir
                && substr($realDir,0,strlen($realRoot)) == $realRoot) {
            return true;
        }
        return false;
    }

    /**
     * validate a file.
     *
     * @param $dir
     * @param $file
     * @return bool
     */
    public function isVarFile ($dir,$file)
    {
        if (is_array($file)) {
            foreach ($file AS $f) {
                if (!$this->isVarFile($dir,$f)) {
                    return false;
                }
            }
            return true;
        } else {
            if ($this->isVarDirectory($dir)) {
                $dirPath = realpath($this->getVarDirectoryPath($dir));
                $filePath = realpath($this->getVarFilePath($dir,$file));
                if ($dirPath && $filePath
                    && substr($filePath,0,strlen($dirPath)) == $dirPath) {
                    return true;
                }
            }
            return false;
        }
    }

    /**
     * @param $dir
     * @param $file
     * @return boolean
     */
    public function isVarLocation ($dir,$file)
    {
        $filePath = $this->getVarFilePath($dir,$file);
        $info = pathinfo($filePath);
        $realDir = realpath($info['dirname']);
        $realRoot = $this->getVarRoot(true);
        if ($realRoot && $realDir
            && substr($realDir,0,strlen($realRoot)) == $realRoot) {
            return true;
        }
        return false;
    }

}
