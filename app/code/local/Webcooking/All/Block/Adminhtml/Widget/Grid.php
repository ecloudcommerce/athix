<?php

/**
 * Vincent Enjalbert
 *
 * Version Française :
 * *****************************************************************************
 *
 * Notification de la Licence
 *
 * Ce fichier source est sujet au CLUF
 * qui est fourni avec ce module dans le fichier LICENSE-FR.txt.
 * Il est également disponible sur le web à l'adresse suivante:
 * http://www.enjalbert.net/licences/magento/LICENSE-FR.txt
 *
 * =============================================================================
 *        NOTIFICATION SUR L'UTILISATION DE L'EDITION MAGENTO
 * =============================================================================
 * Ce module est conçu pour l'édition COMMUNITY de Magento
 * WebCooking ne garantit pas le fonctionnement correct de cette extension
 * sur une autre édition de Magento excepté l'édition COMMUNITY de Magento.
 * WebCooking ne fournit pas de support d'extension en cas
 * d'utilisation incorrecte de l'édition.
 * =============================================================================
 *
 * English Version :
 * *****************************************************************************
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE-EN.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.web-cooking.net/licences/magento/LICENSE-EN.txt
 *
 * =============================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =============================================================================
 * This package designed for Magento COMMUNITY edition
 * WebCooking does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * WebCooking does not provide extension support in case of
 * incorrect edition usage.
 * =============================================================================
 *
 * @category   Webcooking
 * @package    Webcooking_All
 * @copyright  Copyright (c) 2011-2014 Vincent René Lucien Enjalbert
 * @copyright  Based on work of Inchoo http://inchoo.net/magento/enhanced-export/
 * @license    http://www.web-cooking.net/licences/magento/LICENSE-EN.txt
 * @license    http://inchoo.net/magento/enhanced-export/
 */


class Webcooking_All_Block_Adminhtml_Widget_Grid extends Mage_Adminhtml_Block_Widget_Grid {

    protected $_exportPageSize = 50000;
    
    protected function _prepareColumns()
    {
        return parent::_prepareColumns();
    }
 
    
    protected function _exportXmlItem(Varien_Object $item, Varien_Io_File $adapter)
    {
        $indexes = array();
        foreach ($this->_columns as $column) {
            if (!$column->getIsSystem()) {
                $indexes[] = $column->getIndex();
            }
        }
        $adapter->streamWrite($item->toXml($indexes));
    }
    
    public function getExcelFileEnhanced()
    {
    	$this->_isExport = true;
        $this->_prepareGrid();
 
        $io = new Varien_Io_File();
 
        $path = Mage::getBaseDir('var') . DS . 'export' . DS; //best would be to add exported path through config
        $name = md5(microtime());
        $file = $path . DS . $name . '.xml';
 
        /**
         * It is possible that you have name collision (summer/winter time +1/-1)
         * Try to create unique name for exported .csv file
         */
        while (file_exists($file)) {
        	sleep(1);
        	$name = md5(microtime());
        	$file = $path . DS . $name . '.xml';
        }
 
        $io->setAllowCreateFolders(true);
        $io->open(array('path' => $path));
        $io->streamOpen($file, 'w+');
        $io->streamLock(true);
        $io->streamWriteCsv($this->_getExportHeaders());
        //$this->_exportPageSize = load data from config
        $this->_exportIterateCollectionEnhanced('_exportXmlItem', array($io));
 
        if ($this->getCountTotals()) {
            $io->streamWriteCsv($this->_getExportTotals());
        }
 
        $io->streamUnlock();
        $io->streamClose();
 
        return array(
            'type'  => 'filename',
            'value' => $file,
            'rm'    => false // can delete file after use
        );
    }
    
    public function getXmlFileEnhanced()
    {
    	$this->_isExport = true;
        $this->_prepareGrid();
 
        $io = new Varien_Io_File();
 
        $path = Mage::getBaseDir('var') . DS . 'export' . DS; //best would be to add exported path through config
        $name = md5(microtime());
        $file = $path . DS . $name . '.xml';
 
        /**
         * It is possible that you have name collision (summer/winter time +1/-1)
         * Try to create unique name for exported .csv file
         */
        while (file_exists($file)) {
        	sleep(1);
        	$name = md5(microtime());
        	$file = $path . DS . $name . '.xml';
        }
 
        $io->setAllowCreateFolders(true);
        $io->open(array('path' => $path));
        $io->streamOpen($file, 'w+');
        $io->streamLock(true);
        $io->streamWrite($this->_getExportXmlHeaders());
        //$this->_exportPageSize = load data from config
        $this->_exportIterateCollectionEnhanced('_exportXmlItem', array($io));
 
        if ($this->getCountTotals()) {
            $io->streamWrite($this->_getExportXmlTotals());
        }
 
        $io->streamUnlock();
        $io->streamClose();
 
        return array(
            'type'  => 'filename',
            'value' => $file,
            'rm'    => false // can delete file after use
        );
    }
    
    protected function _getExportXmlHeaders()
    {
        $xml = '<?xml version="1.0" encoding="UTF-8"?>';
        $xml.= '<items>';
        return $xml;
    }

    /**
     * Retrieve Totals row array for Export
     *
     * @return array
     */
    protected function _getExportXmlTotals()
    {
        $xml = '</items>';
        return $xml;
    }
    
    
    public function getCsvFileEnhanced()
    {
    	$this->_isExport = true;
        $this->_prepareGrid();
 
        $io = new Varien_Io_File();
 
        $path = Mage::getBaseDir('var') . DS . 'export' . DS; //best would be to add exported path through config
        $name = md5(microtime());
        $file = $path . DS . $name . '.csv';
 
        /**
         * It is possible that you have name collision (summer/winter time +1/-1)
         * Try to create unique name for exported .csv file
         */
        while (file_exists($file)) {
        	sleep(1);
        	$name = md5(microtime());
        	$file = $path . DS . $name . '.csv';
        }
 
        $io->setAllowCreateFolders(true);
        $io->open(array('path' => $path));
        $io->streamOpen($file, 'w+');
        $io->streamLock(true);
        $io->streamWriteCsv($this->_getExportHeaders());
        //$this->_exportPageSize = load data from config
        $this->_exportIterateCollectionEnhanced('_exportCsvItem', array($io));
 
        if ($this->getCountTotals()) {
            $io->streamWriteCsv($this->_getExportTotals());
        }
 
        $io->streamUnlock();
        $io->streamClose();
 
        return array(
            'type'  => 'filename',
            'value' => $file,
            'rm'    => false // can delete file after use
        );
    }
    
    
    
    public function _exportIterateCollectionEnhanced($callback, array $args)
    {
        $originalCollection = $this->getCollection();
        $count = null;
        $page  = 1;
        $lPage = null;
        $break = false;

        while ($break !== true) {
            $break = $this->_exportIterateCollectionPageEnhanced($originalCollection, $page, $count, $callback, $args);
            
            $page ++;
            /*Mage::log('PAGE '.$page, null, 'debug.export.perf2.log');
            Mage::log(memory_get_usage(true)/(1024*1024).'Mb', null, 'debug.export.perf2.log');
            $collection = clone $originalCollection;
            $collection->setPageSize($this->_exportPageSize);
            $collection->setCurPage($page);
            $collection->load();
            if (is_null($count)) {
                $count = $collection->getSize();
                $lPage = $collection->getLastPageNumber();
            }
            if ($lPage == $page) {
                $break = true;
            }
            $page ++;

            foreach ($collection as $item) {
                call_user_func_array(array($this, $callback), array_merge(array($item), $args));
            }*/
        }
    }
    
    public function _exportIterateCollectionPageEnhanced($originalCollection, $page, $count, $callback, $args)
    {
        //Mage::log('PAGE '.$page, null, 'debug.export.perf2.log');
        //Mage::log(memory_get_usage(true)/(1024*1024).'Mb', null, 'debug.export.perf2.log');
        $collection = clone $originalCollection;
        $collection->setPageSize($this->_exportPageSize);
        $collection->setCurPage($page);
        $collection->load();
        if (is_null($count)) {
            $count = $collection->getSize();
            $lPage = $collection->getLastPageNumber();
        }
        if ($lPage == $page) {
            $break = true;
        }

        foreach ($collection as $item) {
            call_user_func_array(array($this, $callback), array_merge(array($item), $args));
        }
        return $break;
    }
  
}