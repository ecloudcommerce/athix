<?php

/**
 * Vincent Enjalbert
 *
 * Version Française :
 * *****************************************************************************
 *
 * Notification de la Licence
 *
 * Ce fichier source est sujet au CLUF
 * qui est fourni avec ce module dans le fichier LICENSE-FR.txt.
 * Il est également disponible sur le web à l'adresse suivante:
 * http://www.web-cooking.net/licences/magento/LICENSE-FR.txt
 *
 * =============================================================================
 *        NOTIFICATION SUR L'UTILISATION DE L'EDITION MAGENTO
 * =============================================================================
 * Ce module est conçu pour l'édition COMMUNITY de Magento
 * WebCooking ne garantit pas le fonctionnement correct de cette extension
 * sur une autre édition de Magento excepté l'édition COMMUNITY de Magento.
 * WebCooking ne fournit pas de support d'extension en cas
 * d'utilisation incorrecte de l'édition.
 * =============================================================================
 *
 * English Version :
 * *****************************************************************************
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE-EN.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.web-cooking.net/licences/magento/LICENSE-EN.txt
 *
 * =============================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =============================================================================
 * This package designed for Magento COMMUNITY edition
 * WebCooking does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * WebCooking does not provide extension support in case of
 * incorrect edition usage.
 * =============================================================================
 *
 * @category   Webcooking
 * @package    Webcooking_GoogleUniversalAnalytics
 * @copyright  Copyright (c) 2011-2014 Vincent René Lucien Enjalbert
 * @license    http://www.web-cooking.net/licences/magento/LICENSE-EN.txt
 */
class Webcooking_GoogleUniversalAnalytics_Helper_Data extends Mage_Core_Helper_Abstract {

    const TRANSACTION_EVENT_PLACE_ORDER = 'place_order';
    const TRANSACTION_EVENT_CHANGE_ORDER_STATUS = 'change_order_status';
    const TRANSACTION_EVENT_CREATE_INVOICE = 'create_invoice';
    const TRANSACTION_EVENT_CAPTURE_INVOICE = 'capture_invoice';
    
    function generateUuid() {
        return sprintf( '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
            // 32 bits for "time_low"
            mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ),

            // 16 bits for "time_mid"
            mt_rand( 0, 0xffff ),

            // 16 bits for "time_hi_and_version",
            // four most significant bits holds version number 4
            mt_rand( 0, 0x0fff ) | 0x4000,

            // 16 bits, 8 bits for "clk_seq_hi_res",
            // 8 bits for "clk_seq_low",
            // two most significant bits holds zero and one for variant DCE1.1
            mt_rand( 0, 0x3fff ) | 0x8000,

            // 48 bits for "node"
            mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff )
        );
    }
    /*
    public function generateUuid() {
        $md5 = md5(uniqid('', true));
        return substr($md5, 0, 8) . '-' .
                substr($md5, 8, 4) . '-' .
                substr($md5, 12, 4) . '-' .
                substr($md5, 16, 4) . '-' .
                substr($md5, 20, 12);
    }*/

    public function getClientId($createIfNotFound = true) {
        $coreSession = Mage::getSingleton('core/session');
        if(!$coreSession->getGuaClientId() && $createIfNotFound) {
            $coreSession->setGuaClientId($this->generateUuid());
            if($coreSession->getGuaClientId()) { // If somehow $coreSession->getGuaClientId() return empty, there is risks of infinite loop when sending the event
                $params = array(
                    Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::SESSION_CONTROL => 'start'
                );
                Mage::getModel('googleuniversalanalytics/measurement_protocol')
                        ->sendEvent('backend', 'session', 'start', 1, false, $coreSession->getGuaClientId(), null, $params);
            }
        }
        return $coreSession->getGuaClientId();
    }

    public function getAccountId($store = null) {
        return Mage::getStoreConfig('googleuniversalanalytics/general/account', $store);
    }

    public function isActive($store = null) {
        return Mage::getStoreConfig('googleuniversalanalytics/general/active', $store);
    }

    public function isGoogleAnalyticsAvailable($store = null) {
        return $this->getAccountId($store) && $this->isActive($store);
    }

    public function isEnhancedEcommerceActivated($store = null) {
        return ($this->isTagManagerModeActivated($store) || $this->isGoogleAnalyticsAvailable($store)) && Mage::getStoreConfigFlag('googleuniversalanalytics/enhanced_ecommerce/active', $store);
    }

    /**
     * @deprecated
     * abandoned
     */
    public function isTagManagerModeActivated($store = null) {
        return !$this->isGoogleAnalyticsAvailable($store) && Mage::getStoreConfigFlag('googleuniversalanalytics/enhanced_ecommerce/tag_manager', $store);
    }
    
    public function isDemographicsActivated($store = null) {
        return Mage::getStoreConfigFlag('googleuniversalanalytics/options/enable_demographics', $store);
    }
    
    public function isForceSSLActivated($store = null) {
        return Mage::getStoreConfigFlag('googleuniversalanalytics/options/force_ssl', $store);
    }
    
    public function isEnhancedLinkActivated($store = null) {
        return Mage::getStoreConfigFlag('googleuniversalanalytics/options/link_id_required', $store);
    }
    
    public function isLinkerActivated($store = null) {
        return Mage::getStoreConfigFlag('googleuniversalanalytics/options/linker_autolink', $store);
    }
    
    public function getLinkerAutoLink($store = null) {
        return Mage::getStoreConfig('googleuniversalanalytics/options/linker_autolink', $store);
    }
    
    public function isLinkerParametersInAnchor($store = null) {
        return Mage::getStoreConfigFlag('googleuniversalanalytics/options/linker_parameters_in_anchor', $store);
    }
    
    public function isLinkerCrossDomainAutoLinkingForForms($store = null) {
        return Mage::getStoreConfigFlag('googleuniversalanalytics/options/cross_domain_auto_linking_for_forms', $store);
    }
    
    public function isOptOutActivated($store = null) {
        return Mage::getStoreConfigFlag('googleuniversalanalytics/options/enable_opt_out', $store);
    }
    
    public function isIPAnonymized($store = null) {
        return Mage::getStoreConfigFlag('googleuniversalanalytics/options/anonymize_ip', $store);
    }
    
    public function formatTrackerName($trackerName) {
        return preg_replace('%[^a-zA-Z0-9]$%', '', $trackerName);
    }
    
    public function getTrackerName($addPoint = true) {
        $trackerName = $this->formatTrackerName(Mage::getStoreConfig('googleuniversalanalytics/options/tracker_name'));
        if($trackerName && $addPoint) {
            $trackerName .= '.';
        }
        return $trackerName;
    }
    
    public function getGaCreateOptions($store = null, $asJSON = true) {
        $trackerName = $this->getTrackerName(false);
        $sampleRate = Mage::getStoreConfig('googleuniversalanalytics/options/sample_rate');
        $siteSpeedSampleRate = Mage::getStoreConfig('googleuniversalanalytics/options/site_speed_sample_rate');
        $alwaysSendReferrer = Mage::getStoreConfigFlag('googleuniversalanalytics/options/always_send_referrer');
        $allowAnchor = Mage::getStoreConfigFlag('googleuniversalanalytics/options/allow_anchor');
        $cookieName = Mage::getStoreConfig('googleuniversalanalytics/options/cookie_name');
        $cookieDomain = Mage::getStoreConfig('googleuniversalanalytics/options/cookie_domain');
        $cookieExpiration = Mage::getStoreConfig('googleuniversalanalytics/options/cookie_expiration');
        $legacyCookieDomain = Mage::getStoreConfig('googleuniversalanalytics/options/legacy_cookie_domain');
        $allowLinker = Mage::getStoreConfigFlag('googleuniversalanalytics/options/allow_linker');
        $userId = false;
        if(Mage::getSingleton('customer/session')->isLoggedIn()) {
            $userId = Mage::getSingleton('customer/session')->getCustomer()->getId();
        }
        $options = array();
        if($trackerName) {
            $options['name'] = $trackerName;
        }
        if($sampleRate != 100) {
            $options['sampleRate'] = $sampleRate;
        }
        if($siteSpeedSampleRate != 1) {
            $options['siteSpeedSampleRate'] = $siteSpeedSampleRate;
        }
        if($alwaysSendReferrer) { // default = false
            $options['alwaysSendReferrer'] = $alwaysSendReferrer;
        }
        if(!$allowAnchor) { // default = true
            $options['allowAnchor'] = $allowAnchor;
        }
        if($cookieName) {
            $options['cookieName'] = $cookieName;
        }
        if($cookieDomain) {
            $options['cookieDomain'] = $cookieDomain;
        }
        if($cookieExpiration) {
            $options['cookieExpires'] = $cookieExpiration;
        }
        if($legacyCookieDomain) {
            $options['legacyCookieDomain'] = $legacyCookieDomain;
        }
        if($allowLinker) { //default = false
            $options['allowLinker'] = $allowLinker;
        }
        if($userId) {
            $options['userId'] = $userId;
        }
        if($asJSON) {
            return Zend_Json::encode($options);
        }
        return $options;
    }
    
    public function getRemarketingConversionId($store = null) {
        return Mage::getStoreConfig('googleuniversalanalytics/remarketing/conversion_id', $store);
    }
    
    public function useRemarketing($store = null) {
        if(!Mage::getStoreConfigFlag('googleuniversalanalytics/remarketing/active', $store)) {
            return false;
        }
        if(!$this->getRemarketingConversionId($store)) {
            return false;
        }
        return true;
    }
    
    public function getAdwordsConversionId($store = null) {
        return Mage::getStoreConfig('googleuniversalanalytics/adwords/conversion_id', $store);
    }
    
    public function getAdwordsConversionLabel($store = null) {
        return Mage::getStoreConfig('googleuniversalanalytics/adwords/label', $store);
    }
    
    public function getAdwordsConversionValue($store = null) {
        return Mage::getStoreConfig('googleuniversalanalytics/adwords/value', $store);
    }
    
    public function useAdwords($store = null) {
        if(!Mage::getStoreConfigFlag('googleuniversalanalytics/adwords/active', $store)) {
            return false;
        }
        if(!$this->getAdwordsConversionId($store)) {
            return false;
        }
        return true;
    }

    public function addCustomDimension($dimensionIndex, $dimensionValue, $addScriptTags=false) {
        $js = '';
        if($addScriptTags) {
            $js .= '<script>' . "\n";
        }
        $js .= "ga('set','dimension{$dimensionIndex}','" . $dimensionValue . "');" . "\n";
        if($addScriptTags) {
            $js .= '</script>' . "\n";
        }
        return $js;
    }
    
}