<?php

require_once 'blprocessor.php';

$processor = new Bl_Error_Processor();

if (isset($reportData) && is_array($reportData)) {
    $processor->saveReport($reportData);
}

$processor->processReport();
