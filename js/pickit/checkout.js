function cargarShippingMethod() {
    var pickitLink = jQuery('#pickit_link');
    var version = pickitLink.attr("data-fancybox");
    var fbv = pickitLink.attr("data-fancybox-version");

    if(version == 0 && fbv == 0) {
        //alert('viejo');
        jQuery(document).ready(function() {
            window.addEventListener('message', function (event) {
                if (event.data == 'close') {
                    jQuery.fancybox.close();
                }
            }, false);
            pickitLink.fancybox({
                'width'				: 985,
                'height'			: 637,
                'showCloseButton'	: false,
                'onClosed'			: function() {
                    jQuery.ajax({
                        url: pickitLink.attr("data-url"),
                        type: "GET",
                        beforeSend: function() {
                            jQuery("#loader-container").append("<p id=\"loader\">Cargando...<img src='" + pickitLink.attr("data-loader") + "' /></p>");
                            jQuery('li.pickit').hide();
                        },
                        success: function (response) {
                            shipping.save();
                        }
                    });
                }
            });
        });
    } else {
        jQuery(document).ready(function() {
            window.addEventListener('message', function (event) {
                if (event.data == 'close') {
                    jQuery.fancybox.close();
                }
            }, false);
            pickitLink.click(function() {
                pickitLink.fancybox({
                    maxWidth	: 1200,
                    autoSize	: true,
                    maxHeight	: 750,
                    fitToView	: false,
                    width		: 985,
                    height		: 637,
                    autoSize	: false,
                    closeBtn	: false,
                    openEffect	: 'none',
                    closeEffect	: 'none',
                    iframe: {
                        scrolling : 'no',
                        preload   : true
                    },
                    beforeClose: function() {
                        jQuery.ajax({
                            url: pickitLink.attr("data-url"),
                            type: "GET",
                            beforeSend: function() {
                                jQuery("#loader-container").append("<p id=\"loader\">Cargando...<img src='" + pickitLink.attr("data-loader") + "' /></p>");
                                jQuery('li.pickit').hide();
                            },
                            success: function (response) {
                                shipping.save();
                            }
                        });
                    }
                });
            });
        });
    }
}

function setTipoEnvioDomicilio(method) {
    jQuery.ajax({
        url: "/pickit/indexpickit/setPunto?tipoEnvio=" + method,
        type: "GET"
    })
}


function openPickit() {
    var pickitLink = jQuery('#pickit_link');
    pickitLink.trigger("click");
}
