<?php
require_once(dirname(__DIR__) . '../../../app/Mage.php');

ini_set('max_execution_time', 6000);
umask(0);
Mage::app();
ini_set('display_errors', 1);

const SEP = ",";
class Integration
{
	const ATHIX_WEBSITE_CODE = "base";
	const DIADORA_WEBSITE_CODE = "diadora";

	const DATE_FORMAT = "Y-m-d H:i:s";

	public $attrSimpleSync;
	public $sincAttrConfigurable;
	public $resource;
	public $readConnection;
	public $writeConnection;
	public $logFile;
	public $atributes;
	public $attributeIds;
	public $attributeTypes;
	public $productEntityTypeId;
	public $products;
	public $updatedProductCount;
	public $createdProducts;
	public $notCreatedProducts;
	public $notUpdatedProducts;
	public $notSimpleProducts;
	public $wrongWebsiteProducts;
	public $athixWebsiteId;
	public $diadoraWebsiteId;
	public $catalogSyncConfig;
	public $forceCreate;
	public $skuList;

	public function __construct($forceCreate)
	{
		$this->forceCreate = $forceCreate;
		// MySQL resources
		$this->resource = Mage::getSingleton('core/resource');
		$this->readConnection = $this->resource->getConnection('core_read');
		$this->writeConnection = $this->resource->getConnection('core_write');

		// Configuración del log
		$format = "Y-m-d_H-i-s";
		$dateStr = (new DateTime())->format($format);
		$this->logFile = $dateStr . ".log";

		//Atributos que se reciben en la consulta
		$this->attributes = array(
			"ProductoId",					// EAV attribute - equivalencia_equis: varchar
			"ProductoIdAnterior",
			"Precio",						// EAV attribute - price: decimal
			"CantDisponibleStock",			// 
			// "Marca",						// EAV attribute - marca: text / Marca: select
			// "Descripcion",				// EAV attribute - description: text
			// "Familia",					// EAV attribute set
			// "Linea",						// EAV attribute - linea:varchar
			// "SubLinea",					// Pareciera que este es el eav attribute "linea", y el campo linea no sé qué es
			// "Proveedor",					// EAV attribute - proveedor: varchar
			// "Material",					// EAV attribute - material: varchar
			// "Color",						// EAV attribute - color: varchar
			// "Curva",						// 
			// "CurvaCantidadUnidades",		// 
		);

		$this->attributeMapping = array(
			/* equisAttribute => array(
					magentoAttribute => 
					type => simple/option/multiselect
				) 
			*/
			"ProductoId" => array(
				"magentoAttribute" => "equivalencia_equis",
				"type" => "simple"
			),
			"ProductoIdAnterior" => array(
				"magentoAttribute" => "sku",
				"type" => "simple"
			),
			"Descripcion" => array(
				"magentoAttribute" => "name",
				"type" => "simple"
			),
			"Familia" => array(
				"magentoAttribute" => null,
				"type" => null
				// attribute_set
			),
			"Linea" => array(
				"magentoAttribute" => "genero",
				"type" => "option"
			),
			"SubLinea" => array(
				"magentoAttribute" => "linea",
				"type" => "simple"
			),
			"Marca" => array(
				"magentoAttribute" => "manufacturer",
				"type" => "option"
			),
			"Proveedor" => array(
				"magentoAttribute" => "proveedor",
				"type" => "simple"
			),
			"Material" => array(
				"magentoAttribute" => "material",
				"type" => "simple"
			),
			"Color" => array(
				"magentoAttribute" => "color",
				"type" => "multiselect"
			),
			"Curva" => array(
				"magentoAttribute" => "talle",
				"type" => "option"
			),
			"CurvaCantidadUnidades" => array(
				"magentoAttribute" => null,
				"type" => "simple"
			),
			"Precio" => array(
				"magentoAttribute" => "price",
				"type" => "simple"
			),
			"PrecioSugerido" => array(
				"magentoAttribute" => null,
				"type" => null
			),
			"CantDisponibleStock" => array(
				"magentoAttribute" => null,
				"type" => null
				// stock_item
			)
		);

		$this->createdProducts = array();
		$this->notCreatedProducts = array();
		$this->athixWebsiteId = $this->getWebsiteIdByCode(self::ATHIX_WEBSITE_CODE);
		$this->diadoraWebsiteId = $this->getWebsiteIdByCode(self::DIADORA_WEBSITE_CODE);
		$this->updatedProductCount = 0;
		$this->catalogSyncConfig = Mage::getStoreConfig("equisconfig/catalogimport");
	}

	/* ---------------- FUNCIONES GET MYSQL ---------------- */

	protected function getEntityType($entityCode)
	{
		$query = 'SELECT `entity_type_id` FROM ' . $this->resource->getTableName('eav_entity_type') . ' WHERE `entity_type_code` = "' . $entityCode . '";';
		$results = $this->readConnection->fetchAll($query);
		if (count($results) > 0) {
			return $results[0]['entity_type_id'];
		}
	}

	protected function getAttributeId($attributeCode, $entityType)
	{
		$query = 'SELECT `attribute_id` FROM ' . $this->resource->getTableName('eav_attribute') . ' WHERE `attribute_code` = "' . $attributeCode . '" and `entity_type_id` = "' . $entityType . '";';
		$results = $this->readConnection->fetchAll($query);
		if (count($results) > 0) {
			return $results[0]['attribute_id'];
		}
	}

	protected function getAttributeSetId($attributeSetName, $productType)
	{
		$query = 'SELECT `attribute_set_id` FROM ' . $this->resource->getTableName('eav_attribute_set') . ' WHERE `attribute_set_name` = "' . $attributeSetName . '" and `entity_type_id` = "' . $productType . '";';
		$results = $this->readConnection->fetchAll($query);
		if (count($results) > 0) {
			return $results[0]['attribute_set_id'];
		}
	}

	protected function getAttributeType($attributeId)
	{
		$query = "SELECT `backend_type` FROM `eav_attribute` WHERE `attribute_id` = '" . $attributeId . "';";
		$results = $this->readConnection->fetchAll($query);
		if (count($results) > 0) {
			return $results[0]['backend_type'];
		}
	}

	protected function loadSkuList()
	{
		$query = 'SELECT  `sku`  FROM ' . $this->resource->getTableName('catalog_product_entity');
		$results = $this->readConnection->fetchAll($query);
		$this->skuList = array_map(function ($el) {
			return $el["sku"];
		}, $results);
	}

	protected function loadEquisProduct()
	{
		$query = 'SELECT `value` FROM `catalog_product_entity_varchar`  WHERE `attribute_id` = ' .$this->attributeIds["equivalencia_equis"];
		$results = $this->readConnection->fetchAll($query);
		$this->equisList = array_map(function ($el) {
			return $el["value"];
		}, $results);
	}

	protected function getProduct($sku)
	{
		$query = 'SELECT * FROM ' . $this->resource->getTableName('catalog_product_entity') . ' WHERE `sku` = "' . $sku . '";';
		$results = $this->readConnection->fetchAll($query);
		if (count($results) > 0) {
			return $results[0];
		} else {
			return false;
		}
	}

	protected function getProductByCode($equisCode)
	{
		$attributeTable = "catalog_product_entity_" . $this->attributeTypes["equivalencia_equis"];
		$query = 'SELECT * FROM ' . $this->resource->getTableName('catalog_product_entity') . ' 
		INNER JOIN `' . $attributeTable . '` 
			ON `' . $attributeTable . '`.`entity_id` = ' . $this->resource->getTableName('catalog_product_entity') . '.`entity_id` 
				AND `' . $attributeTable . '`.`attribute_id` = "' . $this->attributeIds["equivalencia_equis"] . '" 
		WHERE `' . $attributeTable . '`.`value` = "' . $equisCode . '";';
		$results = $this->readConnection->fetchAll($query);
		if (count($results) > 0) {
			return $results[0];
		} else {
			return false;
		}
	}

	protected function getParentEntityId($childId)
	{
		$query = 'SELECT `parent_id` FROM `catalog_product_super_link` WHERE `product_id` = "' . $childId . '"';
		$results = $this->readConnection->fetchAll($query);
		if (count($results) > 0) {
			return $results[0]['parent_id'];
		}
		return null;
	}

	protected function getWebsiteIdByCode($websiteCode)
	{
		$query = 'SELECT `website_id` FROM `core_website` WHERE `code` = "' . $websiteCode . '"';
		$results = $this->readConnection->fetchAll($query);
		if (count($results) > 0) {
			return $results[0]['website_id'];
		}
		return null;
	}

	protected function getProductWebsiteId($productId)
	{
		$query = 'SELECT `website_id` FROM `catalog_product_website` WHERE `product_id` = "' . $productId . '"';
		$results = $this->readConnection->fetchAll($query);
		if (count($results) > 0) {
			$websiteIds = array_map(function ($row) {
				return $row["website_id"];
			}, $results);
			return $websiteIds;
		}
		return null;
	}

	protected function getStockItemId($productId)
	{
		$query = 'SELECT `item_id` FROM `cataloginventory_stock_item` WHERE `product_id` = "' . $productId . '"';
		$results = $this->readConnection->fetchAll($query);
		if (count($results) > 0) {
			return $results[0]['item_id'];
		}
		return null;
	}

	protected function getStockAmount($productId)
	{
		$query = 'SELECT `qty` FROM `cataloginventory_stock_item` WHERE `product_id` = "' . $productId . '"';
		$results = $this->readConnection->fetchAll($query);
		if (count($results) > 0) {
			return $results[0]['qty'];
		}
		return null;
	}

	protected function getOptionId($optionValue, $attributeCode)
	{
		$attributeId = $this->attributeIds[$attributeCode];
		$query = 'SELECT DISTINCT * FROM `eav_attribute_option` 
		INNER JOIN `eav_attribute_option_value` 
			ON `eav_attribute_option`.`option_id` = `eav_attribute_option_value`.`option_id` 
		WHERE `attribute_id` = "' . $attributeId . '" 
			AND `eav_attribute_option_value`.`value` = "' . $optionValue . '"';
		$results = $this->readConnection->fetchAll($query);
		if (count($results) > 0) {
			return $results[0]['option_id'];
		}
		return null;
	}

	/* ---------------- FUNCIONES UPDATE MYSQL ---------------- */

	protected function updateAttribute($attributeCode, $entityId, $value)
	{
		if (array_key_exists($attributeCode, $this->attributeTypes))
			$attributeType = $this->attributeTypes[$attributeCode];
		else
			$attributeType = $this->defaultAttributeTypes[$attributeCode];
		switch ($attributeType) {
			case "text":
				$this->updateText($this->attributeIds[$attributeCode], $entityId, $value);
				break;
			case "varchar":
				$this->updateVarchar($this->attributeIds[$attributeCode], $entityId, $value);
				break;
			case "int":
				$this->updateInt($this->attributeIds[$attributeCode], $entityId, $value);
				break;
			case "decimal":
				$this->updateDecimal($this->attributeIds[$attributeCode], $entityId, $value);
				break;
			default:
				// No existe el atributo
				break;
		}
	}

	protected function updateText($attributeId, $entityId, $value)
	{
		$selectQuery = 'SELECT `value` FROM `catalog_product_entity_text` WHERE `attribute_id` = "' . $attributeId . '" and `entity_id` = "' . $entityId . '";';
		$selectResults = $this->readConnection->fetchAll($selectQuery);
		if (count($selectResults) > 0) {
			$query = "UPDATE `catalog_product_entity_text` SET `catalog_product_entity_text`.`value` = " . $this->writeConnection->quote($value) . " WHERE `catalog_product_entity_text`.`entity_id` = '" . $entityId . "' and `catalog_product_entity_text`.`attribute_id` =  '" . $attributeId . "'";
		} else {
			$query = "INSERT INTO `catalog_product_entity_text` (`entity_type_id`, `attribute_id`, `entity_id`, `value`) VALUES (" . $this->productEntityTypeId . ", " . $attributeId . ", " . $entityId . ", '" . $value . "')";
		}
		return $this->writeConnection->exec($query);
	}

	protected function updateVarchar($attributeId, $entityId, $value)
	{
		$selectQuery = 'SELECT `value` FROM `catalog_product_entity_varchar` WHERE `attribute_id` = "' . $attributeId . '" and `entity_id` = "' . $entityId . '";';
		$selectResults = $this->readConnection->fetchAll($selectQuery);

		if (count($selectResults) > 0) {
			$query = "UPDATE `catalog_product_entity_varchar` SET `catalog_product_entity_varchar`.`value` = " . $this->writeConnection->quote($value) . " WHERE `catalog_product_entity_varchar`.`entity_id` = '" . $entityId . "' and `catalog_product_entity_varchar`.`attribute_id` =  '" . $attributeId . "'";
		} else {
			$query = "INSERT INTO `catalog_product_entity_varchar` (`entity_type_id`, `attribute_id`, `entity_id`, `value`) VALUES (" . $this->productEntityTypeId . ", " . $attributeId . ", " . $entityId . ", '" . $value . "')";
		}
		return $this->writeConnection->exec($query);
	}

	protected function updateInt($attributeId, $entityId, $value)
	{
		$selectQuery = 'SELECT `value` FROM `catalog_product_entity_int` WHERE `attribute_id` = "' . $attributeId . '" and `entity_id` = "' . $entityId . '";';
		$selectResults = $this->readConnection->fetchAll($selectQuery);
		if (count($selectResults) > 0) {
			$query = "UPDATE `catalog_product_entity_int` SET `catalog_product_entity_int`.`value` = '" . $value . "' WHERE `catalog_product_entity_int`.`entity_id` = '" . $entityId . "' and `catalog_product_entity_int`.`attribute_id` =  '" . $attributeId . "'";
		} else {
			$query = "INSERT INTO `catalog_product_entity_int` (`entity_type_id`, `attribute_id`, `entity_id`, `value`) VALUES (" . $this->productEntityTypeId . ", " . $attributeId . ", " . $entityId . ", " . $value . ")";
		}
		return $this->writeConnection->exec($query);
	}

	protected function updateDecimal($attributeId, $entityId, $value)
	{
		$selectQuery = 'SELECT `value` FROM `catalog_product_entity_decimal` WHERE `attribute_id` = "' . $attributeId . '" and `entity_id` = "' . $entityId . '";';
		$selectResults = $this->readConnection->fetchAll($selectQuery);
		if (count($selectResults) > 0) {
			$query = "UPDATE `catalog_product_entity_decimal` SET `catalog_product_entity_decimal`.`value` = '" . $value . "' WHERE `catalog_product_entity_decimal`.`entity_id` = '" . $entityId . "' and `catalog_product_entity_decimal`.`attribute_id` =  '" . $attributeId . "'";
		} else {
			$query = "INSERT INTO `catalog_product_entity_decimal` (`entity_type_id`, `attribute_id`, `entity_id`, `value`) VALUES (" . $this->productEntityTypeId . ", " . $attributeId . ", " . $entityId . ", " . $value . ")";
		}
		return $this->writeConnection->exec($query);
	}

	protected function updateAttributeSet($entityId, $entityType,  $value)
	{
		$query = "UPDATE `catalog_product_entity` SET `catalog_product_entity`.`attribute_set_id` = '" . $value . "' WHERE `catalog_product_entity`.`entity_id` = '" . $entityId . "' and `catalog_product_entity`.`entity_type_id` =  '" . $entityType . "'";
		return $this->writeConnection->exec($query);
	}

	function updateStock($id, $qty, $onlySetAvailable = false)
	{
		$query = "UPDATE `cataloginventory_stock_item` SET `qty` = '" . $qty . "'";

		if ($qty > 0) {
			$query .= ", `is_in_stock`='1'";
		} else if (!$onlySetAvailable) {
			$query .= ", `is_in_stock`='0'";
		}

		$query .= "WHERE `product_id` = '" . $id . "'";

		return $this->writeConnection->exec($query);
	}

	protected function updateStockMovement($stockItemId, $stockAfter)
	{
		// 'bubble_stock_movement'
		$isInStock = $stockAfter > 0 ? 1 : 0;
		$createdAt = (new DateTime())->format(self::DATE_FORMAT);
		$message = "Stock updated on equis sync";
		$query = "INSERT INTO `bubble_stock_movement` (`item_id`, `user`, `user_id`, `qty`, `is_in_stock`, `message`, `created_at`)
		VALUES (" . $stockItemId . ", 'admin', 1, " . $stockAfter . ", " . $isInStock . ", '" . $message  . "', '" . $createdAt . "')";
		return $this->writeConnection->exec($query);
	}

	protected function assignWebsite($productId, $websiteId)
	{
		if ($productId && $productId != "") {
			// Borra las asignaciones a otros website
			$deleteQuery = 'DELETE FROM `catalog_product_website` WHERE `product_id` = "' . $productId . '";';
			$this->writeConnection->exec($deleteQuery);
		}
		$query = "INSERT INTO `catalog_product_website` (`product_id`, `website_id`)
		VALUES (" . $productId . ", " . $websiteId . ")";
		return $this->writeConnection->exec($query);
	}

	/* ---------------- FUNCIONES DE LOG ---------------- */

	protected function createLog()
	{
		// Crea el archivo en la carpeta /var/log/equis/
		$folder = Mage::getBaseDir('var') . "/log/equis/";
		if (!is_dir($folder)) {
			mkdir($folder);
		}

		$path = $folder . $this->logFile;

		file_put_contents($path, "", LOCK_EX);
		return;
	}

	protected function logInFile($msj)
	{
		$folder = Mage::getBaseDir('var') . "/log/equis/";
		if (!is_dir($folder)) {
			// No existe la carpeta
			mkdir($folder);
		}
		$path = $folder . $this->logFile;
		// Escribir los contenidos en el fichero,
		// usando la bandera FILE_APPEND para añadir el contenido al final del fichero
		// y la bandera LOCK_EX para evitar que cualquiera escriba en el fichero al mismo tiempo
		file_put_contents($path, $msj, FILE_APPEND | LOCK_EX);
	}

	/* ---------------- OTRAS FUNCIONES ---------------- */

	protected function formatText($text)
	{
		// Elimina etiquetas HTML, espacios al inicio o fin del string, y reemplaza caracteres \n y \r por espacio
		// return strip_tags(str_replace(array("\n", "\r"), " ", trim($text)));
		return str_replace(array("\n", "\r"), " ", trim($text));	// Deja las etiquetas HTML
	}

	protected function checkActiveIntegration()
	{
		$activeIntagration = Mage::getStoreConfigFlag('equisconfig/catalogimport/active');

		if (!$activeIntagration) { // Sincronización desactivada por config
			$msj = "LA SINCRONIZACIÓN NO ESTÁ ACTIVADA";
			echo $msj . "<br/>";
			$this->logInFile($msj . "\n");
			return false;
		}

		return true;
	}

	protected function loadAttributeIds()
	{
		// TypeID de producto
		$this->productEntityTypeId = $this->getEntityType('catalog_product');

		// AttributeID de los campos a actualizar
		$this->attributeIds = array(
			"price" =>	 			$this->getAttributeId('price', $this->productEntityTypeId),
			"equivalencia_equis" => $this->getAttributeId('equivalencia_equis', $this->productEntityTypeId),
			"marca" => 				$this->getAttributeId('marca', $this->productEntityTypeId),
			"material" => 			$this->getAttributeId('material', $this->productEntityTypeId),
			"color" => 				$this->getAttributeId('color', $this->productEntityTypeId),
			"linea" => 				$this->getAttributeId('linea', $this->productEntityTypeId),
			"manufacturer" => 		$this->getAttributeId('manufacturer', $this->productEntityTypeId),
			"genero" => 			$this->getAttributeId('genero', $this->productEntityTypeId),
			"codigo" => 			$this->getAttributeId('codigo', $this->productEntityTypeId),
			"talle" =>	 			$this->getAttributeId('talle', $this->productEntityTypeId),
			"name" =>	 			$this->getAttributeId('name', $this->productEntityTypeId),
			// "tarea" =>	 			$this->getAttributeId('tarea', $this->productEntityTypeId),
			// "proveedor" => 		$this->getAttributeId('proveedor', $this->productEntityTypeId),
		);
	}

	protected function loadAttributeTypes()
	{
		// AttributeTypes default de los campos a actualizar
		$this->defaultAttributeTypes = array(
			"price" =>	 			"decimal",
			"equivalencia_equis" => "varchar",
			"marca" => 				"varchar",
			"material" => 			"varchar",
			"color" => 				"varchar",
			"linea" => 				"varchar",
			"manufacturer" => 		"int",
			"genero" => 			"int",
			"codigo" => 			"text",
			"talle" =>	 			"varchar",
			"name" =>               "varchar",
			// "tarea" =>	 			"varchar",
			// "proveedor" => 		"varchar",
		);

		// AttributeTypes de los campos a actualizar
		$this->attributeTypes = array(
			"price" =>	 			$this->getAttributeType($this->attributeIds['price'], $this->productEntityTypeId),
			"equivalencia_equis" => $this->getAttributeType($this->attributeIds['equivalencia_equis'], $this->productEntityTypeId),
			"marca" => 				$this->getAttributeType($this->attributeIds['marca'], $this->productEntityTypeId),
			"material" => 			$this->getAttributeType($this->attributeIds['material'], $this->productEntityTypeId),
			"color" => 				$this->getAttributeType($this->attributeIds['color'], $this->productEntityTypeId),
			"linea" => 				$this->getAttributeType($this->attributeIds['linea'], $this->productEntityTypeId),
			"manufacturer" => 		$this->getAttributeType($this->attributeIds['manufacturer'], $this->productEntityTypeId),
			"genero" => 			$this->getAttributeType($this->attributeIds['genero'], $this->productEntityTypeId),
			"codigo" => 			$this->getAttributeType($this->attributeIds['codigo'], $this->productEntityTypeId),
			"talle" =>	 			$this->getAttributeType($this->attributeIds['talle'], $this->productEntityTypeId),
			"name" =>	 			$this->getAttributeType($this->attributeIds['name'], $this->productEntityTypeId),
			// "tarea" =>	 			$this->getAttributeType($this->attributeIds['tarea'], $this->productEntityTypeId),
			// "proveedor" => 		$this->getAttributeType($this->attributeIds['proveedor'], $this->productEntityTypeId),
		);
	}

	protected function manageCurl($curlResponse, $curlError)
	{
		if ($curlError) { // Error en la conexión
			$msj = "NO SE PUDO COMPLETAR LA SINCRONIZACIÓN POR UN ERROR EN LA CONEXIÓN (curl): ";
			echo $msj . "<br/>";
			echo "cURL Error #: " . $curlError . "<br/>";
			$this->logInFile($msj . "\n");
			$this->logInFile($curlError . "\n");
			return false;
		}

		$objResponse = json_decode($curlResponse, true)["Productos"];
		if (!$objResponse) { // No hay productos para sincronizar
			$msj = "NO HAY PRODUCTOS MODIFICADOS";
			echo $msj . "<br/>";
			$this->logInFile($msj . "\n");
			return false;
		}

		if ($objResponse["Error"] != "") { // Error en la consulta
			$msj = "LA CONSULTA SE EJECUTÓ CON UN ERROR";
			echo $msj . ": " . $objResponse["Error"] . "<br/>";
			$this->logInFile($msj . "\n");
			return false;
		}

		return $objResponse;
	}

	protected function startCurlConnection()
	{
		$curl = curl_init();

		$credentials = Mage::getStoreConfig('equisconfig/credentials');

		$option = Mage::getStoreConfig('equisconfig/catalogimport/parameter_option');
		$condition = Mage::getStoreConfig('equisconfig/catalogimport/parameter_condition');

		$params = array("Opcion" => $option, "Condicion" => $condition);
		if ($condition == "D") {
			$lastModified = Mage::getStoreConfig('equisconfig/catalogimport/parameter_lastModified');
			$params["ModificadoDesde"] = $lastModified;
		}

		$optionArray = array(
			CURLOPT_URL => $credentials["url_catalog"],
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 60,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "POST",
			CURLOPT_POSTFIELDS => $params,
			CURLOPT_HTTPHEADER => array(),
		);

		curl_setopt_array($curl, $optionArray);

		$curlResponse = curl_exec($curl);
		$curlError = curl_error($curl);

		curl_close($curl);

		return $this->manageCurl($curlResponse, $curlError);
	}

	protected function getProductIfExists($sku, $equivalenciaEquis)
	{
		// Buscar el producto por sku 
		
		$magentoProduct = array_search($sku, $this->skuList);
		if (!$magentoProduct) {
			// Buscar el producto por equivalencia_equis
			$magentoProduct =  array_search($equivalenciaEquis, $this->skuList);
			if ($magentoProduct) {
				return $this->getProduct($equivalenciaEquis);
			} else {
				$magentoProduct = array_search($sku, $this->equisList);
				if (!$magentoProduct) {
					$magentoProduct = in_array($equivalenciaEquis, $this->equisList);
					if ($magentoProduct) {
						return $this->getProductByCode($equivalenciaEquis);
					} else {
						return false;
					}
				} else {
					return $this->getProductByCode($sku);
				}
			}
		} else {
			return $this->getProduct($sku);
		}
	}

	protected function updateSingleProduct($productId, $productObj)
	{
		$simpleAttributesToUpdate = explode(",", $this->catalogSyncConfig["simple_attributes_to_update"]);
		$configurableAttributesToUpdate = explode(",", $this->catalogSyncConfig["configurable_attribute_to_update"]);

		$stock = $productObj['CantDisponibleStock'];

		$simpleMappedValues = $this->getMappedValues($productObj, "simple");

		if ($this->catalogSyncConfig["update_simple"]) {

			foreach ($simpleMappedValues["simple"] as $magentoAttribute => $equisValue) {
				$this->updateAttribute($magentoAttribute, $productId, $equisValue);
			}

			foreach ($simpleMappedValues["option"] as $magentoAttribute => $equisValue) {
				if (strpos($equisValue, "Talle ") !== false) {
					$arrayCurva = explode("Talle ", $equisValue);
					$realValue = trim($arrayCurva[1]);
				} else {
					$realValue = $equisValue;
				}
				if ($realValue) {

					$option = $this->getOptionId($realValue, $magentoAttribute);
					if ($option)
						$this->updateAttribute($magentoAttribute, $productId, $option);
				}
			}

			foreach ($simpleMappedValues["multiselect"] as $magentoAttribute => $equisValue) {
				$equisValueOptions = array();
				if (strpos($equisValue, "/") !== false) {
					$optionValues = explode("/", $equisValue);
					foreach ($optionValues as $optionName) {
						$equisValueOptionId = $this->getOptionId($optionName, $magentoAttribute);
						if ($equisValueOptionId)
							$equisValueOptions[] = $equisValueOptionId;
					}
				} else {
					$equisValueOptionId = $this->getOptionId($equisValue, $magentoAttribute);
					if ($equisValueOptionId)
						$equisValueOptions[] = $equisValueOptionId;
				}
				$this->updateAttribute($magentoAttribute, $productId, implode(",", $equisValueOptions));
			}

			if (in_array("stock", $simpleAttributesToUpdate)) {
				$stockBefore = $this->getStockAmount($productId);
				$this->updateStock($productId, $stock);
				if ($stockBefore != $stock) {
					// Actualiza stock movement
					$stockItemId = $this->getStockItemId($productId);
					$this->updateStockMovement($stockItemId, $stock);
				}
			}
			if ($this->catalogSyncConfig["update_website"] == "1") {
				$websiteId = $this->getWebsiteFromEquis($productObj);
				$this->assignWebsite($productId, $websiteId, true);
			}
		}

		if ($this->catalogSyncConfig["update_configurable"]) {
			$parentProductId = $this->getParentEntityId($productId);

			if ($parentProductId) {
				$configurableMappedValues = $this->getMappedValues($productObj, "configurable");
				foreach ($configurableMappedValues["simple"] as $magentoAttribute => $equisValue) {
					$this->updateAttribute($magentoAttribute, $parentProductId, $equisValue);
				}
				foreach ($configurableMappedValues["option"] as $magentoAttribute => $equisValue) {
					if (strpos($equisValue, "Talle ") !== false) {
						$arrayCurva = explode("Talle ", $equisValue);
						$realValue = trim($arrayCurva[1]);
					} else {
						$realValue = $equisValue;
					}
					if ($realValue) {

						$option = $this->getOptionId($realValue, $magentoAttribute);
						if ($option)
							$this->updateAttribute($magentoAttribute, $parentProductId, $option);
					}
				}
				foreach ($configurableMappedValues["multiselect"] as $magentoAttribute => $equisValue) {
					$equisValueOptions = array();
					if (strpos($equisValue, "/") !== false) {
						$optionValues = explode("/", $equisValue);
						foreach ($optionValues as $optionName) {
							$equisValueOptionId = $this->getOptionId($optionName, $magentoAttribute);
							if ($equisValueOptionId)
								$equisValueOptions[] = $equisValueOptionId;
						}
					} else {
						$equisValueOptionId = $this->getOptionId($equisValue, $magentoAttribute);
						if ($equisValueOptionId)
							$equisValueOptions[] = $equisValueOptionId;
					}
					$this->updateAttribute($magentoAttribute, $parentProductId, implode(",", $equisValueOptions));
				}
				if (in_array("stock", $configurableAttributesToUpdate))
					$this->updateStock($parentProductId, $stock, true);
			}
		}

		// Actualiza el log

		$msj = implode(SEP, $productObj);
		$msj .= SEP .$this->getWebsiteFromEquis($productObj);
		$this->logInFile($msj . "\n");
	}

	protected function createSimpleProduct($productObj)
	{
		$sku = $productObj['ProductoIdAnterior'];
		$sku = $sku == "" ? $productObj['ProductoId'] : $sku;
		$precio = $productObj['Precio'];
		$stock = $productObj['CantDisponibleStock'];
		$equivalenciaEquis = $productObj["ProductoId"];
		$familia = $productObj["Familia"] == "Calzados" ? "Calzado" : $productObj["Familia"];
		$attributeSetId = $this->getAttributeSetId($familia, $this->productEntityTypeId);
		$nombre = $productObj["Descripcion"];
		$curva = $productObj["Curva"];
		$color = $productObj["Color"];
		$website = $this->getWebsiteFromEquis($productObj);

		if (!$attributeSetId) throw new Exception("No existe el conjunto de atributos: " . $familia);
		$simple = Mage::getModel('catalog/product');
		$simple
			->setWebsiteIds(array($website))
			->setAttributeSetId($attributeSetId)
			->setTypeId(Mage_Catalog_Model_Product_Type::TYPE_SIMPLE)
			->setCreatedAt(strtotime('now'))
			->setName($nombre)
			->setDescription($nombre)
			->setShortDescription($nombre)
			->setSku($sku)
			->setWeight(0)
			->setStatus(Mage_Catalog_Model_Product_Status::STATUS_DISABLED)
			->setVolumen(0)
			->setPrice($precio)
			->setTaxClassId(0) // Tax class (0: none, 1: default, 2: taxable, 4: shipping)
			->setVisibility(Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH)
			->setMediaGallery(array('images' => array(), 'values' => array()));

		//Agregar info de stock
		$stockData = array(
			"manage_stock" => "1",
			"original_inventory_qty" => "0",
			"use_config_min_qty" => "1",
			"use_config_min_sale_qty" => "1",
			"use_config_max_sale_qty" => "1",
			"is_qty_decimal" => "0",
			"is_decimal_divided" => "0",
			"backorders" => "0",
			"notify_stock_qty" => "100",
			"enable_qty_increments" => "0",
			"qty_increments" => "0"
		);

		if ($stock > 0) {
			$stockData["qty"] = $stock;
			$stockData["is_in_stock"] = "1";
		} else {
			$stockData["qty"] = "0";
			$stockData["is_in_stock"] = "0";
		}
		$simple->setStockData($stockData);

		$simple->save();

		// Atributos adicionales
		// $this->updateAttribute("codigo", $simple->getId(), $productObj["ProductoId"]);
		$this->updateAttribute("linea", $simple->getId(), $productObj["SubLinea"]);
		$this->updateAttribute("marca", $simple->getId(), $productObj["Marca"]);
		$this->updateAttribute("material", $simple->getId(), $productObj["Material"]);
		$this->updateAttribute("equivalencia_equis", $simple->getId(), $equivalenciaEquis);

		// Atributos con opciones
		$manufacturerOptionId = $this->getOptionId($productObj["Marca"], "manufacturer");
		if ($manufacturerOptionId)
			$this->updateAttribute("manufacturer", $simple->getId(), $manufacturerOptionId);

		$generoOptionId = $this->getOptionId($productObj["Linea"], "genero");
		if ($generoOptionId)
			$this->updateAttribute("genero", $simple->getId(), $generoOptionId);

		// Talle
		if (strpos($curva, "Talle ") !== false) {
			$arrayCurva = explode("Talle ", $curva);
			$talleValue = trim($arrayCurva[1]);
		} else {
			$talleValue = $curva;
		}
		if ($talleValue) {
			$talleOptionId = $this->getOptionId($talleValue, "talle");
			if ($talleOptionId)
				$this->updateAttribute("talle", $simple->getId(), $talleOptionId);
		}

		// Color es un multiselect, puede tener más de un valor
		$colorOptions = array();
		if (strpos($color, "/") !== false) {
			$arrayColor = explode("/", $color);
			foreach ($arrayColor as $colorName) {
				$colorOptionId = $this->getOptionId($colorName, "color");
				if ($colorOptionId)
					$colorOptions[] = $colorOptionId;
			}
		} else {
			$colorOptionId = $this->getOptionId($color, "color");
			if ($colorOptionId)
				$colorOptions[] = $colorOptionId;
		}
		$this->updateAttribute("color", $simple->getId(), implode(",", $colorOptions));

		$msj = implode(SEP, $productObj);
		$msj .= SEP .$this->getWebsiteFromEquis($productObj);
		$this->logInFile($msj . "\n");
		return $simple->getId();
	}


	protected function getWebsiteFromEquis($productObj)
	{
		$marca = strtolower($productObj["Marca"]);

		if ($marca == "diadora" || $marca == "diadora lifestyle") {
			$website = $this->diadoraWebsiteId;
		} else {
			$website = $this->athixWebsiteId;
		}
		return $website;
	}

	protected function validateProductWebste($productId, $productObj)
	{
		$productWebsiteIds = $this->getProductWebsiteId($productId);
		$equisWebsite = $this->getWebsiteFromEquis($productObj);
		return $productWebsiteIds && in_array($equisWebsite, $productWebsiteIds);
	}

	/**
	 * Mapea los valores de atributos de equis en sus correspondientes atributos de magento, separados por tipo.
	 * Solo devuelve los atributos que se tienen que actualizar para el tipo de producto, o todo el mapeo, si $productType es nulo
	 * @param Array $productObj
	 * @param String|null $productType
	 * @return Array
	 */
	protected function getMappedValues($productObj, $productType)
	{
		$mappedValues = array(
			"simple" => [],
			"option" => [],
			"multiselect" => []
		);

		foreach ($this->attributeMapping as $equisAttribute => $magentoAttributeData) {
			if ($productType == "simple") {
				if ($magentoAttributeData["update_simple"])
					$mappedValues[$magentoAttributeData["type"]][$magentoAttributeData["magentoAttribute"]] = $productObj[$equisAttribute];
			} else if ($productType == "configurable") {
				if ($magentoAttributeData["update_configurable"])
					$mappedValues[$magentoAttributeData["type"]][$magentoAttributeData["magentoAttribute"]] = $productObj[$equisAttribute];
			} else {
				$mappedValues[$magentoAttributeData["type"]][$magentoAttributeData["magentoAttribute"]] = $productObj[$equisAttribute];
			}
		}
		return $mappedValues;
	}

	/**
	 * Actualiza $this->attributeMapping para indicar si cada atributo se tiene que crear/actualizar
	 */
	protected function prepareAttributes()
	{
		$simpleAttributesToUpdate = explode(",", $this->catalogSyncConfig["simple_attributes_to_update"]);
		$configurableAttributesToUpdate = explode(",", $this->catalogSyncConfig["configurable_attribute_to_update"]);
		foreach ($this->attributeMapping as $equisAttribute => $attributeData) {
			$equisAttributeLower = ($attributeData["magentoAttribute"]);
			
			$this->attributeMapping[$equisAttribute]["update_simple"] = in_array($equisAttributeLower, $simpleAttributesToUpdate);
			$this->attributeMapping[$equisAttribute]["update_configurable"] = in_array($equisAttributeLower, $configurableAttributesToUpdate);
		}
	}

	protected function updateProducts()
	{
		$this->updatedProductCount = 0;
		$this->prepareAttributes();

		foreach ($this->products as $productObj) {
			$sku = $productObj['ProductoIdAnterior'];
			if ($sku == "") {
				if ($productObj['ProductoId'] != "" && $productObj['ProductoId'] != null) {
					$sku = $productObj['ProductoId'];
				} else {
					continue;
				}
			}
			$magentoProduct = $this->getProductIfExists($sku, $productObj['ProductoId']);
		
			if ($magentoProduct) {	
				if (!$this->forceCreate) {
					if ($magentoProduct["type_id"] != "simple") {
						// Solo actualiza productos simples
						$this->notSimpleProducts[] = $sku;
						$msj = "El producto es configurable: " . $sku;
						$this->logInFile($msj . "\n");
						continue;
					}
					try {
						// Actualiza el producto
						$productId = $magentoProduct['entity_id'];

						if ($this->validateProductWebste($productId, $productObj) || $this->catalogSyncConfig["update_website"] != "0") {
							$this->updateSingleProduct($productId, $productObj);
							$this->updatedProductCount++;
						} else {
							// Si la marca no coincide con el website del producto y la config está seteada para no actualizarlo, no se actualiza
							$this->wrongWebsiteProducts[] = $sku;
							$msj = "El producto no pertenece al website de la marca: " . $sku;
							$this->logInFile($msj . "\n");
							continue;
						}
					} catch (Exception $e) {
						$this->notUpdatedProducts[] = $sku;
						$msj = "Error actualizando el producto con SKU: " . $sku;
						$this->logInFile($msj . "\n");
						$this->logInFile($e->getMessage() . "\n");
						continue;
					}
				} else {
					// Si la creación forzada está activada, no actualiza productos
					$this->notUpdatedProducts[] = $sku;
					$msj = "Śólo se permite creación de productos: " . $sku;
					$this->logInFile($msj . "\n");
					continue;
				}
			} else {
				//Crear producto
				if (!$this->forceCreate && !Mage::getStoreConfigFlag("equisconfig/catalogimport/create_products")) {
					// Si no está activada la creación forzada, y la creación está deshabilitada por config
					continue;
				}
				try {
					$productId = $this->createSimpleProduct($productObj);
					$this->createdProducts[] = $sku;
				} catch (Exception $e) {
					$this->notCreatedProducts[] = $sku;
					$msj = "Error creando el producto con SKU: " . $sku;
					$this->logInFile($msj . "\n");
					$this->logInFile($e->getMessage() . "\n");
					continue;
				}
			}
		}
	}

	public function reindex()
	{
		$indexCodes = array(
			"catalog_product_attribute",
			"catalog_product_price",
			"catalog_product_flat",
			"catalog_category_flat",
			"catalog_category_product",
			"catalogsearch_fulltext",
			"cataloginventory_stock"
		);
		foreach ($indexCodes as $index) {
			$this->logInFile('Reindexando  ' . $index . "\n");
			try {
				$process = Mage::getModel('index/indexer')->getProcessByCode($index);
				$process->reindexAll();
			} catch (Exception $e) {
				echo 'Hubo un error reindexando el índice: ' . $index . '<br/>';
				$this->logInFile($e->getMessage() . "\n");
			}
		}
	}

	protected function end($success = false)
	{

		if ($success) {
			$this->reindex();
			$endDateTime = Mage::getModel('core/date')->date(self::DATE_FORMAT);
			Mage::getModel('core/config')->saveConfig('equisconfig/catalogimport/parameter_lastModified', $endDateTime);
		}

		// Pie del log
		$endDate = Mage::getModel('core/date')->date(self::DATE_FORMAT);
		$msj = "SINCRONIZACIÓN TERMINADA: " . $endDate;
		$msj2 = "(" .
			count($this->products) . " productos recibidos, " .
			$this->updatedProductCount . " actualizados, " .
			count($this->createdProducts) . " creados, " .
			count($this->notUpdatedProducts) . " simples no actualizados, " .
			count($this->wrongWebsiteProducts) . " productos salteados por ser de distinto website, " .
			count($this->notSimpleProducts) . " configurables no actualizados, " .
			count($this->notCreatedProducts) . " no creados)";
		echo $msj . "<br/>";
		echo $msj2 . "<br/>";
		$this->logInFile($msj . "\n");
		$this->logInFile($msj2 . "\n");

		if (count($this->createdProducts)) {
			$msj = "PRODUCTOS CREADOS: \n";
			foreach ($this->createdProducts as $sku) {
				$msj .= $sku . SEP . " ";
			}
			$this->logInFile($msj . "\n");
		}

		if (count($this->notCreatedProducts)) {
			$msj = "PRODUCTOS NO CREADOS: \n";
			foreach ($this->notCreatedProducts as $sku) {
				$msj .= $sku . SEP . " ";
			}
			$this->logInFile($msj . "\n");
		}

		if (count($this->notUpdatedProducts)) {
			$msj = "PRODUCTOS NO ACTUALIZADOS: \n";
			foreach ($this->notUpdatedProducts as $sku) {
				$msj .= $sku . SEP . " ";
			}
			$this->logInFile($msj . "\n");
		}

		$msj = "<br/>" . "Detalles de la integración en: log/equis/";
		$msj .= $this->logFile;
		echo $msj . "<br/>";

		return;
	}

	public function main()
	{
		$this->createLog();

		if (!$this->checkActiveIntegration())
			return $this->end();

		$this->products = $this->startCurlConnection();
		if (!$this->products)
			return $this->end();

		// Cabecera del log
		$startDate = Mage::getModel('core/date')->date(self::DATE_FORMAT);
		$logHeader = "SINCRONIZACIÓN INICIADA: " . $startDate;
		$msj = "Se van a actualizar " . count($this->products) . " productos";
		$this->logInFile($logHeader . "\n");
		$this->logInFile($msj . "\n");

		// Loguea detalles de atributos a actualizar por decicion de las config

		if ($this->catalogSyncConfig["active"]) {
			if ($this->catalogSyncConfig["update_simple"]) {
				$msj = "esta habilitada la sincronizacion de productos simples";
				$this->logInFile($msj . "\n");
				$msj1 = "atributos simples a actualizar:";
				$this->logInFile($msj1 . "\n");
				$msj2 = $this->catalogSyncConfig["simple_attributes_to_update"];
				$this->logInFile($msj2 . "\n");
				$msj = "";
				if ($this->catalogSyncConfig["update_website"] == "0")
					$msj .= "no actualiza productos con website distinto a equis, ";
				if ($this->catalogSyncConfig["update_website"] == "1")
					$msj .= "actualiza website de productos, ";
				if ($this->catalogSyncConfig["update_website"] == "2")
					$msj .= "ignora website, ";
				$this->logInFile($msj . "\n");
			} else {
				$msj = "no esta habilitada la sincronizacion de productos simples";
				$this->logInFile($msj . "\n");
			}
			if ($this->catalogSyncConfig["update_configurable"]) {
				$msj = "esta habilitada la sincronizacion de productos configurables";
				$this->logInFile($msj . "\n");
				$msj1 = "atributos configurables a actualizar:";
				$this->logInFile($msj1 . "\n");
				$msj2 = $this->catalogSyncConfig["configurable_attribute_to_update"];
				$this->logInFile($msj2 . "\n");
			} else {
				$msj = "no esta habilitada la sincronizacion de productos configurables";
				$this->logInFile($msj . "\n");
			}
		}
		if ($this->catalogSyncConfig["create_products"]) {
			$msj = "esta habilitada la creacion de productos";
			$this->logInFile($msj . "\n");
		} else {
			$msj = "no esta habilitada la creacion de productos";
			$this->logInFile($msj . "\n");
		}

		if ($this->forceCreate) {
			$msj = "CREACIÓN FORZADA DE PRODUCTOS";
			echo $msj . "<br/>";
			$this->logInFile($msj . "\n");
		}

		// Cabecera para mostrar datos de productos actualizados
		$msj = implode(SEP . " ", array_keys($this->attributeMapping));
		$msj .= SEP . " WesbiteId";
		$this->logInFile($msj);
		$this->logInFile("\n");

		$this->loadAttributeIds();
		$this->loadAttributeTypes();
		$this->loadSkuList();
		$this->loadEquisProduct();

		$this->updateProducts();

		$this->end(true);
	}
}
