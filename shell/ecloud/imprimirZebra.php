<?php

    require_once('../../app/Mage.php'); //Path to Magento
    //ini_set('max_execution_time', 6000);
    umask(0);
    Mage::app();

    //Validar que haya un usuario logueado en adminhtml
    Mage::getSingleton('core/session', array('name'=>'adminhtml'));
    // if(!Mage::getSingleton('admin/session')->isLoggedIn()){
    //     //Usuario no esta logueado en admin
    //     die("El usuario debe estar logueado");
    // }
    $idOrden = Mage::app()->getRequest()->getParams('idorden');
    $shipmentId = Mage::app()->getRequest()->getParams('shipment');

    if($idOrden['idorden'] == "") {
        //No se encuentra la orden
        die("Se debe especificar un id de pedido");
    }
    if($shipmentId['shipment'] == "") {
        //No se encuentra la orden
        die("Se debe especificar un id de envío");
    }
    $ordenAndreani = Mage::getModel('andreani/order')->load($idOrden['idorden']);
    $order = Mage::getModel("sales/order")->load($ordenAndreani->getIdOrden());
    $store = explode("\n", $order->getStoreName())[0];
    $shipment = Mage::getModel('sales/order_shipment')->load($shipmentId["shipment"]);
    $andreaniDatosGuia  = $shipment->getAndreaniDatosGuia();
    if($ordenAndreani->getAgrupadorBultos() && $ordenAndreani->getAgrupadorBultos() != '') {
        $andreaniDatosGuia  = json_decode(unserialize($andreaniDatosGuia))->datosguia;
        $sucursalDeDistribucion = $andreaniDatosGuia->sucursalDeDistribucion->id . " " . $andreaniDatosGuia->sucursalDeDistribucion->descripcion;
    }
    else{
        $andreaniDatosGuia  = json_decode(unserialize($andreaniDatosGuia))->datosguia->GenerarEnviosDeEntregaYRetiroConDatosDeImpresionResult;
        $sucursalDeDistribucion = $andreaniDatosGuia->SucursalDeDistribucion;
    }
    $codigoBarras       = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA).'/uploads/default/'.$ordenAndreani->getCodTracking().'.png';
    //Validar que exista la orden
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Etiqueta</title>
<meta name="language" content="es">
<link rel="stylesheet" type="text/css" href="../../skin/frontend/base/default/css/andreani/styles.min.css" />
<link rel="stylesheet" type="text/css" href="../../skin/frontend/base/default/css/andreani/style.css" />
<script src="<?php echo Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_JS); ?>barcode/JsBarcode.all.min.js"></script>
</head>
<body>
    <div class="subpage-zebra">
    <div class="constancias-sobres">
        <header class="banner-zebra">
            <div>
                <div class="logo">
                    <img src="../../skin/frontend/base/default/images/andreani/logo.png">
                </div>
                <div class="title-right">        
                    <?php switch($ordenAndreani->getContrato()): 
                              case Mage::getStoreConfig('carriers/andreaniestandar/contrato',Mage::app()->getStore()): ?>
                            <h2>DOMICILIO<br>ESTANDAR</h2>
                        <?php break; ?>
                        <?php case Mage::getStoreConfig('carriers/andreanisucursal/contrato',Mage::app()->getStore()): ?>
                        <h2>ENTREGA<br>SUCURSAL</h2>
                        <?php break; ?>
                        <?php case Mage::getStoreConfig('carriers/andreaniurgente/contrato',Mage::app()->getStore()): ?>
                          <h2>ENTREGA<br>URGENTE</h2>
                        <?php break; ?>
                    <?php endswitch; ?>
                </div>
            </div>
        </header>
        <table border="0" cellpadding="0" cellspacing="0" class="tableZebra" width="100%">
            <thead>
                <th width="45%"></th>
                <th></th>
                <th></th>
            </thead>
            <tbody>
                <tr>
                    <td valign="top" class="tdPadding" colspan="3">
                        <h4 class="nombre black"><?php echo $ordenAndreani->getNombre()." ".$ordenAndreani->getApellido(); ?></h4>
                        <h3 class="direccion black direccion-full-width"><?php echo $ordenAndreani->getDireccion(); ?></h3>
                        <p><?php echo $ordenAndreani->getLocalidad(); ?> / <?php echo $ordenAndreani->getCpDestino(); ?> / <?php echo $ordenAndreani->getProvincia(); ?></p>
                    </td>
                </tr>
                <tr>
                    <td valign="top" class="tdPadding tdBorderBottom">
                        <p class="black"><strong>ID: <?php echo $ordenAndreani->getOrderIncrementId(); ?></strong></p>
                        <p>Peso: <?php echo $ordenAndreani->getPeso(); ?>gr
                        <p>Ancho: -</p>
                        <p>Alto:  -</p>
                        <p>Largo:  -</p>
                    </td>
                    <td valign="top" class="tdPadding tdBorderBottom">
                        <p class="black"><strong>REF: </strong><?php echo $ordenAndreani->getDetalleProductos(); ?></p>
                        <p>N° de BULTO</p>
                        <p class="bulto"><?php echo $ordenAndreani->getTotalizador() ? $ordenAndreani->getTotalizador() : "1/1"; ?></p>
                    </td>
                </tr>
                <tr>
                    <td valign="top" class="tdPadding tdBorderBottom condicionLeft remitente">
                        <p>REMITENTE:</p>
                        <p class="nombreCliente black"><?php echo ($store == "" ? "ATHIX" : strtoupper($store)); ?></p>
                        <?php 
                        $address = "M.ROSS 925 ROSARIO SUD" ;
                        $addressArr = explode(",", $address);
                        $street = $addressArr[0];
                        $postCode = $addressArr[1];
                        $locality = $addressArr[2];
                        ?>
                        <p><?php echo $street ?></p>
                        <p><?php echo $locality . " " . $postCode; ?></p>
                    </td>
                    <td class="tdBorderBottom" colspan="2">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                            <thead>
                                <th width="80%"></th>
                                <th></th>
                            </thead>
                            <tbody>
                                <tr>
                                    <td valign="top" class="tdPadding infoContrato">
                                        <p>Correo Andreani RNPSP Nº 586</p>
                                        <!--<p class="">Condición de entrega</p>-->
                                        <p>CL: <?php echo $ordenAndreani->getCliente(); ?> / Cont: <?php echo $ordenAndreani->getContrato(); ?></p>
                                        <p>Sucursal de Rendición:Rosario</p>
                                    </td>
<!--                                     <td valign="top" class="tdPadding tipoCondicion">
                                        <?php if($ordenAndreani->getContrato() == "400004880"): ?>
                                            <p>B</p>
                                        <?php else: ?>
                                            <p>D</p>
                                        <?php endif;?>
                                    </td> -->
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td valign="top" class=" tdPadding condicionLeft tdBorderBottom " colspan="3" >
                       <div class="numero-sucursal-retiro">
                        <?php if ($ordenAndreani->getSucursalRetiro() == 0  || $ordenAndreani->getSucursalRetiro() == ''): ?>
                            <?php $sucursalDist = explode('-', $ordenAndreani->getSucursalDistribucion()); ?>
                            <?php if(count($sucursalDist) > 1): ?>
                                <p class="sucursar_retiro"><?php echo $sucursalDist[0]; ?></p>
                            <?php endif; ?>
                        <?php else: ?>
                            <p class="sucursar_retiro"><?php echo $ordenAndreani->getSucursalRetiro(); ?></p>
                        <?php endif ?>
                        </div>
                        <div class="nombre-sucursal-retiro">
                            <p class="title">Sucursal de Distribución</p>
                            <p class="label-sucursal-retiro" style="white-space: normal;">
                                <?php echo $sucursalDeDistribucion ?>   
                            </p>
                        </div>                        
                    </td>
                </tr>
                <tr>
                    <td class="codigoZebra tdBorderBottom " colspan="3">
                        <svg id="barcode"></svg>
                        <p class="codigo-barras-p"><?php echo $ordenAndreani->getCodTracking(); ?></p>
                        <script type="text/javascript">
                            JsBarcode("#barcode", "<?php echo $ordenAndreani->getCodTracking(); ?>", {
                                width: 1,
                                height: 50,
                                displayValue: false,
                                margin: 0
                            });
                        </script>
                        <style type="text/css">
                            svg#barcode {width: 98%;}
                        </style>
                    </td>
                   
                </tr>
            </tbody>
        </table>
    </div>
</div>

</body>
</html>