
jQuery(document).ready(function(){
	/* Seteo mascara de 4 digitos para el input de postcode */
	jQuery('#billing\\:postcode').mask('0000');
	jQuery('#billing\\:dni').mask('00000000');
	jQuery('#billing\\:vat_id').mask('00000000000');
	jQuery('#billing\\:telephone').mask('00000000000000');
	
	jQuery('#shipping\\:postcode').mask('0000');
	jQuery('#shipping\\:dni').mask('00000000');
	jQuery('#shipping\\:telephone').mask('0000000000000');
});