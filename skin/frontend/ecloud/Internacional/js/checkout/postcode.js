
jQuery(document).ready(function(){
  
    /* Seteo mascara de 4 digitos para el input de postcode */
    jQuery('#billing\\:postcode').mask('0000');
    jQuery('#shipping\\:postcode').mask('0000');

    jQuery('#billing\\:telephone').mask('00000000000000');
    jQuery('#shipping\\:telephone').mask('0000000000000');
  
    /* Reseteo el valor del codigo postal si cambian de provincia */
    jQuery("#billing\\:region_id").change(function(){
        jQuery("#billing\\:postcode").val("");    
        /*jQuery( "#billing\\:region_id").val("");
        var city_label = jQuery("#billing\\:region_id option:first").text();
        jQuery("#uniform-billing\\:region_id span").html(city_label);    */
        jQuery("#billing\\:postcode").focus();    
    });
    
    jQuery("#shipping\\:region_id").change(function(){
      jQuery("#shipping\\:postcode").val("");    
      /*jQuery( "#shipping\\:region_id").val("");
      var city_label = jQuery("#shipping\\:region_id option:first").text();
      jQuery("#uniform-shipping\\:region_id span").html(city_label);       */
      jQuery("#shipping\\:postcode").focus();
    });
    
    // /* After 4 inputs characters */
    // jQuery('#shipping\\:postcode').live('input', function(e){
    //     var postcode_chars = jQuery(this).val().length;
    //     if(postcode_chars == 4){
    //         getShippingCityByPostalCode(jQuery(this).val());
    //     }
    // });
  
    // /* Postcode on blur */
    // jQuery("#shipping\\:postcode").blur(function(){
    //     getShippingCityByPostalCode(jQuery(this).val());
    // });
    
    // /* After 4 inputs characters */
    // jQuery('#billing\\:postcode').live('input', function(e){
    //     var postcode_chars = jQuery(this).val().length;
    //     if(postcode_chars == 4){
    //         getBillingCityByPostalCode(jQuery(this).val());
    //     }
    // });
  
    // /* Postcode on blur */
    // jQuery("#billing\\:postcode").blur(function() {
    //       getBillingCityByPostalCode(jQuery(this).val());
    // });
    
});

function getBillingCityByPostalCode(postcode){
  
    jQuery.ajax({
        url: "/brandlivepostcode/ajax/getCityByPostCode",
        type: "POST",
        data: {postcode: postcode},
        dataType: "json",
        success: function(data){
            if(data.result == 'success'){
              var region_id = data.region_id;
              // Setteo la ciudad en base al region_id que me devuelve
              jQuery( "#billing\\:region_id").val(region_id);
              
              // Obtengo el nombre de la provincia
              var city_label = jQuery("#billing\\:region_id option:selected").text();
              
              // Setteo el nombre de la provincia 
              jQuery("#uniform-billing\\:region_id span").html(city_label);
            }else{
              jQuery( "#billing\\:region_id").val("");
              var city_label = jQuery("#billing\\:region_id option:first").text();
              jQuery("#uniform-billing\\:region_id span").html(city_label);
            }
        },
        error: function(error)
        {
          console.log("Error: " + JSON.parse(JSON.stringify(error)));
        }
    });
  
}

function getShippingCityByPostalCode(postcode){
    
    // TODO pasar a una funcion asi no llamamos dos veces a la misma logica 
    /*
    var shipping_postcode_selector                  = jQuery("#shipping\\:postcode");
    var shipping_region_selector                    = jQuery("#shipping\\:region_id");
    var shipping_region_selector_selected_option    = jQuery("#shipping\\:region_id option:selected");
    var shipping_region_selector_first_option       = jQuery("#shipping\\:region_id option:first");
    var shipping_uniform_selector                   = jQuery("#uniform-shipping\\:region_id span");
    
    updateRegionByPostCode(shipping_postcode_selector, shipping_region_selector, shipping_region_selector_selected_option, shipping_region_selector_first_option, shipping_uniform_selector);
    */
    
    jQuery.ajax({
        url: "/brandlivepostcode/ajax/getCityByPostCode",
        type: "POST",
        data: {postcode: postcode},
        dataType: "json",
        success: function(data){
            if(data.result == 'success'){
              var region_id = data.region_id;
              
              // Setteo la ciudad en base al region_id que me devuelve
              jQuery( "#shipping\\:region_id").val(region_id);
              
              // Obtengo el nombre de la provincia
              var city_label = jQuery("#shipping\\:region_id option:selected").text();
              
              // Setteo el nombre de la provincia 
              jQuery("#uniform-shipping\\:region_id span").html(city_label);
            }else{
              jQuery( "#shipping\\:region_id").val("");
              var city_label = jQuery("#shipping\\:region_id option:first").text();
              jQuery("#uniform-shipping\\:region_id span").html(city_label);
            }
        },
        error: function(error)
        {
          console.log("Error: " + JSON.parse(JSON.stringify(error)));
        }
    });
}