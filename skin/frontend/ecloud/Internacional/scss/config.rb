# note: this should never truly be refernced since we are using relative assets
add_import_path "../../../ecloud/default/scss"
http_path = "/skin/frontend/ecloud/Internacional/"
css_dir = "../css"
sass_dir = "../scss"
images_dir = "../images"
javascripts_dir = "../js"
relative_assets = true

output_style = :expanded
environment = :production
sourcemap = true