/*!
 * Theme Athix
 * Ecloudsolutions.
 */
jQuery(window).load(function(){
 	Theme.homeSlider();
});

jQuery(document).ready(function(){
 	Theme.init();
 	CatalogView.init();
 	ProductPage.init();
 	CHECKOUT.init();
});
var CHECKOUT = {
	init:function(){
		this.cuponReload();
	},
	cuponReload:function(){
		if (jQuery('body').hasClass('opc-index-index')) {
			jQuery('.opc-messages-action button').on("click", function() {
               IWD.OPC.Shipping.saveShippingMethod();
            });
		}
	}
}

var ProductPage = {
	init:function(){
		if (jQuery('body').hasClass('.catalog-product-view')) {
			this.onMobile();
			this.onDesktop();
			this.moverDesc();
		};
	},
	onMobile: function(){
		ProductPage.thumbsCarousel();
		jQuery(document).on('ajaxStop',function(){
			ProductPage.thumbsCarousel();
		});
		jQuery(window).resize(function(){
			ProductPage.thumbsCarousel();
		});
	},
	onDesktop: function(){
		ProductPage.onImageContainerResize();
		jQuery(document).on('ajaxStop',function(){
			ProductPage.onImageContainerResize();
		});
		jQuery(window).load(function(){
			ProductPage.onImageContainerResize();
		});
		jQuery(window).resize(function(){
			ProductPage.onImageContainerResize();
		});
	},
	thumbsCarousel:function(){
		jQuery('.product-image-thumbs.carousel').remove();
		if(jQuery(window).width() < 771){
			jQuery('.thumb-link').show(); 
			jQuery(".product-image-thumbs .thumb-link").css('max-width','');
			var quickview = jQuery(".ajaxview").length;
			var owlLoaded = jQuery(".product-image-thumbs").hasClass(".owl-loaded");
			var thumbsqty = jQuery(".product-image-thumbs .thumb-link").length;
			if (!owlLoaded){
				if (quickview < 1) {
					if( thumbsqty > 1 ){
						jQuery('.more-views .product-image-thumbs').hide();
						if(jQuery('.more-views .product-image-thumbs.carousel').length == 0){
							jQuery('.more-views .product-image-thumbs').clone().addClass('carousel').appendTo('.more-views');
							jQuery('.more-views .product-image-thumbs.carousel').show();
							jQuery('.more-views .product-image-thumbs.carousel').owlCarousel({
								items: 5,
								dots: false,
								nav:true,
								loop:true,
								autoWidth:false,
								slideBy:1,
								responsive:{
									0:{
										items:1
									}
								}
							})
						}else{
							jQuery('.more-views .product-image-thumbs.carousel').show();
						}
					}else{
						jQuery('.more-views .product-image-thumbs').show();
					}
				}
			}
		}else{
			//jQuery(owlLoaded).destroy();
			jQuery('.more-views .product-image-thumbs').show();
		}
	},
	onImageContainerResize:function(){
		if (jQuery('.product-image-container').length) {
			if(jQuery(window).width() > 770){
				mainImgHeight = jQuery('.product-image-zoom').outerHeight() - 10;
		 		jQuery('.more-views').css('max-height',mainImgHeight);
		 		if(jQuery(".product-image-thumbs .thumb-link").length > 4){
		 			jQuery('.more-views').css('overflow','scroll');
		 			jQuery('.more-views').css('overflow-x','hidden');
		 		}
		 		ProductPage.thumbnailsHeightAndClick();
		 	}else{
		 		jQuery('.more-views').css('max-height','');
		 		jQuery('.more-views').css('overflow','');
		 	}

		}
	},
	thumbnailsHeightAndClick: function(){
		thumbsCount = jQuery(".product-image-thumbs .thumb-link").length;
		if( thumbsCount != 1){
		    jQuery(".product-image-thumbs .thumb-link").first().hide();
		    if( thumbsCount >= 2){
		    	jQuery(".product-image-thumbs .thumb-link").css('max-width','');
		    	var zoomImgHeight = jQuery('.product-image-zoom').outerHeight() - 15;
		    	jQuery(".product-image-thumbs .thumb-link").css('max-width',(zoomImgHeight/2));
		    	if (thumbsCount == 4) {
		    		jQuery(".product-image-thumbs .thumb-link").css('max-width',(zoomImgHeight/3));
		    	}
		    	if(thumbsCount > 4){
		    		jQuery(".product-image-thumbs .thumb-link").height('auto');
		    		jQuery(".product-image-thumbs .thumb-link").css('max-width','');
		 			jQuery('.more-views').css('overflow','scroll');
		 			jQuery('.more-views').css('overflow-x','hidden');
		 		}
		    }
		    function _onThumbnailClick(){
		    	if(jQuery(window).width() > 770){
			    	jQuery('.thumb-link').click(function(){
			    		if(jQuery(".product-image-thumbs .thumb-link").length != 1){
			    			jQuery('.thumb-link').show(); 
			    			jQuery(this).hide();
			    		}
			    	})
		    	}
		    }
		    _onThumbnailClick();
		}
	},
	moverDesc: function (){
		desc = jQuery(".prod_attributes .short-description").length;
		if( desc != 0){
			jQuery(".prod_attributes .short-description").insertAfter('.price-info');
		}
	}
}

var CatalogView = {
	init: function(){
		if (jQuery('.catalog-category-view').length || jQuery('.catalogsearch-result-index').length) {
			this.toggleSwatches();
			this.onAjaxLayeredFilter();
		}
	},
	onAjaxLayeredFilter: function(){
		jQuery('.col-main').bind('DOMSubtreeModified', function() {
			// jQuery(document).trigger('ajaxFilteredContent');
			setTimeout(function(){
				jQuery('.ajax_load').fadeOut();
			},300);
		});
	},
	onInfiniteScrollReady: function(){
		jQuery(document).on('ajaxStop',function(){
			jQuery(document).on('infiniteScrollReady', function(items){
				console.log('infiniteScrollReady document');
				ConfigurableMediaImages.ajaxLoadSwatchList();
			});
		});
	},
	toggleSwatches: function(prodgrid){
		jQuery(document).on('ajaxStop',function(){
		    jQuery('.products-grid li').not('.toggled').each(function(){
		    	if (jQuery(this).find('.configurable-swatch-list').length > 0) {
			    	jQuery(this).find('.swatches-toggle').click(function(){
				        jQuery(this).toggleClass('opened');
				        jQuery(this).siblings('.configurable-swatch-list').slideToggle();
				    });
				    jQuery(this).addClass('toggled');
		    	}
		    });
	    });
	},
	InifiniteScrollRendered: function(){
		jQuery(document).on('ajaxStop',function(){
			//Integracion con infiniteScroll.
			window.ias.on('rendered', function(items){
				ConfigurableMediaImages.ajaxLoadSwatchList();
				// Theme.toggleSwatchesOnProdList();
			});
		});
	}
}

var Theme ={
	init:function(){
	 	this.navMenuBanners();
	 	this.navFeaturedBtns();
	 	this.toggleShopbyAdvanced();
	 	this.stickyHeader();
	 	this.owlCarousel();
	 	this.tabsCarousel();
	 	this.tabContact();
	 	this.toggleSortResponsive();
		this.activateSwipes();
		this.changeDOM();
		this.hideAjaxLoader();
		this.closeSkipLinksOnDocumentClick();
		this.headerBanner();
	},
	headerBanner: function(){
		if (jQuery('.banner-top-mobile').length ) {
	      jQuery('#header-nav').add('#header-search').add('#header-cart').add('#header-account').addClass('top-banner');        
	  	}
	},
	closeSkipLinksOnDocumentClick: function(){
		jQuery('.skip-content').click(function(e){
			e.stopPropagation();
		});
		jQuery('body').click(function(e){
			jQuery('.skip-link.skip-active').removeClass('skip-active');
			jQuery('.skip-content.skip-active').removeClass('skip-active');
		});
	},
	hideAjaxLoader: function(){
		jQuery(document).on('ajaxStop',function(){
			setTimeout(function(){
				jQuery('.ajax_load').fadeOut();
			},300)
		});
	},
	owlCarousel:function(){
		jQuery('.owl-carousel').owlCarousel({
			items: 5,
			dots: true,
			nav:true,
			loop:true,
			margin:10,
			autoWidth:false,
			slideBy:1,
			responsive:{
				0:{
					items:1
				},
				600:{
					items: 2
				},
				1000:{
					items:5
				}
			}
		})
	},
	tabsCarousel: function(){
		jQuery('#content-carousel ul').owlCarousel({
			items: 4,
			dots: false,
			nav:true,
			loop:true,
			autoWidth:false,
			responsiveClass:true,
		    responsive:{
		        0:{
		            items:1,
		            slideBy:2
		        },
		        600:{
		            items:2,
		           	slideBy:2
		        },
		        1000:{
		            items:4
		        }
		    }
		});
	},
	tabContact:function(){
	    jQuery("div.contact-tab-menu>div.list-group>a").click(function(e) {
	        e.preventDefault();
	        jQuery(this).siblings('a.active').removeClass("active");
	        jQuery(this).addClass("active");
	        var index = jQuery(this).index();
	        jQuery("div.contact-tab>div.contact-tab-content").removeClass("active");
	        jQuery("div.contact-tab>div.contact-tab-content").eq(index).addClass("active");
	    });
	},
	homeSlider: function(){
		if (jQuery('body').hasClass('cms-home')) {
			var length = jQuery("#home-slider .rslides").find('li').length;
			if (length > 1) {
				jQuery('#home-slider').insertAfter('#header');
				jQuery("#home-slider ul.rslides").owlCarousel({
					autoplay: true,             // Boolean: Animate automatically, true or false
					items: 1,
					singleItem: true,
					loop: true,
					//dots: false
				});
			}
		}
	},
	navFeaturedBtns: function(){
		if(jQuery('.nav-primary li.level0').length > 0){
			jQuery('.nav-primary li.level0').each(function(){
				if(jQuery(this).find('a').html() == 'Flexy'){
					jQuery(this).addClass('flexy');
				}
				if(jQuery(this).find('a').html() == 'Super Sale'){
					jQuery(this).addClass('super_sale');
				}
				if(jQuery(this).find('a').html() == 'Super Sale'){
					jQuery(this).addClass('super_sale');
				}
			});
		}
	},
	navMenuBanners: function(){
		function _navElementOffset(){
			var navli = jQuery('.nav-primary li');
			jQuery(navli).mouseenter(function(){
				var hasBanner = jQuery(this).find('.submenu-banner').children().length;
				if (hasBanner > 0){
					var ul = jQuery(this).find('ul.level0');
					var toright_space = (jQuery(window).width() - (jQuery(this).offset().left + jQuery(this).outerWidth()));
					var toleft_space  = jQuery(this).offset().left;
					jQuery(ul).addClass('hasbanner');
					if (toright_space > toleft_space) {
						//banner de izquierda a derecha
						jQuery(ul).css('min-width',toright_space);
						jQuery(ul).css('left',0);
						jQuery(ul).css('right','auto');
					}else{
						//banner de derecha a izquierda
						jQuery(ul).css('min-width',toleft_space);
						jQuery(ul).css('right',0);
						jQuery(ul).css('left','auto');
					}
				}
			})
		}
		_navElementOffset();
	},
	toggleSortResponsive: function(){
	    jQuery( "#sort-xs label" ).click(function() {
	    	jQuery(this).toggleClass('active');
	    	jQuery( "#sort-xs ul" ).removeClass('no-display');

	    	jQuery('.block-subtitle.block-subtitle--filter').removeClass('active');
		  	jQuery( "#narrow-by-list" ).addClass('no-display');
		  	jQuery( "#sort-xs ul" ).toggle();
		});

		jQuery( ".block-subtitle.block-subtitle--filter" ).click(function() {
	    	jQuery('#sort-xs label').removeClass('active');
		  	jQuery( "#sort-xs ul" ).addClass('no-display');
		});

	},
	toggleShopbyAdvanced: function(){
		jQuery('.block-content.toggle-content').addClass('accordion-open');
		jQuery('.amshopby-advanced li.amshopby-cat.has-child.active.parent').addClass('expanded');
		jQuery('.amshopby-advanced li.amshopby-cat.has-child').click(function(){
			jQuery(this).toggleClass('expanded');
		});
	},
	stickyHeader: function(){
		jQuery('#header').sticky({topSpacing:0});
	},
	activateSwipes: function(){
		if(jQuery(window).width() < 767){
			jQuery(window).load(function(){
				jQuery(".pink-image-left .description").swipe( {
				    //Generic swipe handler for all directions
				    swipe:function(event, direction, distance, duration, fingerCount, fingerData) {
				    	if(direction == 'left'); {
				    		jQuery(this).addClass('swipe-show');
				    	}
				    	if (direction == 'right') {
				    		jQuery(this).removeClass('swipe-show');
				    	}
				    }
				});
			})
		}
	},
	changeDOM: function(){
		if(jQuery(window).width() < 767){
			// Pink Spring
			jQuery('.pink-spring').append(jQuery(".pink-image-left .description .ver-mas"));
			jQuery(".pink-spring .ver-mas").wrap('<div class="ver-mas-container"></div>');

			// Toggle Footer
			jQuery('.footer .title').each(function(){
				jQuery(this).append('<span class="fa fa-angle-down"></span>');
			});
			jQuery('.title span').click(function(){
				jQuery('.footer .title').not(jQuery(this).parents('.title')).removeClass('active');
				jQuery(this).parents('.title').toggleClass('active');
				jQuery('.footer ul').not(jQuery(this).parents('.title').siblings('ul')).removeClass('active');
				jQuery(this).parents('.title').siblings('ul').toggleClass('active');
			});
		}
	}
}

var Ajaxtheme = {
	showMessage:function(message,status){
		jQuery('.messages').remove();
	    if (status== "ERROR") {
	        var stat = "error";
	    } else {
	        var stat = "success";
	    }
	    var html = '<ul id="message-wrapper" class="messages"><li class="' + stat + '-msg"><ul><li><span>' + message + '</span></li></ul></li></ul>';
	    jQuery(".col-main" ).prepend(html);
	    jQuery(".cms-index-index .home-wrapper").prepend(html);
	    jQuery('.messages').slideDown('400', function () {
	        setTimeout(function () {
	            jQuery('.messages').slideUp('400', function () {
	                jQuery(this).slideUp(400);
	                jQuery(this).remove();
	            });
	        }, 10000)
	        jQuery('#message-wrapper').fadeIn('slow');
	    });
	    function _gotoMessage(){
	    	wrapper_position = jQuery('#message-wrapper').offset().top  + 100;
	    	positiony = jQuery(document).scrollTop();
	    	if (positiony > wrapper_position) {
	    		jQuery('html, body').stop().animate({
	    			scrollTop: jQuery('#message-wrapper').offset().top +  150
	    		},1000);
	    	};
	    }
	    _gotoMessage();
	},
	reAssignLinksClick: function(){
	    var skipContents = jQuery('#header-cart');
	    var skipLinks    = jQuery('.skip-cart');
	    skipLinks.on('click', function (e) {
	        e.stopPropagation(); //Fix for closeSkipLinksOnDocumentClick();
	        e.preventDefault();
	        var self = jQuery(this);
	        // Use the data-target-element attribute, if it exists. Fall back to href.
	        var target = self.attr('data-target-element') ? self.attr('data-target-element') : self.attr('href');
	        // Get target element
	        var elem = jQuery(target);
	        // Check if stub is open
	        var isSkipContentOpen = elem.hasClass('skip-active') ? 1 : 0;
	        // Hide all stubs
	        skipLinks.removeClass('skip-active');
	        skipContents.removeClass('skip-active');
	        // Toggle stubs
	        if (isSkipContentOpen) {
	            self.removeClass('skip-active');
	        } else {
	            self.addClass('skip-active');
	            elem.addClass('skip-active');
	        }
	    });
	    jQuery('#header-cart').on('click', '.skip-link-close', function(e) {
	        var parent  = jQuery(this).parents('.skip-content');
	        var link 	= parent.siblings('.skip-link');
	        parent.removeClass('skip-active');
	        link.removeClass('skip-active');
	        e.preventDefault();
	    });
	},
	setAjaxData: function(data,iframe){
	    if(data.status == 'ERROR'){
	        alert(data.message);
	    }else{
	        if(jQuery('.block-cart')){
	            jQuery('.header-top .header-minicart').html(data.sidebar);
	        }
	        if(jQuery('.header .links')){
	            jQuery('.header .links').replaceWith(data.toplink);
	        }
	    }
	},
	wishlist:function(url,id){
	    url = url.replace("wishlist/index/add","brandlivetheme/ajax/wishlist");
	    url += 'isAjax/1/';
	    jQuery('#ajax_loading'+id).show();
	    jQuery.ajax( {
	        url : url,
	        dataType : 'json',
	        success : function(data) {
	            jQuery('#ajax_loading'+id).hide();
	            if(data.status == 'ERROR'){
	                Ajaxtheme.showMessage(data.message,status); // Hellothemes improved
	            }else{
	                Ajaxtheme.showMessage(data.message,status); // Hellothemes improved
	                if(jQuery('.block-wishlist').length){
	                    jQuery('.block-wishlist').replaceWith(data.sidebar);
	                }else{
	                    if(jQuery('.col-right').length){
	                        jQuery('.col-right').prepend(data.sidebar);
	                    }
	                }
	            }
	        }
	    });
	}
}
