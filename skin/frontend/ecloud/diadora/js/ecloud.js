/*!
 * Theme Athix
 * Ecloudsolutions.
 */
jQuery(window).load(function(){
 	Theme.homeSlider();
});

jQuery(document).ready(function(){
 	Theme.init();
 	CatalogView.init();
 	ProductPage.init();
 	Cart.init();
	Checkout_actions.init();
	FOOTER.init();
});
var FOOTER = {
	init: function(){
		//this.newsLeave();
		//this.newsSelect();
	},
	newsLeave: function(sender, defaultValue) {
	    if (jQuery.trim(sender.value) == '') sender.value = defaultValue;
	},
	newsSelect: function(sender, defaultValue) {
	    if (sender.value == defaultValue) sender.value = '';
	    else sender.select();
	},
}
var ProductPage = {
	init:function(){
		if (jQuery('body').hasClass('catalog-product-view')) {
			this.thumbsCarouselFull();
			this.onMobile();
			this.onDesktop();
			this.moveElements();
			this.thumbsCarouselFull();
			this.ventaCruzadaPDP();
			this.relacionados();
		};

	},
	relacionados: function(){
		if(jQuery('#aw-arp-list ul li').length > 1){
			jQuery('#aw-arp-list ul').owlCarousel({
				items: 4,
				dots: false,
				nav:true,
				loop:true,
				margin:10,
				responsive:{
					0:{
						items:2
					},
					600:{
						items:2
					},
					1000:{
						items:4
					}
				}
			});
		}else{
			jQuery('#aw-arp-list ul').show();
			jQuery('#aw-arp-list ul').addClass("no-carousel-relacionados");
		}
	},
	ventaCruzadaPDP: function(){
		if(jQuery('.catalog-product-view #crosssell-products-list li').length > 2){
			jQuery('.catalog-product-view #crosssell-products-list').owlCarousel({
				items: 5,
				dots: true,
				nav:true,
				singleItem: true,
				
				margin:4,
				loop:true,
				responsiveBaseElement:jQuery(".right")[0],
				responsive:{
					0:{
						items:5
					},
					600:{
						items:5
					},
					1000:{
						items:5
					}
				}
			});
		}else{
			jQuery('#crosssell-products-list').show();
			jQuery("#crosssell-products-list").addClass("no-carrousel");
		}
	},
	thumbsCarouselFull: function(){
		if(jQuery(window).width() < 771){
			if(jQuery('.product-image-gallery img').length > 1){
				jQuery('.product-image-gallery').owlCarousel({
					items: 1,
					dots: true,
					margin:10,
					nav:false,
					loop:true,
					autoWidth:false,
	
				});
			} else{
				jQuery('.product-image-gallery').show();
			}

		}

	},
	moveElements: function (){
		if(jQuery(window).width() < 771){
			jQuery(".add-to-cart-buttons").insertAfter(".crosssell");
			jQuery(".guia-talle").insertAfter(".product-options dl.last label");
		}
	},
	onMobile: function(){
		ProductPage.thumbsCarousel();
		jQuery(document).on('ajaxStop',function(){
			ProductPage.thumbsCarousel();
		});
		jQuery(window).resize(function(){
			ProductPage.thumbsCarousel();
		});
	},
	onDesktop: function(){
		ProductPage.onImageContainerResize();
		jQuery(document).on('ajaxStop',function(){
			ProductPage.onImageContainerResize();
		});
		jQuery(window).load(function(){
			ProductPage.onImageContainerResize();
		});
		jQuery(window).resize(function(){
			ProductPage.onImageContainerResize();
		});
	},
	thumbsCarousel:function(){
		jQuery('.product-image-thumbs.carousel').remove();
		if(jQuery(window).width() < 771){
			jQuery('.thumb-link').show(); 
			jQuery(".product-image-thumbs .thumb-link").css('max-width','');
			var quickview = jQuery(".ajaxview").length;
			var owlLoaded = jQuery(".product-image-thumbs").hasClass(".owl-loaded");
			var thumbsqty = jQuery(".product-image-thumbs .thumb-link").length;
			if (!owlLoaded){
				if (quickview < 1) {
					if( thumbsqty > 1 ){
						jQuery('.more-views .product-image-thumbs').hide();
						if(jQuery('.more-views .product-image-thumbs.carousel').length == 0){
							jQuery('.more-views .product-image-thumbs').clone().addClass('carousel').appendTo('.more-views');
							jQuery('.more-views .product-image-thumbs.carousel').show();
							jQuery('.more-views .product-image-thumbs.carousel').owlCarousel({
								items: 5,
								dots: false,
								nav:true,
								loop:true,
								autoWidth:false,
								slideBy:1,
								responsive:{
									0:{
										items:1
									}
								}
							})
						}else{
							jQuery('.more-views .product-image-thumbs.carousel').show();
						}
					}else{
						jQuery('.more-views .product-image-thumbs').show();
					}
				}
			}
		}else{
			//jQuery(owlLoaded).destroy();
			jQuery('.more-views .product-image-thumbs').show();
		}
	},
	onImageContainerResize:function(){
		if (jQuery('.product-image-container').length) {
			if(jQuery(window).width() > 770){
				mainImgHeight = jQuery('.product-image-zoom').outerHeight() - 10;
		 		jQuery('.more-views').css('max-height',mainImgHeight);
		 		if(jQuery(".product-image-thumbs .thumb-link").length > 4){
		 			jQuery('.more-views').css('overflow','scroll');
		 			jQuery('.more-views').css('overflow-x','hidden');
		 		}
		 		ProductPage.thumbnailsHeightAndClick();
		 	}else{
		 		jQuery('.more-views').css('max-height','');
		 		jQuery('.more-views').css('overflow','');
		 	}

		}
	},
	thumbnailsHeightAndClick: function(){
		thumbsCount = jQuery(".product-image-thumbs .thumb-link").length;
		if( thumbsCount != 1){
		    jQuery(".product-image-thumbs .thumb-link").first().hide();
		    if( thumbsCount >= 2){
		    	jQuery(".product-image-thumbs .thumb-link").css('max-width','');
		    	var zoomImgHeight = jQuery('.product-image-zoom').outerHeight() - 15;
		    	jQuery(".product-image-thumbs .thumb-link").css('max-width',(zoomImgHeight/2));
		    	if (thumbsCount == 4) {
		    		jQuery(".product-image-thumbs .thumb-link").css('max-width',(zoomImgHeight/3));
		    	}
		    	if(thumbsCount > 4){
		    		jQuery(".product-image-thumbs .thumb-link").height('auto');
		    		jQuery(".product-image-thumbs .thumb-link").css('max-width','');
		 			jQuery('.more-views').css('overflow','scroll');
		 			jQuery('.more-views').css('overflow-x','hidden');
		 		}
		    }
		    function _onThumbnailClick(){
		    	if(jQuery(window).width() > 770){
			    	jQuery('.thumb-link').click(function(){
			    		if(jQuery(".product-image-thumbs .thumb-link").length != 1){
			    			jQuery('.thumb-link').show(); 
			    			jQuery(this).hide();
			    		}
			    	})
		    	}
		    }
		    _onThumbnailClick();
		}
	},

}
var Checkout_actions = {
	init:function(){
		if (jQuery('body').hasClass('checkout-onepage-index')) {
			this.Pickit_removerTelefonoConUnos();
		}
		this.updateCartResume();
		document.body.fire('checkoutstep:reloaded');
	},
	updateCartResume: function(){
		var cartResumeElement = jQuery('#checkout-cart-resume');
		if(cartResumeElement.length > 0){
			 document.observe("checkoutstep:reloaded", function(event) {
				 var prodcount = parseFloat(jQuery('.custom-minicart-footer .col-items-count span').text());
				 var prodcountlabel = cartResumeElement.find('.cart-prod-count-label').attr('data-label');
				 if(prodcount == 1){
					 prodcountlabel = prodcountlabel.replace('s', '');
				 }
				 var shippingprice = '$'+window.quoteShippingAmount;
				 if(jQuery('#shipping_method-progress-opcheckout .price').length > 0){
					 shippingprice = jQuery('#shipping_method-progress-opcheckout .price').html();
				 }
				 shippingprice = shippingprice.toString().replace('$', '$ ');
 
				 var shippingprice_float = parseFloat(shippingprice.replace('$ ', '').replace(',', '.'));
				 
				 shippingprice = shippingprice.replace('.', ',');
				 cartResumeElement.find('.cart-shipping-val').html(shippingprice);
				 
				 cartResumeElement.find('.cart-prod-count-label').text(prodcountlabel);
				 cartResumeElement.find('.cart-prod-count').html(prodcount);
 
				 var subTotal = window.quoteBaseSubTotal.toString().replace('.', ',');
				 cartResumeElement.find('.cart-subtotal-val').html('$ '+subTotal);
 
				 var calculatedTotal = window.quoteBaseSubTotal + shippingprice_float;
				 
				 var grandTotal = window.checkQuoteBaseGrandTotal;
				 if(calculatedTotal > window.checkQuoteBaseGrandTotal){
					 grandTotal = calculatedTotal;
				 }
				 
				 grandTotal = Math.round((grandTotal + Number.EPSILON) * 100) / 100;
				 grandTotal = grandTotal.toString().replace('.', ',')
				 cartResumeElement.find('.cart-total-val').html('$ '+ grandTotal);
			});
		}
	},
	Pickit_removerTelefonoConUnos: function(){
		document.observe("checkoutstep:done", function(event) {
			var regex = /\/ 11111111/g;
			var sectores = jQuery('#checkout-review-table, #checkout-progress-wrapper');
			jQuery.each(sectores, function(){
				var text = jQuery(this).html();
				console.log('ejecutado: '+jQuery(this).attr('id'));
				if(regex.test(text)){
					console.log('encontrado: '+jQuery(this).attr('id'));
					jQuery(this).html(text.replace(regex, ''));
				}
			});
		});
	}
}
var Cart = {
	init:function(){
		if (jQuery('body').hasClass('checkout-cart-index')) {
			this.ventaCruzada();
			this.ocultarPickitEnResumen();
			this.moveShippingEstimate();
		};
	},
	moveShippingEstimate:function(){
		jQuery(".cart-forms .shipping").insertAfter(".cupons");
	},
	ocultarPickitEnResumen: function(){
		var substr = new RegExp('pickit','i');
		jQuery('#shopping-cart-totals-table .a-right').each(function(){
			if(substr.test(jQuery(this).html())){
				jQuery(this).parent().hide();
			}
		});
	},
	ventaCruzada: function(){
		jQuery('.checkout-cart-index #crosssell-products-list').owlCarousel({
			items: 4,
			dots: false,
			margin:10,
			nav:true,
			loop:true,
			autoWidth:false,
			responsiveClass:true,
		    responsive:{
		        0:{
		            items:2,
		            slideBy:2
		        },
		        600:{
		            items:2,
		           	slideBy:2
		        },
		        1000:{
		            items:4
		        }
		    }
		});
	},
}
var CatalogView = {
	init: function(){
		if (jQuery('.catalog-category-view').length || jQuery('.catalogsearch-result-index').length) {
			this.toggleSwatches();
			this.onAjaxLayeredFilter();
			this.Filter();
		}
	},
	Filter: function(){
		jQuery('span.filter').on('click',function(){
			jQuery("#block-content").toggleClass("skip-active");
		});
		if (jQuery(window).width() >= 991){
			jQuery('#narrow-by-list dt').on('click',function(){
				jQuery(this).next('dd').toggle();
				
			});
		 }
		jQuery(".sort-by").insertAfter("#filter-content");
		
		if (jQuery(window).width() < 991){
			if(jQuery("dt:contains('Disciplina')").length){
				jQuery("dt:contains('Disciplina')").parent().parent().addClass("mobile-filter");
				
				jQuery(".mobile-filter").insertBefore(".category-products");

				// function Minuscula(string){
					
				// 	return string.charAt(0).toLowerCase() + string.slice(1).replace(/ú/gi,"u");
				
				// }
				// jQuery(".mobile-filter ol").find("li").each(function()
				// {
				// 	var data = jQuery(this).attr("data-text");  
				// 	CadenaTexto = Minuscula(data.toLowerCase().replace(" ", "-")); 
				// 	jQuery(this).addClass(CadenaTexto);
				// 	jQuery(this).children("a").prepend('<img src=/skin/frontend/ecloud/diadora/images/' + CadenaTexto +".jpg" + '>');
					
				// });
			}
			var owloptions = {
				autoplay: false,
			   	singleItem: false,
				loop: true,
			   	dots: false,
			   	nav:false,
			   	autoWidth: true,
			   	responsiveClass:true
   
		   };
		   if(jQuery(window).width() > 400){
			   jQuery('.mobile-filter ol').owlCarousel(owloptions);
		   }
		}
   },
	onAjaxLayeredFilter: function(){
		jQuery('.col-main').bind('DOMSubtreeModified', function() {
			// jQuery(document).trigger('ajaxFilteredContent');
			setTimeout(function(){
				jQuery('.ajax_load').fadeOut();
			},300);
		});
	},
	onInfiniteScrollReady: function(){
		jQuery(document).on('ajaxStop',function(){
			jQuery(document).on('infiniteScrollReady', function(items){
				console.log('infiniteScrollReady document');
				ConfigurableMediaImages.ajaxLoadSwatchList();
			});
		});
	},
	toggleSwatches: function(prodgrid){
		jQuery(document).on('ajaxStop',function(){
		    jQuery('.products-grid li').not('.toggled').each(function(){
		    	if (jQuery(this).find('.configurable-swatch-list').length > 0) {
			    	jQuery(this).find('.swatches-toggle').click(function(){
				        jQuery(this).toggleClass('opened');
				        jQuery(this).siblings('.configurable-swatch-list').slideToggle();
				    });
				    jQuery(this).addClass('toggled');
		    	}
		    });
	    });
	},
	InifiniteScrollRendered: function(){
		jQuery(document).on('ajaxStop',function(){
			//Integracion con infiniteScroll.
			window.ias.on('rendered', function(items){
				ConfigurableMediaImages.ajaxLoadSwatchList();
				// Theme.toggleSwatchesOnProdList();
			});
		});
	}
}

var Theme ={
	init:function(){
		this.TopBar();
	 	this.navMenuBanners();
	 	this.toggleShopbyAdvanced();
	 	this.stickyHeader();
	 	this.owlCarousel();
	 	this.tabsCarousel();
	 	this.tabContact();
	 	//this.destacadosCarousel();
	 	//this.ventaCruzada();
	 	this.toggleSortResponsive();
		this.activateSwipes();
		this.changeDOM();
		this.hideAjaxLoader();
		this.closeSkipLinksOnDocumentClick();
		this.headerMobileSearch();
		this.removeisSticky();
		this.minicartQtyBtns();
		this.devolcionesPage();
		this.closeMenuMobile();
		this.scrollContacto();
	},
	TopBar: function(){
		if(jQuery(window).width() < 991){
			if(jQuery(".topbanner").length){

				jQuery(window).scroll(function () {
					jQuery('#header-nav').add(".search-desktop").toggleClass("top-banner-hidden", (jQuery(window).scrollTop() > 1));
				});
				jQuery("#header-nav").add(".search-desktop").addClass("top-banner-show");

				
			}
		}
	},
	scrollContacto: function(){
		function gotoMessage(){
			jQuery('html,body').stop().animate({
				scrollTop: jQuery('.webforms-success-text').offset().top - 700
			},2000);
			window.scrollTo(0, 0);
		}
		jQuery(".cms-contacto .buttons-set .button").click(function() {
			gotoMessage();

		});



	},
	closeMenuMobile: function(){
		jQuery(".search-desktop").on('click', '.skip-link-close', function(e) {
	        var element  = jQuery('.search-desktop');
	
	        element.removeClass('active');
	       
	        e.preventDefault();
		});
		jQuery('#header-cart').add("#header-nav .title-mobile").on('click', '.skip-link-close', function(e) {
	        var parent  = jQuery(this).parents('.skip-content');
	        var link 	= parent.siblings('.skip-link');
	        parent.removeClass('skip-active');
	        link.removeClass('skip-active');
			e.preventDefault();
			jQuery(".mobile-nav-container a").removeClass("skip-active");
		});
	},
	devolcionesPage: function (){
		if (jQuery('body').hasClass('cms-cambios-devoluciones')) {
			jQuery('.webforms-fields-pickit-input').hide();
			jQuery('.webforms-fields-options-to .list-select li input[value="Lo entrego en un punto Pickit (puntos en toda la ciudad)"]').on( "click", function() {
				jQuery('.webforms-fields-pickit-input').show();
				jQuery('.webforms-fields-pickit-input input').toggleClass('required-entry');
				jQuery('.webforms-fields-pickit-input label').toggleClass('required');
				jQuery(".retiro-domicilio").hide();
			});

			jQuery('.webforms-fields-options-to .list-select li input[value="Quiero lo vengan a buscar a mi domicilio (alguien debe atender al correo)"]').on( "click", function() {
				jQuery('.webforms-fields-pickit-input').hide();
				jQuery('.webforms-fields-pickit-input input').toggleClass('required-entry');
				jQuery('.webforms-fields-pickit-input label').toggleClass('required');
				jQuery(".retiro-domicilio").show();
			});

			jQuery(".retiro-domicilio .checkbox").prop('checked', true);
			jQuery('.webforms-fields-producto-2').hide();
			jQuery('.webforms-fields-agregar-producto-3').hide();

			jQuery(".webforms-fields-agregar-producto-2 .list-select li input").click(function() {
				jQuery('.webforms-fields-agregar-producto-2 .list-select li input').toggleClass('on', jQuery(this).is(":checked"));
				if(jQuery('.webforms-fields-agregar-producto-2 .list-select li input').hasClass('on')) {
					 jQuery('.webforms-fields-producto-2').show();
					 jQuery('.webforms-fields-agregar-producto-3').show();
				}else{ 
					jQuery('.webforms-fields-producto-2').hide();
					jQuery('.webforms-fields-agregar-producto-3').hide();
				}
			});

			jQuery('.webforms-fields-producto-3').hide();
			jQuery(".webforms-fields-agregar-producto-3 .list-select li input").click(function() {
				jQuery('.webforms-fields-agregar-producto-3 .list-select li input').toggleClass('on', jQuery(this).is(":checked"));
				if(jQuery('.webforms-fields-agregar-producto-3 .list-select li input').hasClass('on')) {
					 jQuery('.webforms-fields-producto-3').show();
				}else{ 
					jQuery('.webforms-fields-producto-3').hide();
				}
			});

			jQuery('.webforms-fields-calle').hide();
			jQuery('.webforms-fields-nro').hide();
			jQuery('.webforms-fields-piso-depto').hide();
			jQuery('.webforms-fields-cod-postal').hide();
			jQuery('.webforms-fields-localidad').hide();
			jQuery('.webforms-fields-provincia').hide();

			jQuery(".retiro-domicilio .checkbox").click(function() {
				jQuery('.retiro-domicilio .checkbox').toggleClass('on', jQuery(this).is(":checked"));
				if(jQuery('.retiro-domicilio .checkbox').hasClass('on')) {
					jQuery('.webforms-fields-calle').hide();
					jQuery('.webforms-fields-nro').hide();
					jQuery('.webforms-fields-piso-depto').hide();
					jQuery('.webforms-fields-cod-postal').hide();
					jQuery('.webforms-fields-localidad').hide();
					jQuery('.webforms-fields-provincia').hide();
				}else{ 
					jQuery('.webforms-fields-calle').show();
					 jQuery('.webforms-fields-calle input').toggleClass('required-entry');
					 jQuery('.webforms-fields-calle label').toggleClass('required');
					 jQuery('.webforms-fields-nro').show();
					 jQuery('.webforms-fields-nro input').toggleClass('required-entry');
					 jQuery('.webforms-fields-nro label').toggleClass('required');
					 jQuery('.webforms-fields-piso-depto').show();
					 jQuery('.webforms-fields-cod-postal').show();
					 jQuery('.webforms-fields-cod-postal input').toggleClass('required-entry');
					 jQuery('.webforms-fields-cod-postal label').toggleClass('required');
					 jQuery('.webforms-fields-localidad').show();
					 jQuery('.webforms-fields-localidad input').toggleClass('required-entry');
					 jQuery('.webforms-fields-localidad label').toggleClass('required');
					 jQuery('.webforms-fields-provincia').show();
					 jQuery('.webforms-fields-provincia input').toggleClass('required-entry');
					 jQuery('.webforms-fields-provincia label').toggleClass('required');
				}
			});

		}

	},


	closeSkipLinksOnDocumentClick: function(){
		jQuery('.skip-content').click(function(e){
			e.stopPropagation();
		});
		jQuery('body').click(function(e){
			jQuery('.skip-link.skip-active').removeClass('skip-active');
			jQuery('.skip-link.active').removeClass('active');
			jQuery('.skip-content.skip-active').removeClass('skip-active');
		});
	},
	headerMobileSearch: function(){
		if(jQuery(window).width() < 991){
			jQuery( ".search-account-minicart .search .skip-search i" ).click(function() {
				jQuery(this).parent().siblings('.search-desktop').toggleClass('active');

			});
			jQuery("#search_mini_form").insertAfter('.search-account-minicart .search-mobile .dummy');
		}
	},
	hideAjaxLoader: function(){
		jQuery(document).on('ajaxStop',function(){
			setTimeout(function(){
				jQuery('.ajax_load').fadeOut();
			},300)
		});
	},
	owlCarousel:function(){
		// if(jQucrossery('body').hasClass('checkout-cart-index') && jQuery('body').hasClass('checkout-onepage-index')){
			if(jQuery('.owl-carousel').length > 0){
				jQuery('.owl-carousel').owlCarousel({
					items: 4,
					dots: true,
					nav:true,
					loop:true,
					margin:0,
					autoWidth:false,
					slideBy:1,
					responsive:{
						0:{
							items:1
						},
						600:{
							items: 2
						},
						1000:{
							items:4
						}
					}
				})
			}
		// }
	},
	tabsCarousel: function(){
		if(jQuery('#content-carousel ul').length > 0){
			jQuery('#content-carousel ul').owlCarousel({
				items: 4,
				dots: false,
				nav:true,
				loop:true,
				autoWidth:false,
				responsiveClass:true,
			    responsive:{
			        0:{
			            items:2,
			            slideBy:2
			        },
			        600:{
			            items:2,
			           	slideBy:2
			        },
			        1000:{
			            items:4
			        }
			    }
			});
		}
	},

	destacadosCarousel: function(){
		if(jQuery('#destacados ul').length > 0){
			jQuery('#destacados ul').owlCarousel({
				items: 4,
				dots: false,
				nav:true,
				loop:true,
				autoWidth:false,
				responsiveClass:true,
			    responsive:{
			        0:{
			            items:2,
			            slideBy:2
			        },
			        600:{
			            items:2,
			           	slideBy:2
			        },
			        1000:{
			            items:4
			        }
			    }
			});
		}
	},

	tabContact:function(){
	    jQuery("div.contact-tab-menu>div.list-group>a").click(function(e) {
	        e.preventDefault();
	        jQuery(this).siblings('a.active').removeClass("active");
	        jQuery(this).addClass("active");
	        var index = jQuery(this).index();
	        jQuery("div.contact-tab>div.contact-tab-content").removeClass("active");
	        jQuery("div.contact-tab>div.contact-tab-content").eq(index).addClass("active");
	    });
	},
	homeSlider: function(){
		if (jQuery('body').hasClass('cms-home')) {
			var length = jQuery("#home-slider .rslides").find('li').length;
			if (length > 1) {
				//jQuery('#home-slider').prependTo('.container-fluid.col1-layout');
				jQuery("#home-slider ul.rslides").owlCarousel({
					autoplay: true,             // Boolean: Animate automatically, true or false
					items: 1,
					singleItem: true,
					loop: true,
					autoplayTimeout: 11000
					//dots: false
				});
			}
		}
	},
	navMenuBanners: function(){
		function _navElementOffset(){
			var navli = jQuery('.nav-primary li');
			jQuery(navli).mouseenter(function(){
				var hasBanner = jQuery(this).find('.submenu-banner').children().length;
				if (hasBanner > 0){
					var ul = jQuery(this).find('ul.level0');
					var toright_space = (jQuery(window).width() - (jQuery(this).offset().left + jQuery(this).outerWidth()));
					var toleft_space  = jQuery(this).offset().left;
					jQuery(ul).addClass('hasbanner');
					if (toright_space > toleft_space) {
						//banner de izquierda a derecha
						jQuery(ul).css('min-width',toright_space);
						jQuery(ul).css('left',0);
						jQuery(ul).css('right','auto');
					}else{
						//banner de derecha a izquierda
						jQuery(ul).css('min-width',toleft_space);
						jQuery(ul).css('right',0);
						jQuery(ul).css('left','auto');
					}
				}
			})
		}
		_navElementOffset();
	},
	toggleSortResponsive: function(){
	    jQuery( "#sort-xs label" ).click(function() {
	    	jQuery(this).toggleClass('open');
	    	jQuery( "#sort-xs ul" ).toggleClass('open');
		});

		jQuery( ".block-subtitle.block-subtitle--filter" ).click(function() {
	    	jQuery(this).toggleClass('open');
		  	jQuery( "#narrow-by-list" ).toggleClass('open');
		});
	},
	toggleShopbyAdvanced: function(){
		jQuery('.block-content.toggle-content').addClass('accordion-open');
		jQuery('.amshopby-advanced li.amshopby-cat.has-child.active.parent').addClass('expanded');
		jQuery('.amshopby-advanced li.amshopby-cat.has-child').click(function(){
			jQuery(this).toggleClass('expanded');
		});
	},
	 stickyHeader: function(){
	 	if(!jQuery('body').hasClass('checkout-cart-index') && !jQuery('body').hasClass('checkout-onepage-index')){
	 		jQuery('#header').sticky({topSpacing:0});
	 	}
	 },
	activateSwipes: function(){
		if(jQuery(window).width() < 767){
			jQuery(window).load(function(){
				jQuery(".pink-image-left .description").swipe( {
				    //Generic swipe handler for all directions
				    swipe:function(event, direction, distance, duration, fingerCount, fingerData) {
				    	if(direction == 'left'); {
				    		jQuery(this).addClass('swipe-show');
				    	}
				    	if (direction == 'right') {
				    		jQuery(this).removeClass('swipe-show');
				    	}
				    }
				});
			})
		}
	},
	changeDOM: function(){
		if(jQuery(window).width() < 992){
			// Pink Spring
			jQuery('.pink-spring').append(jQuery(".pink-image-left .description .ver-mas"));
			jQuery(".pink-spring .ver-mas").wrap('<div class="ver-mas-container"></div>');

			// Toggle Footer
			jQuery('.footer .title').each(function(){
				jQuery(this).append('<span class="fa fa-angle-down"></span>');
			});
			jQuery('.footer .title').click(function(){
			    jQuery('.footer .title').not(jQuery(this)).removeClass('active');
				jQuery(this).toggleClass('active');
				jQuery('.footer ul').not(jQuery(this).siblings('ul')).removeClass('active');
				jQuery(this).siblings('ul').toggleClass('active');
			});
		}
	},
	removeisSticky:function(){
	 	if (jQuery('#header-sticky-wrapper').hasClass('is-sticky')) {
	 		jQuery('#header-sticky-wrapper').removeClass('is-sticky');
	 	}
	},
	minicartQtyBtns: function(){
		if(jQuery('.qty-wrapper .minusQty').length > 0){
			jQuery.each(jQuery('.qty-wrapper'), function(){
				var input = jQuery(this).find('input');
				var minusBtn = jQuery(this).find('.minusQty');
				var plusBtn = jQuery(this).find('.plusQty');
				var updBtn = jQuery(this).find('.quantity-button');
				minusBtn.on('click', function(){
					var qty = parseInt(jQuery(input).val());
					if ( !isNaN(qty) ) {
						qty = 0 ? qty+1 : (qty >= 1 ? qty-1 : 0);
						if (qty >= 1) {
							jQuery(input).val(qty);
						}
					}   
					updBtn.trigger('click');
				});
				plusBtn.on('click', function(){
					var qty = parseInt(jQuery(input).val());
					if ( !isNaN(qty) ) {
						qty = 1 ? qty+1 : (qty >= 1 ? qty-1 : 0);
						if (qty >= 1) {
							jQuery(input).val(qty);
						}
					}   
					updBtn.trigger('click');
				});
			
			});
			
		};
	}
}

jQuery( document ).ajaxComplete(function() {
	Theme.minicartQtyBtns();
});


var Ajaxtheme = {
	showMessage:function(message,status){
		jQuery('.messages').remove();
	    if (status== "ERROR") {
	        var stat = "error";
	    } else {
	        var stat = "success";
	    }
	    var html = '<ul id="message-wrapper" class="messages"><li class="' + stat + '-msg"><ul><li><span>' + message + '</span></li></ul></li></ul>';
	    jQuery(".col-main" ).prepend(html);
	    jQuery(".cms-index-index .home-wrapper").prepend(html);
	    jQuery('.messages').slideDown('400', function () {
	        setTimeout(function () {
	            jQuery('.messages').slideUp('400', function () {
	                jQuery(this).slideUp(400);
	                jQuery(this).remove();
	            });
	        }, 10000)
	        jQuery('#message-wrapper').fadeIn('slow');
	    });
	    function _gotoMessage(){
	    	wrapper_position = jQuery('#message-wrapper').offset().top  + 100;
	    	positiony = jQuery(document).scrollTop();
	    	if (positiony > wrapper_position) {
	    		jQuery('html, body').stop().animate({
	    			scrollTop: jQuery('#message-wrapper').offset().top +  150
	    		},1000);
	    	};
	    }
	    _gotoMessage();
	},
	reAssignLinksClick: function(){
	    var skipContents = jQuery('#header-cart');
	    var skipLinks    = jQuery('.skip-cart');
	    skipLinks.on('click', function (e) {
	        e.stopPropagation(); //Fix for closeSkipLinksOnDocumentClick();
	        e.preventDefault();
	        var self = jQuery(this);
	        // Use the data-target-element attribute, if it exists. Fall back to href.
	        var target = self.attr('data-target-element') ? self.attr('data-target-element') : self.attr('href');
	        // Get target element
	        var elem = jQuery(target);
	        // Check if stub is open
	        var isSkipContentOpen = elem.hasClass('skip-active') ? 1 : 0;
	        // Hide all stubs
	        skipLinks.removeClass('skip-active');
	        skipContents.removeClass('skip-active');
	        // Toggle stubs
	        if (isSkipContentOpen) {
	            self.removeClass('skip-active');
	        } else {
	            self.addClass('skip-active');
	            elem.addClass('skip-active');
	        }
	    });
	    jQuery('#header-cart').add("#header-nav .title-mobile").on('click', '.skip-link-close', function(e) {
	        var parent  = jQuery(this).parents('.skip-content');
	        var link 	= parent.siblings('.skip-link');
	        parent.removeClass('skip-active');
	        link.removeClass('skip-active');
	        e.preventDefault();
		});
	},
	setAjaxData: function(data,iframe){
	    if(data.status == 'ERROR'){
	        alert(data.message);
	    }else{
	        if(jQuery('.block-cart')){
	            jQuery('.minicart .header-minicart').html(data.sidebar);
	        }
	        if(jQuery('.header .links')){
	            jQuery('.header .links').replaceWith(data.toplink);
	        }
	    }
	},
	wishlist:function(url,id){
	    url = url.replace("wishlist/index/add","brandlivetheme/ajax/wishlist");
	    url += 'isAjax/1/';
	    jQuery('#ajax_loading'+id).show();
	    jQuery.ajax( {
	        url : url,
	        dataType : 'json',
	        success : function(data) {
	            jQuery('#ajax_loading'+id).hide();
	            if(data.status == 'ERROR'){
	                Ajaxtheme.showMessage(data.message,status); // Hellothemes improved
	            }else{
	                Ajaxtheme.showMessage(data.message,status); // Hellothemes improved
	                if(jQuery('.block-wishlist').length){
	                    jQuery('.block-wishlist').replaceWith(data.sidebar);
	                }else{
	                    if(jQuery('.col-right').length){
	                        jQuery('.col-right').prepend(data.sidebar);
	                    }
	                }
	            }
	        }
	    });
	}
}

